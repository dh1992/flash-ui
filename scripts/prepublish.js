const fs = require('fs');
const path = require('path');

const package = require('../package');
const srcPackage = require('../publish/package');
const mockPackage = require('../publish/mock/package');

srcPackage.version = package.version;
srcPackage.peerDependencies = package.peerDependencies;
mockPackage.version = package.version;

const publishDir = path.join(__dirname, '../publish');

fs.writeFileSync(path.join(publishDir, 'package.json'), JSON.stringify(srcPackage, null, 2));
fs.writeFileSync(path.join(publishDir, 'mock/package.json'), JSON.stringify(mockPackage, null, 2));
