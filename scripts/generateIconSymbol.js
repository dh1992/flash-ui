const path = require('path');
const fs = require('fs');
const cheerio = require('cheerio');

const iconDir = path.join(__dirname, '../src/fui/icon/svg');

const svgFileNames = fs.readdirSync(iconDir)
.filter((svgFileName) => svgFileName.split('.').pop() === 'svg');

const symbolHtml = svgFileNames
.map((svgFileName) => {
  const location = path.join(iconDir, svgFileName);
  const content = fs.readFileSync(location, 'utf8');
  const $ = cheerio.load(content);
  const iconName = svgFileName.split('.').shift();
  // 移除title标签
  $('title').remove();
  $('style').remove();
  // 在svg标签上添加id，并移除xmlns属性
  $('svg')
  .attr('id', iconName)
  .attr('xmlns', null)
  .attr('width', null)
  .attr('height', null)
  .attr('data-name', null);
  // 移除class属性
  $('path')
  .attr('class', null);
  $('rect')
  .attr('class', null);
  $('polygon')
  .attr('class', null);
  // 移除path和g标签上的多余属性
  $('path')
  .attr('fill', null)
  .attr('class', null);
  $('g')
  .attr('fill', null)
  .attr('id', null)
  .attr('data-name', null);
  // 把svg标签替换为symbol
  return $('body').html().replace(/svg/g, 'symbol');
})
.join('\n');

const ret = `<svg style="display:none" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" >\n${symbolHtml}\n</svg>`;
const retFileName = path.join(iconDir, '../icon-symbol.component.html');

fs.writeFileSync(retFileName, ret);
