<a name="0.11.10"></a>
## [0.11.10](http://172.16.1.41:10080/TDC/flash-ui/compare/v0.11.9...v0.11.10) (2021-01-04)


### Bug Fixes

* [WARP-54585](http://172.16.0.244:8080/browse/WARP-54585) number input bug fix ([a75fc02](http://172.16.1.41:10080/TDC/flash-ui/commits/a75fc02))



<a name="0.11.9"></a>
## [0.11.9](http://172.16.1.41:10080/TDC/flash-ui/compare/v0.11.8...v0.11.9) (2020-12-09)


### Bug Fixes

* [WARP-53575](http://172.16.0.244:8080/browse/WARP-53575) remove unused dependency ([5d547f9](http://172.16.1.41:10080/TDC/flash-ui/commits/5d547f9))



<a name="0.11.8"></a>
## [0.11.8](http://172.16.1.41:10080/TDC/flash-ui/compare/v0.11.7...v0.11.8) (2020-12-09)


### Bug Fixes

* [WARP-53189](http://172.16.0.244:8080/browse/WARP-53189) fix cascade trigger bug ([0cf3805](http://172.16.1.41:10080/TDC/flash-ui/commits/0cf3805))
* [WARP-53213](http://172.16.0.244:8080/browse/WARP-53213) ci stages ([ec2d172](http://172.16.1.41:10080/TDC/flash-ui/commits/ec2d172))
* [WARP-53243](http://172.16.0.244:8080/browse/WARP-53243) pass lint test ([c4ef306](http://172.16.1.41:10080/TDC/flash-ui/commits/c4ef306))


### Features

* [WARP-53242](http://172.16.0.244:8080/browse/WARP-53242) update nav background color ([af0dbba](http://172.16.1.41:10080/TDC/flash-ui/commits/af0dbba))
* [WARP-53575](http://172.16.0.244:8080/browse/WARP-53575) update ng11 ([9d3d011](http://172.16.1.41:10080/TDC/flash-ui/commits/9d3d011))



<a name="0.11.7"></a>
## [0.11.7](http://172.16.1.41:10080/TDC/flash-ui/compare/v0.11.6...v0.11.7) (2020-11-26)


### Bug Fixes

* [WARP-52723](http://172.16.0.244:8080/browse/WARP-52723) btn icon color ([2509798](http://172.16.1.41:10080/TDC/flash-ui/commits/2509798))


### Features

* [WARP-52628](http://172.16.0.244:8080/browse/WARP-52628) api services ([ce180b4](http://172.16.1.41:10080/TDC/flash-ui/commits/ce180b4))
* [WARP-52682](http://172.16.0.244:8080/browse/WARP-52682) model display ([820aa02](http://172.16.1.41:10080/TDC/flash-ui/commits/820aa02))
* [WARP-52780](http://172.16.0.244:8080/browse/WARP-52780) blue theme ([7cc758e](http://172.16.1.41:10080/TDC/flash-ui/commits/7cc758e))
* [WARP-52784](http://172.16.0.244:8080/browse/WARP-52784) on fly edit ([9df08fa](http://172.16.1.41:10080/TDC/flash-ui/commits/9df08fa))
* [WARP-52923](http://172.16.0.244:8080/browse/WARP-52923) add Anchor ([087b0c9](http://172.16.1.41:10080/TDC/flash-ui/commits/087b0c9))



<a name="0.11.6"></a>
## [0.11.6](http://172.16.1.41:10080/TDC/flash-ui/compare/v0.11.5...v0.11.6) (2020-11-12)


### Bug Fixes

* [WARP-50728](http://172.16.0.244:8080/browse/WARP-50728) theme in doc prod build ([d082e53](http://172.16.1.41:10080/TDC/flash-ui/commits/d082e53))
* [WARP-52618](http://172.16.0.244:8080/browse/warp-52618) ([89163c0](http://172.16.1.41:10080/TDC/flash-ui/commits/89163c0))


### Features

* [WARP-52522](http://172.16.0.244:8080/browse/WARP-52522) support 3 digit hex ([c633913](http://172.16.1.41:10080/TDC/flash-ui/commits/c633913))
* [WARP-52522](http://172.16.0.244:8080/browse/WARP-52522) theme color search ([55e8a38](http://172.16.1.41:10080/TDC/flash-ui/commits/55e8a38))



<a name="0.11.5"></a>
## [0.11.5](http://172.16.1.41:10080/TDC/flash-ui/compare/v0.11.4...v0.11.5) (2020-10-29)



<a name="0.11.4"></a>
## [0.11.4](http://172.16.1.41:10080/TDC/flash-ui/compare/v0.11.3...v0.11.4) (2020-10-28)


### Features

* [WARP-51941](http://172.16.0.244:8080/browse/warp-51941) ([032fd74](http://172.16.1.41:10080/TDC/flash-ui/commits/032fd74))



<a name="0.11.3"></a>
## [0.11.3](http://172.16.1.41:10080/TDC/flash-ui/compare/v0.11.2...v0.11.3) (2020-10-22)


### Bug Fixes

* [WARP-51797](http://172.16.0.244:8080/browse/warp-51797) ([8aca598](http://172.16.1.41:10080/TDC/flash-ui/commits/8aca598))



<a name="0.11.2"></a>
## [0.11.2](http://172.16.1.41:10080/TDC/flash-ui/compare/v0.11.1...v0.11.2) (2020-10-21)


### Bug Fixes

* [WARP-51797](http://172.16.0.244:8080/browse/warp-51797) ([774ebe8](http://172.16.1.41:10080/TDC/flash-ui/commits/774ebe8))



<a name="0.11.1"></a>
## [0.11.1](http://172.16.1.41:10080/TDC/flash-ui/compare/v0.11.0...v0.11.1) (2020-10-20)


### Bug Fixes

* [WARP-51730](http://172.16.0.244:8080/browse/warp-51730) ([3d2c311](http://172.16.1.41:10080/TDC/flash-ui/commits/3d2c311))



<a name="0.11.0"></a>
# [0.11.0](http://172.16.1.41:10080/TDC/flash-ui/compare/v0.10.5...v0.11.0) (2020-10-19)



<a name="0.10.5"></a>
## [0.10.5](http://172.16.1.41:10080/TDC/flash-ui/compare/v0.10.3...v0.10.5) (2020-10-19)


### Features

* [WARP-50728](http://172.16.0.244:8080/browse/WARP-50728) dark colors ([f85b0ef](http://172.16.1.41:10080/TDC/flash-ui/commits/f85b0ef))



<a name="0.10.3"></a>
## [0.10.3](http://172.16.1.41:10080/TDC/flash-ui/compare/v0.10.2...v0.10.3) (2020-08-11)


### Bug Fixes

* input-number-min ([6d71939](http://172.16.1.41:10080/TDC/flash-ui/commits/6d71939))



<a name="0.10.2"></a>
## [0.10.2](http://172.16.1.41:10080/TDC/flash-ui/compare/v0.10.1...v0.10.2) (2020-07-29)


### Bug Fixes

* lint error ([9e1def4](http://172.16.1.41:10080/TDC/flash-ui/commits/9e1def4))


### Features

* add component api comments ([819ae99](http://172.16.1.41:10080/TDC/flash-ui/commits/819ae99))
* doc api description ([bb78a57](http://172.16.1.41:10080/TDC/flash-ui/commits/bb78a57))



<a name="0.10.1"></a>
## [0.10.1](http://172.16.1.41:10080/TDC/flash-ui/compare/v0.10.0...v0.10.1) (2020-07-09)


### Bug Fixes

* tooltip message spacing ([b763806](http://172.16.1.41:10080/TDC/flash-ui/commits/b763806))
* **submenu:** add descendants ([154892b](http://172.16.1.41:10080/TDC/flash-ui/commits/154892b))



<a name="0.10.0"></a>
# [0.10.0](http://172.16.1.41:10080/TDC/flash-ui/compare/v0.9.14...v0.10.0) (2020-07-06)



<a name="0.9.14"></a>
## [0.9.14](http://172.16.1.41:10080/TDC/flash-ui/compare/v0.9.13...v0.9.14) (2020-06-30)


### Bug Fixes

* modal-scroll ([44b0625](http://172.16.1.41:10080/TDC/flash-ui/commits/44b0625))



<a name="0.9.13"></a>
## [0.9.13](http://172.16.1.41:10080/TDC/flash-ui/compare/v0.9.12...v0.9.13) (2020-06-29)


### Bug Fixes

* input-number-bug-fix ([1334e72](http://172.16.1.41:10080/TDC/flash-ui/commits/1334e72))



<a name="0.9.12"></a>
## [0.9.12](http://172.16.1.41:10080/TDC/flash-ui/compare/v0.9.11...v0.9.12) (2020-06-23)


### Bug Fixes

* select-bug-fix ([f77cbcc](http://172.16.1.41:10080/TDC/flash-ui/commits/f77cbcc))
* select-bug-fix ([b7227e4](http://172.16.1.41:10080/TDC/flash-ui/commits/b7227e4))



<a name="0.9.11"></a>
## [0.9.11](http://172.16.1.41:10080/TDC/flash-ui/compare/v0.9.10...v0.9.11) (2020-06-18)


### Features

* select-search ([1acee5a](http://172.16.1.41:10080/TDC/flash-ui/commits/1acee5a))



<a name="0.9.10"></a>
## [0.9.10](http://172.16.1.41:10080/TDC/flash-ui/compare/v0.9.9...v0.9.10) (2020-06-09)


### Features

* textarea maxlength ([6041e62](http://172.16.1.41:10080/TDC/flash-ui/commits/6041e62))



<a name="0.9.9"></a>
## [0.9.9](http://172.16.1.41:10080/TDC/flash-ui/compare/v0.9.8...v0.9.9) (2020-06-08)


### Features

* gen component api ([edb4191](http://172.16.1.41:10080/TDC/flash-ui/commits/edb4191))
* support nested submenu ([d285673](http://172.16.1.41:10080/TDC/flash-ui/commits/d285673))



<a name="0.9.8"></a>
## [0.9.8](http://172.16.1.41:10080/TDC/flash-ui/compare/v0.9.7...v0.9.8) (2020-05-28)


### Bug Fixes

* add-input-number-event ([e4ad8ee](http://172.16.1.41:10080/TDC/flash-ui/commits/e4ad8ee))



<a name="0.9.7"></a>
## [0.9.7](http://172.16.1.41:10080/TDC/flash-ui/compare/v0.9.5...v0.9.7) (2020-04-29)


### Features

* tree select change event ([3e03729](http://172.16.1.41:10080/TDC/flash-ui/commits/3e03729))



<a name="0.9.5"></a>
## [0.9.5](http://172.16.1.41:10080/TDC/flash-ui/compare/v0.9.4...v0.9.5) (2020-03-31)



<a name="0.9.4"></a>
## [0.9.4](http://172.16.1.41:10080/TDC/flash-ui/compare/v0.9.3...v0.9.4) (2020-03-30)


### Features

* datepicker lite ([2f8aef7](http://172.16.1.41:10080/TDC/flash-ui/commits/2f8aef7))



<a name="0.9.3"></a>
## [0.9.3](http://172.16.1.41:10080/TDC/flash-ui/compare/v0.9.2...v0.9.3) (2020-03-26)


### Features

* form readonly ([90e909c](http://172.16.1.41:10080/TDC/flash-ui/commits/90e909c))



<a name="0.9.2"></a>
## [0.9.2](http://172.16.1.41:10080/TDC/flash-ui/compare/v0.9.1...v0.9.2) (2020-03-24)


### Features

* datepicker-select-time ([634ea34](http://172.16.1.41:10080/TDC/flash-ui/commits/634ea34))



<a name="0.9.1"></a>
## [0.9.1](http://172.16.1.41:10080/TDC/flash-ui/compare/v0.9.0...v0.9.1) (2020-03-17)


### Bug Fixes

* dropdown context menu positioning ([b941687](http://172.16.1.41:10080/TDC/flash-ui/commits/b941687))



<a name="0.9.0"></a>
# [0.9.0](http://172.16.1.41:10080/TDC/flash-ui/compare/v0.8.12...v0.9.0) (2020-03-03)



<a name="0.8.12"></a>
## [0.8.12](http://172.16.1.41:10080/TDC/flash-ui/compare/v0.8.11...v0.8.12) (2020-02-10)


### Features

* context menu ([aef7d59](http://172.16.1.41:10080/TDC/flash-ui/commits/aef7d59))



<a name="0.8.11"></a>
## [0.8.11](http://172.16.1.41:10080/TDC/flash-ui/compare/v0.8.10...v0.8.11) (2020-01-19)



<a name="0.8.10"></a>
## [0.8.10](http://172.16.1.41:10080/TDC/flash-ui/compare/v0.8.9...v0.8.10) (2020-01-17)


### Features

* add catalog submenu icon ([559d95d](http://172.16.1.41:10080/TDC/flash-ui/commits/559d95d))



<a name="0.8.9"></a>
## [0.8.9](http://172.16.1.41:10080/TDC/flash-ui/compare/v0.8.8...v0.8.9) (2020-01-13)



<a name="0.8.8"></a>
## [0.8.8](http://172.16.1.41:10080/TDC/flash-ui/compare/v0.8.7...v0.8.8) (2019-11-11)


### Features

* select multiple file ([27fd974](http://172.16.1.41:10080/TDC/flash-ui/commits/27fd974))



<a name="0.8.7"></a>
## [0.8.7](http://172.16.1.41:10080/TDC/flash-ui/compare/v0.8.6...v0.8.7) (2019-10-22)


### Features

* popover trigger by click ([4ecaac6](http://172.16.1.41:10080/TDC/flash-ui/commits/4ecaac6))



<a name="0.8.6"></a>
## [0.8.6](http://172.16.1.41:10080/TDC/flash-ui/compare/v0.8.5...v0.8.6) (2019-10-16)


### Features

* config modal backdrop close ([95b492e](http://172.16.1.41:10080/TDC/flash-ui/commits/95b492e))



<a name="0.8.5"></a>
## [0.8.5](http://172.16.1.41:10080/TDC/flash-ui/compare/v0.8.4...v0.8.5) (2019-08-26)



<a name="0.8.4"></a>
## [0.8.4](http://172.16.1.41:10080/TDC/flash-ui/compare/v0.8.3...v0.8.4) (2019-08-23)


### Features

* tab title template ([fa6fa5e](http://172.16.1.41:10080/TDC/flash-ui/commits/fa6fa5e))



<a name="0.8.3"></a>
## [0.8.3](http://172.16.1.41:10080/TDC/flash-ui/compare/v0.8.2...v0.8.3) (2019-08-20)


### Features

* tree ([b358c9d](http://172.16.1.41:10080/TDC/flash-ui/commits/b358c9d))



<a name="0.8.2"></a>
## [0.8.2](http://172.16.1.41:10080/TDC/flash-ui/compare/v0.8.1...v0.8.2) (2019-07-31)


### Features

* pagination support custom sizes ([1a1070b](http://172.16.1.41:10080/TDC/flash-ui/commits/1a1070b))



<a name="0.8.1"></a>
## [0.8.1](http://172.16.1.41:10080/TDC/flash-ui/compare/v0.8.0...v0.8.1) (2019-06-26)


### Features

* expose input blur ([4c42a60](http://172.16.1.41:10080/TDC/flash-ui/commits/4c42a60))



<a name="0.8.0"></a>
# [0.8.0](http://172.16.1.41:10080/TDC/flash-ui/compare/v0.7.21...v0.8.0) (2019-06-12)



<a name="0.7.21"></a>
## [0.7.21](http://172.16.1.41:10080/TDC/flash-ui/compare/v0.7.20...v0.7.21) (2019-05-21)



<a name="0.7.20"></a>
## [0.7.20](http://172.16.1.41:10080/TDC/flash-ui/compare/v0.7.19...v0.7.20) (2019-05-01)


### Features

* add click dropdown config ([e879ff5](http://172.16.1.41:10080/TDC/flash-ui/commits/e879ff5))



<a name="0.7.19"></a>
## [0.7.19](http://172.16.1.41:10080/TDC/flash-ui/compare/v0.7.18...v0.7.19) (2019-05-01)


### Features

* expose dropdown visible ([b7017f8](http://172.16.1.41:10080/TDC/flash-ui/commits/b7017f8))



<a name="0.7.18"></a>
## [0.7.18](http://172.16.1.41:10080/TDC/flash-ui/compare/v0.7.16...v0.7.18) (2019-04-26)



<a name="0.7.16"></a>
## [0.7.16](http://172.16.1.41:10080/TDC/flash-ui/compare/v0.7.15...v0.7.16) (2019-04-01)



<a name="0.7.15"></a>
## [0.7.15](http://172.16.1.41:10080/TDC/flash-ui/compare/v0.7.14...v0.7.15) (2019-03-29)


### Features

* initCollapse option for menu ([18dbfc4](http://172.16.1.41:10080/TDC/flash-ui/commits/18dbfc4))



<a name="0.7.14"></a>
## [0.7.14](http://172.16.1.41:10080/TDC/flash-ui/compare/v0.7.13...v0.7.14) (2019-01-24)



<a name="0.7.13"></a>
## [0.7.13](http://172.16.1.41:10080/TDC/flash-ui/compare/v0.7.12...v0.7.13) (2019-01-22)



<a name="0.7.12"></a>
## [0.7.12](http://172.16.1.41:10080/TDC/flash-ui/compare/v0.7.11...v0.7.12) (2019-01-21)



<a name="0.7.11"></a>
## [0.7.11](http://172.16.1.41:10080/TDC/flash-ui/compare/v0.7.10...v0.7.11) (2019-01-11)


### Features

* add file dialog accept attribute ([014435d](http://172.16.1.41:10080/TDC/flash-ui/commits/014435d))



<a name="0.7.10"></a>
## [0.7.10](http://172.16.1.41:10080/TDC/flash-ui/compare/v0.7.9...v0.7.10) (2019-01-07)



<a name="0.7.9"></a>
## [0.7.9](http://172.16.1.41:10080/TDC/flash-ui/compare/v0.7.8...v0.7.9) (2019-01-07)



<a name="0.7.8"></a>
## [0.7.8](http://172.16.1.41:10080/TDC/flash-ui/compare/v0.7.7...v0.7.8) (2018-12-24)


### Bug Fixes

* **tab:** change tab key programtically sometimes fail ([deba113](http://172.16.1.41:10080/TDC/flash-ui/commits/deba113))
* misc bugs ([2ca27d1](http://172.16.1.41:10080/TDC/flash-ui/commits/2ca27d1))



<a name="0.7.7"></a>
## [0.7.7](http://172.16.1.41:10080/TDC/flash-ui/compare/v0.7.6...v0.7.7) (2018-12-07)



<a name="0.7.6"></a>
## [0.7.6](http://172.16.1.41:10080/TDC/flash-ui/compare/v0.7.5...v0.7.6) (2018-12-01)


### Bug Fixes

* radio btn ([058c38c](http://172.16.1.41:10080/TDC/flash-ui/commits/058c38c))



<a name="0.7.5"></a>
## [0.7.5](http://172.16.1.41:10080/TDC/flash-ui/compare/v0.7.4...v0.7.5) (2018-11-30)



<a name="0.7.4"></a>
## [0.7.4](http://172.16.1.41:10080/TDC/flash-ui/compare/v0.7.3...v0.7.4) (2018-11-30)



<a name="0.7.3"></a>
## [0.7.3](http://172.16.1.41:10080/TDC/flash-ui/compare/v0.7.2...v0.7.3) (2018-11-29)


### Features

* system tip model; refactor: model styles ([abd0358](http://172.16.1.41:10080/TDC/flash-ui/commits/abd0358))



<a name="0.7.2"></a>
## [0.7.2](http://172.16.1.41:10080/TDC/flash-ui/compare/v0.7.1...v0.7.2) (2018-11-28)


### Bug Fixes

* disabled btn hover color ([9b3119b](http://172.16.1.41:10080/TDC/flash-ui/commits/9b3119b))
* remove fixed height of table row ([805b9a4](http://172.16.1.41:10080/TDC/flash-ui/commits/805b9a4))


### Features

* add progress bar ([f615c16](http://172.16.1.41:10080/TDC/flash-ui/commits/f615c16))
* add prompt ([9b164d0](http://172.16.1.41:10080/TDC/flash-ui/commits/9b164d0))
* add upload ([0e4b4c7](http://172.16.1.41:10080/TDC/flash-ui/commits/0e4b4c7))



<a name="0.7.1"></a>
## [0.7.1](http://172.16.1.41:10080/TDC/flash-ui/compare/v0.7.0...v0.7.1) (2018-11-23)


### Bug Fixes

* **menu:** fix submenu cannot be selected when there is not menu-item under ([687dd40](http://172.16.1.41:10080/TDC/flash-ui/commits/687dd40))


### Features

* **menu:** make sub-menu selectable ([681eeb0](http://172.16.1.41:10080/TDC/flash-ui/commits/681eeb0))



<a name="0.7.0"></a>
# [0.7.0](http://172.16.1.41:10080/TDC/flash-ui/compare/v0.6.18...v0.7.0) (2018-11-23)


### Bug Fixes

* **select:** single mode passing array model failed ([70ddbb5](http://172.16.1.41:10080/TDC/flash-ui/commits/70ddbb5))
* **tab:** init tab select if there is pane &&  add z-index: 1 to visible tab-wrapper ([8d0dc0a](http://172.16.1.41:10080/TDC/flash-ui/commits/8d0dc0a))


### Features

* add menu, select animation & button loading ([184d8f7](http://172.16.1.41:10080/TDC/flash-ui/commits/184d8f7))
* add switch ([0e5125a](http://172.16.1.41:10080/TDC/flash-ui/commits/0e5125a))
* **doc:** add frame-window component to preview component in iframe ([4159d36](http://172.16.1.41:10080/TDC/flash-ui/commits/4159d36))
* **layout:** add layout2 for page layout ([4629f6f](http://172.16.1.41:10080/TDC/flash-ui/commits/4629f6f))
* **menu:** add float mode and dark theme ([3070fa0](http://172.16.1.41:10080/TDC/flash-ui/commits/3070fa0))
* **tab:** preserve opened tab content and detach on hide ([a686d20](http://172.16.1.41:10080/TDC/flash-ui/commits/a686d20))



<a name="0.6.18"></a>
## [0.6.18](http://172.16.1.41:10080/TDC/flash-ui/compare/v0.6.17...v0.6.18) (2018-11-09)



<a name="0.6.17"></a>
## [0.6.17](http://172.16.1.41:10080/TDC/flash-ui/compare/v0.6.16...v0.6.17) (2018-11-09)


### Features

* **select:** select component add multiple mode ([a43c764](http://172.16.1.41:10080/TDC/flash-ui/commits/a43c764))
* **tab:** add index input for tab-pane ([c8bfe6d](http://172.16.1.41:10080/TDC/flash-ui/commits/c8bfe6d))



<a name="0.6.16"></a>
## [0.6.16](http://172.16.1.41:10080/TDC/flash-ui/compare/v0.6.15...v0.6.16) (2018-11-06)


### Bug Fixes

* **form:** add validator-base include registerOnValidatorChange callback ([e202efa](http://172.16.1.41:10080/TDC/flash-ui/commits/e202efa))
* **form:** export equal validator ([9a01a2d](http://172.16.1.41:10080/TDC/flash-ui/commits/9a01a2d))



<a name="0.6.15"></a>
## [0.6.15](http://172.16.1.41:10080/TDC/flash-ui/compare/v0.6.14...v0.6.15) (2018-10-19)


### Features

* **icon:** add guanggong icons ([dc5664c](http://172.16.1.41:10080/TDC/flash-ui/commits/dc5664c))



<a name="0.6.14"></a>
## [0.6.14](http://172.16.1.41:10080/TDC/flash-ui/compare/v0.6.13...v0.6.14) (2018-09-18)


### Bug Fixes

* null number min/max ([b685f67](http://172.16.1.41:10080/TDC/flash-ui/commits/b685f67))



<a name="0.6.13"></a>
## [0.6.13](http://172.16.1.41:10080/TDC/flash-ui/compare/v0.6.12...v0.6.13) (2018-09-13)


### Bug Fixes

* **form:** validate start when there is initial value ([9bf0064](http://172.16.1.41:10080/TDC/flash-ui/commits/9bf0064))


### Features

* **select:** select allow clear ([d01103e](http://172.16.1.41:10080/TDC/flash-ui/commits/d01103e))



<a name="0.6.12"></a>
## [0.6.12](http://172.16.1.41:10080/TDC/flash-ui/compare/v0.6.11...v0.6.12) (2018-09-07)


### Bug Fixes

* **menu:** inside isSubmenuActive undefined when there is no menu items ([2823d33](http://172.16.1.41:10080/TDC/flash-ui/commits/2823d33))



<a name="0.6.11"></a>
## [0.6.11](http://172.16.1.41:10080/TDC/flash-ui/compare/v0.6.10...v0.6.11) (2018-09-06)


### Bug Fixes

* **form:** apply shouldValidate to all form item ([5ac7757](http://172.16.1.41:10080/TDC/flash-ui/commits/5ac7757))
* **menu:** menu overflow body ([6cb8986](http://172.16.1.41:10080/TDC/flash-ui/commits/6cb8986))



<a name="0.6.10"></a>
## [0.6.10](http://172.16.1.41:10080/TDC/flash-ui/compare/v0.6.9...v0.6.10) (2018-09-05)


### Features

* update form text input inner state ([bd1be0c](http://172.16.1.41:10080/TDC/flash-ui/commits/bd1be0c))



<a name="0.6.9"></a>
## [0.6.9](http://172.16.1.41:10080/TDC/flash-ui/compare/v0.6.8...v0.6.9) (2018-09-03)


### Features

* add breadcrumb ([8aef7a5](http://172.16.1.41:10080/TDC/flash-ui/commits/8aef7a5))



<a name="0.6.8"></a>
## [0.6.8](http://172.16.1.41:10080/TDC/flash-ui/compare/v0.6.7...v0.6.8) (2018-08-21)


### Features

* add select option disabled state ([3e7b36e](http://172.16.1.41:10080/TDC/flash-ui/commits/3e7b36e))



<a name="0.6.7"></a>
## [0.6.7](http://172.16.1.41:10080/TDC/flash-ui/compare/v0.6.6...v0.6.7) (2018-07-27)


### Bug Fixes

* **table:** import rxjs relative path error ([f3158e1](http://172.16.1.41:10080/TDC/flash-ui/commits/f3158e1))



<a name="0.6.6"></a>
## [0.6.6](http://172.16.1.41:10080/TDC/flash-ui/compare/v0.6.5...v0.6.6) (2018-07-27)


### Bug Fixes

* **table:** selectChange only emit selected data ([28e433a](http://172.16.1.41:10080/TDC/flash-ui/commits/28e433a))



<a name="0.6.5"></a>
## [0.6.5](http://172.16.1.41:10080/TDC/flash-ui/compare/v0.6.4...v0.6.5) (2018-07-27)


### Bug Fixes

* **table:** fix table-head checkbox select state when no row is selectable ([2cadc97](http://172.16.1.41:10080/TDC/flash-ui/commits/2cadc97))



<a name="0.6.4"></a>
## [0.6.4](http://172.16.1.41:10080/TDC/flash-ui/compare/v0.6.3...v0.6.4) (2018-07-27)


### Bug Fixes

* **table:** fix select align issue when head cell wrap ([afa6a93](http://172.16.1.41:10080/TDC/flash-ui/commits/afa6a93))



<a name="0.6.3"></a>
## [0.6.3](http://172.16.1.41:10080/TDC/flash-ui/compare/v0.6.2...v0.6.3) (2018-07-26)


### Features

* **table:** add fuiTableSelectHide for optional show tableSelect scenario ([9258d7e](http://172.16.1.41:10080/TDC/flash-ui/commits/9258d7e))



<a name="0.6.2"></a>
## [0.6.2](http://172.16.1.41:10080/TDC/flash-ui/compare/v0.6.1...v0.6.2) (2018-07-26)


### Features

* **table:** support table sort passing sortOrder onInit ([3cb85bf](http://172.16.1.41:10080/TDC/flash-ui/commits/3cb85bf))



<a name="0.6.1"></a>
## [0.6.1](http://172.16.1.41:10080/TDC/flash-ui/compare/v0.6.0...v0.6.1) (2018-07-17)


### Bug Fixes

* **pagination:** fix external pagination input disable subsequent pagination change ([562fafd](http://172.16.1.41:10080/TDC/flash-ui/commits/562fafd))



<a name="0.6.0"></a>
# [0.6.0](http://172.16.1.41:10080/TDC/flash-ui/compare/v0.4.11...v0.6.0) (2018-07-06)


### Chores

* upgrade Angular 6 ([fdfc1d0](http://172.16.1.41:10080/TDC/flash-ui/commits/fdfc1d0))


### BREAKING CHANGES

* package peerDependencies to Angular 6



<a name="0.4.11"></a>
## [0.4.11](http://172.16.1.41:10080/TDC/flash-ui/compare/v0.4.10...v0.4.11) (2018-06-29)


### Bug Fixes

* **menu:** item active fail for async render ([d9b1ff0](http://172.16.1.41:10080/TDC/flash-ui/commits/d9b1ff0))



<a name="0.4.10"></a>
## [0.4.10](http://172.16.1.41:10080/TDC/flash-ui/compare/v0.4.9...v0.4.10) (2018-06-25)


### Bug Fixes

* **overlay:** fix overlay position. close [WARP-22713](http://172.16.0.244:8080/browse/WARP-22713) ([d6feeba](http://172.16.1.41:10080/TDC/flash-ui/commits/d6feeba))



<a name="0.4.9"></a>
## [0.4.9](http://172.16.1.41:10080/TDC/flash-ui/compare/v0.4.7...v0.4.9) (2018-06-21)


### Bug Fixes

* **submenu:** hide count and suffix icon when collapsed ([1faca8f](http://172.16.1.41:10080/TDC/flash-ui/commits/1faca8f))


### Features

* **menu:** add menu component ([5542084](http://172.16.1.41:10080/TDC/flash-ui/commits/5542084))
* **search:** add placeholder input ([e4c8fac](http://172.16.1.41:10080/TDC/flash-ui/commits/e4c8fac))


### Performance Improvements

* **submenu:** activate list not exactly ([d715445](http://172.16.1.41:10080/TDC/flash-ui/commits/d715445))



<a name="0.4.7"></a>
## [0.4.7](http://172.16.1.41:10080/TDC/flash-ui/compare/v0.4.6...v0.4.7) (2018-06-12)


### Bug Fixes

* **form:** form-error component optional dep on form-item ([57e5c57](http://172.16.1.41:10080/TDC/flash-ui/commits/57e5c57))



<a name="0.4.6"></a>
## [0.4.6](http://172.16.1.41:10080/TDC/flash-ui/compare/v0.4.5...v0.4.6) (2018-06-06)


### Features

* **table:** support table head sort ([b526c9f](http://172.16.1.41:10080/TDC/flash-ui/commits/b526c9f))



<a name="0.4.5"></a>
## [0.4.5](http://172.16.1.41:10080/TDC/flash-ui/compare/v0.4.3...v0.4.5) (2018-05-30)


### Bug Fixes

* radio and checkout compatibility in IE ([f3fac6e](http://172.16.1.41:10080/TDC/flash-ui/commits/f3fac6e))


### Features

* add property link ([4910a58](http://172.16.1.41:10080/TDC/flash-ui/commits/4910a58))



<a name="0.4.3"></a>
## [0.4.3](http://172.16.1.41:10080/TDC/flash-ui/compare/v0.4.2...v0.4.3) (2018-05-07)


### Bug Fixes

* month format in ie ([a839f75](http://172.16.1.41:10080/TDC/flash-ui/commits/a839f75))



<a name="0.4.2"></a>
## [0.4.2](http://172.16.1.41:10080/TDC/flash-ui/compare/v0.4.1...v0.4.2) (2018-04-13)


### Features

* **form:** add form-item-text directive ([6c4339c](http://172.16.1.41:10080/TDC/flash-ui/commits/6c4339c))



<a name="0.4.1"></a>
## [0.4.1](http://172.16.1.41:10080/TDC/flash-ui/compare/v0.4.0...v0.4.1) (2018-04-12)


### Bug Fixes

* **form-error:** show required error first. form-item fixed margin ([d8f1a08](http://172.16.1.41:10080/TDC/flash-ui/commits/d8f1a08))


### Features

* add modal-foot component ([1f6b268](http://172.16.1.41:10080/TDC/flash-ui/commits/1f6b268))
* add pay icons ([045a0cc](http://172.16.1.41:10080/TDC/flash-ui/commits/045a0cc))



<a name="0.4.0"></a>
# [0.4.0](http://172.16.1.41:10080/TDC/flash-ui/compare/v0.3.8...v0.4.0) (2018-04-10)


### Bug Fixes

* capital to low case ([7b43ce3](http://172.16.1.41:10080/TDC/flash-ui/commits/7b43ce3))
* create __auto__ dir if not exists ([7f72437](http://172.16.1.41:10080/TDC/flash-ui/commits/7f72437))
* doc readme indent ([a4ffd6c](http://172.16.1.41:10080/TDC/flash-ui/commits/a4ffd6c))
* doc-viewer  and example-viewer lint ([d684049](http://172.16.1.41:10080/TDC/flash-ui/commits/d684049))
* export service ([9505143](http://172.16.1.41:10080/TDC/flash-ui/commits/9505143))
* remove comments ([ab38fc1](http://172.16.1.41:10080/TDC/flash-ui/commits/ab38fc1))
* router bug ([62881fc](http://172.16.1.41:10080/TDC/flash-ui/commits/62881fc))
* sass lint ([82d7af7](http://172.16.1.41:10080/TDC/flash-ui/commits/82d7af7))
* submenu dropdown ([8544f5e](http://172.16.1.41:10080/TDC/flash-ui/commits/8544f5e))


### Features

* add avatar example ([f66ac4f](http://172.16.1.41:10080/TDC/flash-ui/commits/f66ac4f))
* add button example ([9f1a476](http://172.16.1.41:10080/TDC/flash-ui/commits/9f1a476))
* add carousel example ([647c372](http://172.16.1.41:10080/TDC/flash-ui/commits/647c372))
* add copy service ([ead5906](http://172.16.1.41:10080/TDC/flash-ui/commits/ead5906))
* add datepicker example ([79deba5](http://172.16.1.41:10080/TDC/flash-ui/commits/79deba5))
* add doc readme ([4cc49e0](http://172.16.1.41:10080/TDC/flash-ui/commits/4cc49e0))
* add dropdown example ([a6bea7a](http://172.16.1.41:10080/TDC/flash-ui/commits/a6bea7a))
* add exampleviewer and docviewer ([ee5f8a5](http://172.16.1.41:10080/TDC/flash-ui/commits/ee5f8a5))
* add filtering example ([c26c3b8](http://172.16.1.41:10080/TDC/flash-ui/commits/c26c3b8))
* add form example ([d73ad77](http://172.16.1.41:10080/TDC/flash-ui/commits/d73ad77))
* add gulp build ([76cf24d](http://172.16.1.41:10080/TDC/flash-ui/commits/76cf24d))
* add gulp docs ([f754ea8](http://172.16.1.41:10080/TDC/flash-ui/commits/f754ea8))
* add icon example ([fec9bd1](http://172.16.1.41:10080/TDC/flash-ui/commits/fec9bd1))
* add input example ([a5af11c](http://172.16.1.41:10080/TDC/flash-ui/commits/a5af11c))
* add loading example ([e4aac5f](http://172.16.1.41:10080/TDC/flash-ui/commits/e4aac5f))
* add markdown css ([6a31a48](http://172.16.1.41:10080/TDC/flash-ui/commits/6a31a48))
* add markdown css config to angular-cli ([900cb62](http://172.16.1.41:10080/TDC/flash-ui/commits/900cb62))
* add message example ([b9c5d13](http://172.16.1.41:10080/TDC/flash-ui/commits/b9c5d13))
* add modal example ([5e81b6b](http://172.16.1.41:10080/TDC/flash-ui/commits/5e81b6b))
* add more examples ([e89551c](http://172.16.1.41:10080/TDC/flash-ui/commits/e89551c))
* add pagination example ([1c4fd20](http://172.16.1.41:10080/TDC/flash-ui/commits/1c4fd20))
* add popover example ([3db51c3](http://172.16.1.41:10080/TDC/flash-ui/commits/3db51c3))
* add prework to delete last compiled output ([ae18ee7](http://172.16.1.41:10080/TDC/flash-ui/commits/ae18ee7))
* add property example ([19a1898](http://172.16.1.41:10080/TDC/flash-ui/commits/19a1898))
* add select example ([c238639](http://172.16.1.41:10080/TDC/flash-ui/commits/c238639))
* add submenu collapse event ([be9a4af](http://172.16.1.41:10080/TDC/flash-ui/commits/be9a4af))
* add submenu example ([a202965](http://172.16.1.41:10080/TDC/flash-ui/commits/a202965))
* add tab example ([a7ae5c4](http://172.16.1.41:10080/TDC/flash-ui/commits/a7ae5c4))
* add table example ([d9abd69](http://172.16.1.41:10080/TDC/flash-ui/commits/d9abd69))
* add test case ([ddd79b0](http://172.16.1.41:10080/TDC/flash-ui/commits/ddd79b0))
* allow table scroll ([d001ab2](http://172.16.1.41:10080/TDC/flash-ui/commits/d001ab2))
* fix avatar md ([ead76b4](http://172.16.1.41:10080/TDC/flash-ui/commits/ead76b4))
* handle not in fui station ([42e6ef3](http://172.16.1.41:10080/TDC/flash-ui/commits/42e6ef3))
* home page test ([84d8490](http://172.16.1.41:10080/TDC/flash-ui/commits/84d8490))
* improt some modules ([27b4715](http://172.16.1.41:10080/TDC/flash-ui/commits/27b4715))
* md example ([60260e4](http://172.16.1.41:10080/TDC/flash-ui/commits/60260e4))
* modify the router logic ([73e3b0a](http://172.16.1.41:10080/TDC/flash-ui/commits/73e3b0a))



<a name="0.3.7"></a>
## [0.3.7](http://172.16.1.41:10080/TDC/flash-ui/compare/v0.3.6...v0.3.7) (2018-03-29)


### Features

* add status icon to submenu component ([0568889](http://172.16.1.41:10080/TDC/flash-ui/commits/0568889))



<a name="0.3.6"></a>
## [0.3.6](http://172.16.1.41:10080/TDC/flash-ui/compare/v0.3.5...v0.3.6) (2018-03-22)


### Features

* add modal error ([11cf789](http://172.16.1.41:10080/TDC/flash-ui/commits/11cf789))



<a name="0.3.5"></a>
## [0.3.5](http://172.16.1.41:10080/TDC/flash-ui/compare/v0.3.4...v0.3.5) (2018-03-20)


### Bug Fixes

* duplicate i18n service instance ([fe1e69d](http://172.16.1.41:10080/TDC/flash-ui/commits/fe1e69d))
* repoint docker registry space ([9a42543](http://172.16.1.41:10080/TDC/flash-ui/commits/9a42543))



<a name="0.3.4"></a>
## [0.3.4](http://172.16.1.41:10080/TDC/flash-ui/compare/v0.3.3...v0.3.4) (2018-02-12)


### Bug Fixes

* add publish:fui script to fix publish ([9d2cc90](http://172.16.1.41:10080/TDC/flash-ui/commits/9d2cc90))
* flex in ie ([92ba6fa](http://172.16.1.41:10080/TDC/flash-ui/commits/92ba6fa))



<a name="0.3.3"></a>
## [0.3.3](http://172.16.1.41:10080/TDC/flash-ui/compare/v0.3.2...v0.3.3) (2018-02-07)


### Bug Fixes

* add i18n for apiError ([cd3a702](http://172.16.1.41:10080/TDC/flash-ui/commits/cd3a702))
* prepublish -> prepublishOnly ([91951e2](http://172.16.1.41:10080/TDC/flash-ui/commits/91951e2))



<a name="0.3.2"></a>
## [0.3.2](http://172.16.1.41:10080/TDC/flash-ui/compare/v0.3.1...v0.3.2) (2017-12-18)


### Bug Fixes

* rollback public-api ([0531617](http://172.16.1.41:10080/TDC/flash-ui/commits/0531617))
* **carousel:** not calculate slide-track width ([e3410b6](http://172.16.1.41:10080/TDC/flash-ui/commits/e3410b6))


### Features

* add host-width-listener ([c9f5787](http://172.16.1.41:10080/TDC/flash-ui/commits/c9f5787))
* add overlay shared ([e2fa3ec](http://172.16.1.41:10080/TDC/flash-ui/commits/e2fa3ec))



<a name="0.3.1"></a>
## [0.3.1](http://172.16.1.41:10080/TDC/flash-ui/compare/v0.2.1...v0.3.1) (2017-11-24)


### Bug Fixes

* build theme.css remove import ([585b85e](http://172.16.1.41:10080/TDC/flash-ui/commits/585b85e))
* relocate popover ([fc3d0dc](http://172.16.1.41:10080/TDC/flash-ui/commits/fc3d0dc))


### Features

* add doc app/build mode ([8f725f8](http://172.16.1.41:10080/TDC/flash-ui/commits/8f725f8))
* build fui before run doc ([59b8b19](http://172.16.1.41:10080/TDC/flash-ui/commits/59b8b19))
* export independent mock ([a6528fe](http://172.16.1.41:10080/TDC/flash-ui/commits/a6528fe))
* export mock module under a namespace ([bb263d2](http://172.16.1.41:10080/TDC/flash-ui/commits/bb263d2))
* popover ([45d1ebb](http://172.16.1.41:10080/TDC/flash-ui/commits/45d1ebb))
* update README.md 0.3.0 ([b0237ed](http://172.16.1.41:10080/TDC/flash-ui/commits/b0237ed))



<a name="0.2.1"></a>
## [0.2.1](http://172.16.1.41:10080/TDC/flash-ui/compare/0.2.1...v0.2.1) (2017-11-16)



<a name="0.2.0"></a>
# [0.2.0](http://172.16.1.41:10080/TDC/flash-ui/compare/v0.1.0...0.2.0) (2017-11-14)


### Features

* export mock module and translate service ([09b68d8](http://172.16.1.41:10080/TDC/flash-ui/commits/09b68d8))



<a name="0.1.0"></a>
# [0.1.0](http://172.16.1.41:10080/TDC/flash-ui/compare/0.1.0...v0.1.0) (2017-11-07)


### Bug Fixes

* remove img import ([db54ccf](http://172.16.1.41:10080/TDC/flash-ui/commits/db54ccf))
* util import path ([d83b8eb](http://172.16.1.41:10080/TDC/flash-ui/commits/d83b8eb))



