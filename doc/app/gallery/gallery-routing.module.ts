import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { GalleryComponent } from './gallery.component';
import { ComponentViewerComponent } from './component-viewer/component-viewer.component';
import { GaDashboardComponent } from './ga-dashboard/ga-dashboard.component';

const routes: Routes = [
  {
    path: '',
    component: GalleryComponent,
    children: [
      {
        path: '',
        component: GaDashboardComponent,
      },
      {
        path: ':path',
        children: [
          {
            path: '**',
            component: ComponentViewerComponent,
          },
        ],
      },
    ],
  },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
  ],
  exports: [
    RouterModule,
  ],
})
export class GalleryRoutingModule { }
