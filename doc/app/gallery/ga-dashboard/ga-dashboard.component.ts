import { Component, ElementRef, HostBinding } from '@angular/core';
import { DocToc } from 'app/shared';

@Component({
  templateUrl: './ga-dashboard.component.html',
  styleUrls: ['./ga-dashboard.component.sass'],
})
export class GaDashboardComponent {
  @HostBinding('class') hostClass = 'fui-layout2-content';

  tocs: DocToc[] = [];

  constructor(
    public elem: ElementRef,
  ) {}

  collectToc() {
    this.tocs = [];

    const headingElements = this.elem.nativeElement.querySelectorAll(`h1, h2`);
    Array.from(headingElements).forEach((element: Element) => {
      const headingId = element.getAttribute('id');
      this.tocs.push(new DocToc(decodeURIComponent(headingId), headingId));
    });
  }

  onContentLoaded() {
    this.collectToc();
  }
}
