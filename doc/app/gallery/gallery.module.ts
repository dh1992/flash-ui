import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import {
  FuiModule,
} from 'flash-ui';

import { GalleryRoutingModule } from './gallery-routing.module';
import { GalleryComponent } from './gallery.component';

/**
 * 子路由组件/模块
 */
import { ComponentViewerComponent } from './component-viewer/component-viewer.component';
import { DocViewerModule } from '../shared/doc-viewer/doc-viewer.module';
import { ContributorsModule } from '../shared/contributors/contributors.module';
import { GaDashboardComponent } from './ga-dashboard/ga-dashboard.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    FuiModule,
    GalleryRoutingModule,
    DocViewerModule,
    ContributorsModule,
  ],
  declarations: [
    GalleryComponent,
    ComponentViewerComponent,
    GaDashboardComponent,
  ],
  providers: [
  ],
  entryComponents: [
  ],
})
export class GalleryModule { }
