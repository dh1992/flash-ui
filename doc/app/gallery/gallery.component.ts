import { Component, OnInit, HostBinding } from '@angular/core';

import autoNavs from '../__auto__/navs';

@Component({
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.sass'],
})
export class GalleryComponent implements OnInit {
  @HostBinding('class') hostClass = 'fui-layout2 fui-layout2-content';

  navs = autoNavs.sort();

  constructor() {
  }

  ngOnInit() {

  }
}
