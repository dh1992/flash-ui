import { Component } from '@angular/core';

@Component({
  selector: 'fui-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass'],
})
export class AppComponent {
  constructor() {
  }
}
