export class DocToc {
  name: string;
  id: string;
  class?: string = '';
  type?: string = '';
  children ? = [];

  constructor(name: string, id: string) {
    this.name = name;
    this.id = id;
  }
}

export const SRC_KINDS = ['HTML', 'TS', 'CSS'] as const;

export type SrcKind = typeof SRC_KINDS[number];

export class SampleCode {
  HTML = '';

  TS = '';

  CSS = '';

  update(srcKind: SrcKind, value: string) {
    this[srcKind] = value || this[srcKind];
  }

  trimCode() {
    SRC_KINDS.forEach((src) => (this[src] || '').trim());
  }

  empty(srcKind: SrcKind): boolean {
    return !this[srcKind];
  }

  notEmpty(srcKind: SrcKind): boolean {
    return !this.empty(srcKind);
  }
}
