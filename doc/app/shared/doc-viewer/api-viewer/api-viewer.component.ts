import {
  Component,
  OnInit,
  Input,
  OnChanges,
} from '@angular/core';
import apis from '../../../__auto__/api';

@Component({
  selector: 'fui-api-viewer',
  templateUrl: './api-viewer.component.html',
  styleUrls: ['./api-viewer.component.sass'],
})
export class ApiViewerComponent implements OnInit, OnChanges {

  @Input() example: string;

  apis: any[] = [];

  constructor() { }

  ngOnInit(): void {
    if (this.example) {
      this.apis = apis[this.example];
    }
  }

  ngOnChanges() {

  }

  getPropertyApis(propertyApis) {
    return propertyApis.filter((api) => api.type !== 'function');
  }

  getMethodApis(methodApis) {
    return methodApis.filter((api) => api.type === 'function');
  }

  getViewId(name: string) {
    return encodeURIComponent(name);
  }
}
