import { HostBinding, Input } from '@angular/core';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'fui-type-label',
  templateUrl: './type-label.component.html',
  styleUrls: ['./type-label.component.sass'],
})
export class TypeLabelComponent implements OnInit {
  @HostBinding('class') hostClass = 'fui-type-label';

  @Input() type: string;

  @Input() short: boolean = false;

  constructor() {}

  ngOnInit(): void {}
}
