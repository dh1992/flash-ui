import { Component, Input, ElementRef, ViewChild, AfterViewInit } from '@angular/core';

const themeSheet = require('../../../../../src/styles/theme.sass');

@Component({
  selector: 'fui-frame-window',
  templateUrl: './frame-window.component.html',
  styleUrls: ['./frame-window.component.sass'],
})
export class FrameWindowComponent implements AfterViewInit {
  @Input() width = '100%';

  @Input() height = '400px';

  @Input() component;

  @ViewChild('frame') frameElement: ElementRef;

  @ViewChild('content') contentElement: ElementRef;

  constructor() { }

  ngAfterViewInit() {
    this.insertDocument();
  }

  insertDocument() {
    const {document} = (this.frameElement.nativeElement as HTMLIFrameElement).contentWindow;
    const element: HTMLElement = this.contentElement.nativeElement;
    document.open();
    document.write(element.innerHTML);
    document.write(`<style>${themeSheet.default}</style>`);
    document.close();

    element.remove();
  }
}
