import { PortalModule } from '@angular/cdk/portal';
import { CdkTreeModule } from '@angular/cdk/tree';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import {
  FuiModule,
} from 'flash-ui';

import { ApiViewerComponent } from './api-viewer/api-viewer.component';
import { ExampleViewerComponent } from './example-viewer/example-viewer.component';
import { DocViewerComponent } from './doc-viewer.component';
import { FrameWindowComponent } from './frame-window/frame-window.component';

import { CopierService } from '../copier/copier.service';
import { ModelViewerComponent } from './model-viewer/model-viewer.component';
import { TypeLabelComponent } from './type-label/type-label.component';
import { CompileModule } from 'app/shared/compiler';

// ExampleViewer is included in the DocViewerModule because they have a circular dependency.
@NgModule({
  imports: [
    FuiModule,
    CommonModule,
    FormsModule,
    PortalModule,
    CdkTreeModule,
    CompileModule,
  ],
  providers: [CopierService],
  declarations: [
    DocViewerComponent,
    ApiViewerComponent,
    ExampleViewerComponent,
    FrameWindowComponent,
    ModelViewerComponent,
    TypeLabelComponent,
  ],
  entryComponents: [ExampleViewerComponent, ApiViewerComponent, ModelViewerComponent],
  exports: [
    DocViewerComponent,
    ApiViewerComponent,
    ExampleViewerComponent,
    FrameWindowComponent,
    TypeLabelComponent,
  ],
})
export class DocViewerModule { }
