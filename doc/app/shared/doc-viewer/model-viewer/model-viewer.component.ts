import { Component, OnInit, Input, AfterViewInit, ElementRef } from '@angular/core';
import hljs from 'highlight.js/lib/highlight';
import typescript from 'highlight.js/lib/languages/typescript';
hljs.registerLanguage('typescript', typescript);

import models from '../../../__auto__/model';

@Component({
  selector: 'fui-model-viewer',
  templateUrl: './model-viewer.component.html',
  styleUrls: ['./model-viewer.component.sass'],
})
export class ModelViewerComponent implements OnInit, AfterViewInit {

  @Input() example: string;

  models: any[] = [];

  constructor(
    private elem: ElementRef,
  ) { }

  ngOnInit(): void {
    if (this.example) {
      this.models = models[this.example];

      this.models.forEach((model) => {
        model.content = model.content.replace(/    /g, '  ');
      });
    }
  }

  ngAfterViewInit() {
    this.elem.nativeElement.querySelectorAll('pre code').forEach((block) => {
      hljs.highlightBlock(block);
    });
  }

  getViewId(name: string) {
    return encodeURIComponent(name);
  }
}
