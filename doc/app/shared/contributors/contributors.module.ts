import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';

import {
  FuiModule,
} from 'flash-ui';

import { ContributorsComponent } from './contributors.component';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    FuiModule,
  ],
  declarations: [ContributorsComponent],
  exports: [ContributorsComponent],
})
export class ContributorsModule { }
