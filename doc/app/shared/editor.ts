export const EDITOR_THEME = {
  base: 'vs',
  inherit: true,
  rules: [{token: ''}],
} as any;

export const EDITOR_OPTIONS = {
  fontFamily: 'Consolas, "Courier New", monospace',
  fontSize: 14,
  tabSize: 2,
  lineNumbersMinChars: 3,
  automaticLayout: true,
  minimap: {
    enabled: false,
  },
};

export const FORMAT_MAPPING = {
  TS: 'typescript',
  HTML: 'html',
  CSS: 'css',
};
