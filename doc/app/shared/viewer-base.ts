import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  templateUrl: '',
})
export class ViewerBaseComponent {

  fragment: string;

  constructor(
    protected route: ActivatedRoute,
  ) {
    this.route.fragment.subscribe((fragment) => {
      this.fragment = fragment;
      this.scrollToView();
    });
  }

  scrollToView() {
    if (!this.fragment) {
      return;
    }

    const element = document.getElementById(this.fragment);
    if (element) {
      element.scrollIntoView();
    }
  }
}
