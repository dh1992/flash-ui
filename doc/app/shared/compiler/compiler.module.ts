import { CompilerComponent } from './compiler.component';

import { NgModule } from '@angular/core';

import { CommonModule } from '@angular/common';

@NgModule({
  imports: [CommonModule],
  declarations: [CompilerComponent],
  providers: [],
  exports: [CompilerComponent],
  entryComponents: [],
})
export class CompileModule {}
