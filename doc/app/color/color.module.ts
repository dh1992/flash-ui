import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {
  FuiModule,
} from 'flash-ui';

import { ColorComponent } from './color.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    FuiModule,
  ],
  declarations: [ColorComponent],
})
export class ColorModule { }
