import { Component, OnInit, HostBinding } from '@angular/core';

import autoGuides from '../__auto__/guides';

@Component({
  selector: 'fui-guides',
  templateUrl: './guides.component.html',
  styleUrls: ['./guides.component.sass'],
})
export class GuidesComponent implements OnInit {
  @HostBinding('class') hostClass = 'fui-layout2 fui-layout2-content';

  guides = autoGuides;

  constructor() { }

  ngOnInit(): void {
  }

}
