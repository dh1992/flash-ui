import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {
  FuiModule,
} from 'flash-ui';

import { DocViewerModule } from '../shared/doc-viewer/doc-viewer.module';
import { GuidesRoutingModule } from './guides-routing.module';
import { GuidesComponent } from './guides.component';
import { GuidesViewerComponent } from './guides-viewer/guides-viewer.component';

@NgModule({
  imports: [
    CommonModule,
    FuiModule,
    DocViewerModule,
    GuidesRoutingModule,
  ],
  declarations: [
    GuidesComponent,
    GuidesViewerComponent,
  ],
})
export class GuidesModule { }
