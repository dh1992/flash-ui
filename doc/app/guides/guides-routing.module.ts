import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { GuidesComponent } from './guides.component';
import { GuidesViewerComponent } from './guides-viewer/guides-viewer.component';

const routes: Routes = [
  {
    path: '',
    component: GuidesComponent,
    children: [
      {
        path: ':path',
        children: [
          {
            path: '**',
            component: GuidesViewerComponent,
          },
        ],
      },
      {
        path: '',
        redirectTo: 'README',
        pathMatch: 'full',
      },
    ],
  },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
  ],
  exports: [
    RouterModule,
  ],
})
export class GuidesRoutingModule { }
