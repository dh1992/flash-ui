import { Component, HostBinding, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import Fuse from 'fuse.js';

class SearchContextItem {
  title: string;
  url: string;
  text: string;
  id: string;
  context: SearchContextItem;
}

class SearchResultMatch {
  indices: number[][];
  key: string;
  value: string;
  html: string;
}

class SearchResult {
  item: SearchContextItem;
  matches: SearchResultMatch[];
}

const SEARCH_RESULT_NUMBER = 10;

const SEARCH_RESULT_TEXT_ELLIPSIS = 10;

@Component({
  selector: 'fui-layout-search',
  templateUrl: './layout-search.component.html',
  styleUrls: ['./layout-search.component.sass'],
})
export class LayoutSearchComponent implements OnInit {
  @HostBinding('class.fui-layout-search') hostClass = true;

  searchContext: SearchContextItem[] = [];
  searchFuse: any;
  searchFuseOption = {
    includeMatches: true,
    minMatchCharLength: 2,
    keys: [
      'text',
    ],
  };
  searchFilter: string;
  searchResults: SearchResult[] = [];
  showSearchResults = false;

  constructor(
    private http: HttpClient,
  ) { }

  ngOnInit(): void {
    // this.getSearchContext();
  }

  getSearchContext() {
    this.http.get(`assets/search/search-context.json?t=${new Date().getTime()}`)
    .subscribe((data) => {
      this.searchContext = data as SearchContextItem[];
      this.searchContext.forEach((item) => {
        item.text = item.text.replace('<', '&lt;').replace('>', '&gt;');
        if (item.context) {
          item.context.text = item.context.text.replace('<', '&lt;').replace('>', '&gt;');
        }
      });
      this.searchFuse = new Fuse(this.searchContext as any[], this.searchFuseOption);
    }, () => {
      console.warn('No search context found.');
    });
  }

  onSearch() {
    this.searchResults = [];
    if (this.searchResults.length === 0 && !this.searchFilter) {
      this.showSearchResults = false;
    }

    if (this.searchFuse && this.searchFilter) {
      this.searchResults = this.searchFuse.search(this.searchFilter).slice(0, SEARCH_RESULT_NUMBER);
      this.searchResults.forEach((result) => {
        this.highlightResult(result);
      });

      if (this.searchResults.length > 0) {
        this.showSearchResults = true;
      } else {
        this.showSearchResults = false;
      }
    }
  }

  private highlightResult(result: SearchResult) {
    const match = result.matches[0];
    const isText = !result.item.id;

    /**
     * Match indices example: [[4, 5], [7, 9], [20 30]]
     */
    const valueParts = [];
    for (let index = 0; index < match.indices.length; index ++) {
      const indices = match.indices[index];

      let startIndex = 0;
      if (index === 0) {
        if (isText) { // For text, ellipsis start if too long
          startIndex = Math.max(0, indices[0] - SEARCH_RESULT_TEXT_ELLIPSIS);
          if (indices[0] + 1 > SEARCH_RESULT_TEXT_ELLIPSIS) {
            valueParts.push('...');
          }
        }
      } else {
        startIndex = match.indices[index - 1][1] + 1;
      }

      // Push value parts into stack, including highlight token
      valueParts.push(...[
        match.value.slice(startIndex, indices[0]),
        '<span class="match-token">',
        match.value.slice(indices[0], indices[1] + 1),
        '</span>',
      ]);
    }

    const lastIndex = match.indices[match.indices.length - 1][1];
    if (isText) { // For text, ellipsis end if too long
      valueParts.push(match.value.slice(lastIndex + 1, lastIndex + SEARCH_RESULT_TEXT_ELLIPSIS + 1));
      if (match.value.length > lastIndex + SEARCH_RESULT_TEXT_ELLIPSIS + 1) {
        valueParts.push('...');
      }
    } else {
      valueParts.push(match.value.slice(lastIndex + 1));
    }

    match.html = valueParts.join('');
  }
}
