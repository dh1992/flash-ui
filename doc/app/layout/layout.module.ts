import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import {
  FuiModule,
} from 'flash-ui';

import { LayoutComponent } from './layout.component';
import { LayoutSearchComponent } from './layout-search/layout-search.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    HttpClientModule,
    RouterModule,
    FuiModule,
  ],
  declarations: [LayoutComponent, LayoutSearchComponent],
})
export class LayoutModule { }
