import { Component, OnInit, HostBinding } from '@angular/core';
import { fuiTranslateService } from 'flash-ui';

import { StyleManagerService } from '../shared/style-manager';

@Component({
  selector: 'fui-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.sass'],
})
export class LayoutComponent implements OnInit {
  @HostBinding('class') hostClass = 'fui-layout2 vertical';

  themes = [
    {key: 'white', name: 'White', isDefault: true},
    {key: 'black', name: 'Black'},
    {key: 'blue', name: 'Blue'},
  ];
  currentTheme = 'white';

  langs = [
    'en_US',
    'zh_CN',
  ];
  currentLang = 'zh_CN';

  constructor(
    private translate: fuiTranslateService,
    private styleManager: StyleManagerService,
  ) {
    this.currentLang = translate.lang;
  }

  ngOnInit() {
    const themeKey = this.styleManager.getStoredThemeKey();
    if (themeKey && themeKey !== this.currentTheme) {
      const theme = this.themes.find((_theme) => _theme.key === themeKey);
      if (theme) {
        this.chooseTheme(theme);
      }
    }
  }

  changeLang(lang) {
    this.translate.setLocale(lang);
    this.currentLang = lang;
  }

  toggleLanguage() {
    const langIndex = (this.langs.indexOf(this.currentLang) + 1) % this.langs.length;
    this.currentLang = this.langs[langIndex];
    this.translate.setLocale(this.currentLang);
  }

  chooseTheme(theme) {
    this.currentTheme = theme.key;
    if (theme.isDefault) {
      this.styleManager.removeStyle('theme');
    } else {
      this.styleManager.setStyle('theme', `assets/${theme.key}.css`);
    }
    this.styleManager.storeTheme(theme.key);
  }
}
