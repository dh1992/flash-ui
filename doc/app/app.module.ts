import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FuiModule } from 'flash-ui';

import { DocViewerModule } from './shared/doc-viewer/doc-viewer.module';
import { StyleManagerService } from './shared/style-manager';
import { ExampleModule } from './__auto__/examples-module';
import { LayoutModule } from './layout/layout.module';
import { ColorModule } from './color/color.module';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FuiModule,
    AppRoutingModule,
    ExampleModule,
    DocViewerModule,
    HttpClientModule,
    LayoutModule,
    ColorModule,
  ],
  declarations: [
    AppComponent,
  ],
  providers: [
    StyleManagerService,
  ],
  bootstrap: [AppComponent],
})
export class AppModule { }
