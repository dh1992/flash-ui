import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { LayoutComponent } from './layout/layout.component';
import { ColorComponent } from './color/color.component';

const routes: Routes = [
  {
    path: '',
    component: LayoutComponent,
    children: [
      {
        path: 'gallery',
        loadChildren: () => import('./gallery/gallery.module').then(m => m.GalleryModule),
      },
      {
        path: 'guides',
        loadChildren: () => import('./guides/guides.module').then(m => m.GuidesModule),
      },
      {
        path: 'color',
        component: ColorComponent,
      },
      {
        path: '',
        redirectTo: 'gallery',
        pathMatch: 'full',
      },
    ],
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { useHash: false, relativeLinkResolution: 'legacy' }),
  ],
  exports: [
    RouterModule,
  ],
})
export class AppRoutingModule { }
