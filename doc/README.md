### gulp构建流程
![](http://7xpser.com1.z0.glb.clouddn.com//18-3-12/29107117.jpg)

### 动态插入markdown和example原理
依赖两个组件：
#### docviewer
通过`angular`的动态插组件的方式，根据路由去请求之前打包构建的html文件（assets/markdown）
#### exampleviewer
通过`angular/cdk`的`ComponentPortal`

PS. 两个组件存放在`doc/app/shared`下


### 文档工作流程
1. 添加文档。在`src/fui`不同的组件下创建跟目录一样名字的md文件，然后写文档，可以在md文件中插入`<!-- example(avatar-icons) -->`引入对应的example。
2. 编写example。example必须提前写好存放在`doc/fui-examples`且文件名必须跟md文件引入名字对应。需遵循以下规则。
 - 一个example只有一份`ts`, `html`, `css`文件，因为`example-viewer`只能消耗一份组件文件。如需定义辅助组件，则在`ts`文件里面定义，利用`template`和`styles`内嵌inline模板和样式。参照`table-nesting`组件。
 - 组件命名考虑名称冲突。因为允许一个ts文件里面定义多组件，推荐使用前缀命名。比如`table-nesting`组件里辅助组件命名为`table-nesting-sublist`，`table-nesting-id`等。
3. 构建命令`npm run gulp:docs`
