import { Component, OnInit } from '@angular/core';

/**
 * @title Upload Error
 */
@Component({
  templateUrl: 'upload-error.component.html',
  styleUrls: ['upload-error.component.css'],
})
export class UploadErrorComponent implements OnInit {
  constructor() { }

  ngOnInit() {
  }
}
