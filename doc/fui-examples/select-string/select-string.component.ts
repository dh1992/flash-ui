import { Component } from '@angular/core';

/**
 * @title String Options
 */
@Component({
  templateUrl: 'select-string.component.html',
})
export class SelectStringComponent {
  stringOptions = ['hello', 'world'];

  stringOption = 'hello';

  constructor(
  ) {
  }
}
