import { Component, OnInit } from '@angular/core';

/**
 * @title Property Overview
 */
@Component({
  templateUrl: 'property-overview.component.html',
  styleUrls: ['property-overview.component.css'],
})
export class PropertyOverviewComponent implements OnInit {
  dpOption: any;
  properties = [
    {
      icon: 'pile',
      value: '商务部仓储项目',
      label: '项目项目项目',
    },
    {
      icon: 'pile',
      value: '商务部仓储项目',
      label: '项目',
    },
    {
      icon: 'pile',
      value: '商务部仓储项目',
      label: '项目',
    },
    {
      icon: 'pile',
      value: '商务部仓储项目',
      label: '项目',
    },
    {
      icon: 'pile',
      value: '商务部仓储项目',
      label: '项目',
    },
    {
      icon: 'user',
      value: 'Monica',
      label: '创建者',
    },
    {
      icon: 'cny-circle',
      value: '待付费',
      label: '付费状态',
      color: 'danger',
    },
    {
      icon: 'share',
      value: '跳转到首页',
      label: '链接链接',
      link: '/',
    },
  ];

  constructor() { }

  ngOnInit() {
  }

}
