import { Component, OnInit } from '@angular/core';

/**
 * @title Menu Custom
 */
@Component({
  selector: 'fui-menu-custom',
  templateUrl: './menu-custom.component.html',
  styleUrls: ['./menu-custom.component.css'],
})
export class MenuCustomComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
