import { Component, OnInit } from '@angular/core';

/**
 * @title Dropdown with title
 */
@Component({
  templateUrl: './dropdown-title.component.html',
})
export class DropdownTitleComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
