import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  HostBinding,
  HostListener } from '@angular/core';

/**
 * @title Nesting Table
 */
@Component({
  templateUrl: 'table-nesting.component.html',
  styleUrls: ['table-nesting.component.css'],
})
export class TableNestingComponent {
  tableData = [
    { id: 1, name: 'cat', type: 'limb', desc: 'miao' },
    { id: 2, name: 'dog', type: 'limb', desc: 'wuf' },
    { id: 3, name: 'fish', type: 'no-limb', desc: 'blue' },
  ];

  loading = true;

  loadingTableData: any[];

  constructor() { }

  selectChange(data) {
    console.log('selectChange', data);
  }

  onDelete(record) {
    console.log('delete', JSON.stringify(record));
  }

  toggleSublist(datum) {
    datum.showSublist = !datum.showSublist;
  }

  startLoading() {
    this.loading = true;
  }

  stopLoading() {
    this.loading = false;
    this.loadingTableData = [
      { name: 'red', alias: '红色' },
      { name: 'blue', alias: '蓝色' },
    ];
  }
}

@Component({
  selector: 'fui-table-nesting-id',
  template: `
  <div class="flex-align-center cursor-pointer">
    <svg fuiIcon="caret-right" [ngClass]="{open: open}"></svg>
    <ng-content></ng-content>
  </div>
  `,
  styles: [`
  svg {
    margin-right: .25rem;
    transition: transform .1s;
  }

  svg.open {
    transform: rotate(90deg);
  }
  `],
})
export class TableNestingIdComponent {
  @Input() open = false;
  @Output() openChange = new EventEmitter();
  @HostBinding('class.fui-table-cell') true;
  @HostListener('click') onclick() {
    this.openChange.emit(!this.open);
  }

  get icon() {
    return this.open ? 'caret-down' : 'caret-right';
  }
}

@Component({
  selector: 'fui-table-nesting-sublist',
  template: `
  <p>
    table sublist works!
  </p>
  `,
})
export class TableNestingSublistComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}

