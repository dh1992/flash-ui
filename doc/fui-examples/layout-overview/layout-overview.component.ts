import { Component } from '@angular/core';

/**
 * @title Layout Overview
 * @iframe
 */
@Component({
  templateUrl: './layout-overview.component.html',
  styleUrls: ['./layout-overview.component.css'],
})
export class LayoutOverviewComponent {
}
