import { Component, OnInit } from '@angular/core';

/**
 * @title Upload Overview
 */
@Component({
  templateUrl: 'upload-overview.component.html',
  styleUrls: ['upload-overview.component.css'],
})
export class UploadOverviewComponent implements OnInit {
  constructor() { }

  ngOnInit() {
  }
}
