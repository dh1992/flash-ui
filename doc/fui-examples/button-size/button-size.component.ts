import { Component, OnInit } from '@angular/core';

/**
 * @title Different Size Button
 */
@Component({
  templateUrl: './button-size.component.html',
  styleUrls: ['./button-size.component.css'],
})
export class ButtonSizeComponent implements OnInit {
  btnColors = ['primary', 'success', 'danger', 'warning', 'default'];

  constructor() { }

  ngOnInit() {
  }

}
