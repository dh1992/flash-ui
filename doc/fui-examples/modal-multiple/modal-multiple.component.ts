import { Component, Injector, Inject, Injectable } from '@angular/core';

import { fuiModalService, fuiModalRef, fui_MODAL_DATA } from 'flash-ui';

@Component({
  template: `<button
    style="margin: 8px 20px 8px 0"
    fuiBtn="primary"
    (click)="openChildModal()"
  >Child Modal</button>`,
})
export class ModalMultipleExmapleComponent {
  constructor(
    private modal: fuiModalService,
  ) { }

  openChildModal() {
    this.modal.info({
      title: 'hello',
      message: 'world',
    });
  }

}

/**
 * @title Mutiple Modal
 */
@Component({
  templateUrl: 'modal-multiple.component.html',
  styleUrls: ['modal-multiple.component.css'],
})
export class ModalMultipleComponent {
  constructor(
    private modal: fuiModalService,
    private injector: Injector,
  ) { }

  openMultipleModal() {
    this.modal.open(ModalMultipleExmapleComponent, {
      title: 'Multiple Modal',
      injector: this.injector,
    });
  }
}
