import { Component, OnInit } from '@angular/core';

/**
 * @title Datepicker Lite Overview
 */
@Component({
  templateUrl: './datepicker-lite-overview.component.html',
  styleUrls: ['./datepicker-lite-overview.component.css'],
})
export class DatepickerLiteOverviewComponent implements OnInit {
  date = null;
  dateTime = null;
  presetDateTime = '2018-08-09T02:39:59.530Z';
  customDateTime = null;

  constructor() { }

  ngOnInit() {
  }
}
