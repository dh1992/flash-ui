import { Component, OnInit } from '@angular/core';
import { fuiMessageService } from 'flash-ui';
import { CopierService } from 'app/shared/copier/copier.service';

/**
 * @title Icon List
 */
@Component({
  templateUrl: './icon-overview.component.html',
  styleUrls: ['./icon-overview.component.css'],
})
export class IconOverviewComponent implements OnInit {
  icons: string[] = [];
  statuses = ['RUNNING', 'PREPARING', 'STOPPED', 'DELETED'];

  constructor(
    private copier: CopierService,
    private message: fuiMessageService,
  ) { }

  ngOnInit() {
    this.getAllIcon();
  }

  getAllIcon() {
    const iconSymbol = document.querySelector('fui-icon-symbol');
    const symbols = iconSymbol.querySelectorAll('symbol');
    for (let i = 0; i < symbols.length; i ++) {
      this.icons.push(symbols[i].id);
    }
  }

  copyIconName(name: string) {
    if (this.copier.copyText(name)) {
      this.message.success('Icon name copied');
    } else {
      this.message.error('Copy failed. Please try again!');
    }
  }
}
