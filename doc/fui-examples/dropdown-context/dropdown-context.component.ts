import { Component, OnInit } from '@angular/core';

/**
 * @title Dropdown context menu
 */
@Component({
  templateUrl: './dropdown-context.component.html',
  styleUrls: ['./dropdown-context.component.css'],
})
export class DropdownContextComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
