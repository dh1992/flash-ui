import { Component } from '@angular/core';

/**
 * @title Async Tab
 */
@Component({
  templateUrl: 'tab-async.component.html',
})
export class TabAsyncComponent {
  selectedIndex = 2;

  showMiddlePane = false;

  helloName = 'hello world';

  asyncName = 'async';

  fuiName = 'fui';

  constructor() {
    setTimeout(() => {
      this.helloName = 'hello change';
    }, 2000);

    setTimeout(() => {
      this.showMiddlePane = true;
    }, 3000);

    setTimeout(() => {
      this.asyncName = 'hello async';
    }, 5000);
  }

  tabChange(index: number) {
    console.log(index);
  }
}
