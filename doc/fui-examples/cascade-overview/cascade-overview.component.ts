import { Component, OnInit } from '@angular/core';

/**
 * @title Cascade Overview
 */
@Component({
  templateUrl: './cascade-overview.component.html',
  styleUrls: ['./cascade-overview.component.css'],
})
export class CascadeOverviewComponent implements OnInit {
  chosen = '';

  direction = 'bottomLeft';

  constructor() { }

  ngOnInit() {
  }

  choose(choice: string) {
    this.chosen = choice;
  }

}
