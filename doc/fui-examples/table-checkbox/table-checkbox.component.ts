import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

/**
 * @title Table with Checkbox
 */
@Component({
  templateUrl: 'table-checkbox.component.html',
  styleUrls: ['table-checkbox.component.css'],
})
export class TableCheckboxComponent {
  tableData = [
    { id: 1, name: 'cat', type: 'limb', desc: 'miao' },
    { id: 2, name: 'dog', type: 'limb', desc: 'wuf', hideCheckbox: true },
    { id: 3, name: 'fish', type: 'no-limb', desc: 'blue' },
  ];

  loading = true;

  loadingTableData: any[];

  constructor() { }

  selectChange(data) {
    console.log('selectChange', data);
  }

  onDelete(record) {
    console.log('delete', JSON.stringify(record));
  }

  toggleSublist(datum) {
    datum.showSublist = !datum.showSublist;
  }

  startLoading() {
    this.loading = true;
  }

  stopLoading() {
    this.loading = false;
    this.loadingTableData = [
      { name: 'red', alias: '红色' },
      { name: 'blue', alias: '蓝色' },
    ];
  }
}

@Component({
  selector: 'fui-table-checkbox-operation',
  template: `
  <svg
    fuiIcon="trash"
    style="cursor:pointer"
    color="default"
    (click)="delete()"
  ></svg>
  `,
})
export class TableCheckboxOperationComponent implements OnInit {
  @Input() record: any;
  @Output() deleteEvent: EventEmitter<any> = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  delete() {
    this.deleteEvent.emit(this.record);
  }
}
