import { Component } from '@angular/core';

/**
 * @title Direction Top
 */
@Component({
  templateUrl: 'select-top.component.html',
})
export class SelectTopComponent {
  stringOptions = ['hello', 'world'];

  stringOption = 'hello';

  constructor(
  ) {
  }
}
