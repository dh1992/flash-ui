import { Component, OnInit } from '@angular/core';

/**
 * @title Calendar Lite Overview
 */
@Component({
  templateUrl: './calendar-lite-overview.component.html',
  styleUrls: ['./calendar-lite-overview.component.css'],
})
export class CalendarLiteOverviewComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }
}
