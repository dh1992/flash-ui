import { Component } from '@angular/core';

/**
 * @title Card type
 */
@Component({
  templateUrl: 'tab-card.component.html',
  styleUrls: ['./tab-card.component.css'],
})
export class TabCardComponent {
}
