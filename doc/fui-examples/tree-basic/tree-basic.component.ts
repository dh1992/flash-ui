import { Component } from '@angular/core';
import { NestedTreeControl } from '@angular/cdk/tree';

/**
 * @title Tree Basic
 */
@Component({
  templateUrl: './tree-basic.component.html',
  styleUrls: ['./tree-basic.component.css'],
})
export class TreeBasicComponent {
  data = [
    {
      name: 'domain 1',
      workspaces: [
        {
          name: 'workspace 1',
          workflows: [
            {
              workflowName: 'workflow 1',
            },
          ],
        },
      ],
    },
  ];
  treeControl: NestedTreeControl<any>;

  constructor() {
    this.treeControl = new NestedTreeControl(this._getChildren);
  }

  hasChildren(node: any) {
    return !!this._getChildren(node);
  }

  private _getChildren(node: any) {
    return node.workspaces || node.workflows;
  }
}

