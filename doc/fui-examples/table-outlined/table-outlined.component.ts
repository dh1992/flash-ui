import { Component, OnInit } from '@angular/core';

const tableData = [
  {name: 'cici', sex: 'female', age: '35'},
  {name: 'amy', sex: 'female', age: '20'},
  {name: 'bella', sex: 'female', age: '32'},
  {name: 'denny', sex: 'male', age: '13'},
  {name: 'eric', sex: 'male', age: '23'},
  {name: 'frank', sex: 'female', age: '46'},
];

/**
 * @title Table outline
 */
@Component({
  selector: 'fui-table-outlined',
  templateUrl: './table-outlined.component.html',
  styleUrls: ['./table-outlined.component.css'],
})
export class TableOutlinedComponent implements OnInit {
  tableData;

  constructor() {
    this.tableData = tableData;
   }

  ngOnInit() {
  }
}
