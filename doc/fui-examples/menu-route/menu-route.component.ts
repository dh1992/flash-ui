import { Component, OnInit } from '@angular/core';


/**
 * @title Menu Route
 */
@Component({
  selector: 'fui-menu-route',
  templateUrl: './menu-route.component.html',
  styleUrls: ['./menu-route.component.css'],
})
export class MenuRouteComponent implements OnInit {

  asyncMenuItems = [];

  constructor() { }

  ngOnInit() {
    setTimeout(() => {
      this.asyncMenuItems = [
        {
          key: '/gallery/menu/user',
          text: 'user',
          icon: 'user',
        },
        {
          key: '/gallery/menu/group',
          text: 'group',
          icon: 'user-group',
        },
      ];
    }, 2000);
  }

  selectChange(key) {
    console.log('selected key', key);
  }

}
