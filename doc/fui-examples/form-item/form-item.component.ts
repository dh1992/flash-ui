import { Component, OnInit } from '@angular/core';

/**
 * @title Form Item
 */
@Component({
  templateUrl: './form-item.component.html',
  styleUrls: ['./form-item.component.css'],
})
export class FormItemComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
