import { Component, OnInit } from '@angular/core';

/**
 * @title Tooltip Overview
 */
@Component({
  templateUrl: 'tooltip-overview.component.html',
  styleUrls: ['tooltip-overview.component.css'],
})
export class TooltipOverviewComponent implements OnInit {
  topPositions = ['topLeft', 'top', 'topRight'];
  bottomPositions = ['bottomLeft', 'bottom', 'bottomRight'];
  leftPositions = ['leftTop', 'left', 'leftBottom'];
  rightPositions = ['rightTop', 'right', 'rightBottom'];

  constructor() { }

  ngOnInit() {
  }

}
