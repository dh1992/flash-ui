import { Component, OnInit } from '@angular/core';

/**
 * @title Dropdown Caret
 */
@Component({
  templateUrl: './dropdown-caret.component.html',
  styleUrls: ['./dropdown-caret.component.css'],
})
export class DropdownCaretComponent implements OnInit {

  directions = [
    'topLeft',
    'top',
    'topCenter',
    'topRight',
    'bottomLeft',
    'bottom',
    'bottomCenter',
    'bottomRight',
    'leftTop',
    'left',
    'leftBottom',
    'rightTop',
    'right',
    'rightBottom',
  ];

  constructor() { }

  ngOnInit() {
  }

}
