import { Component, OnInit } from '@angular/core';

/**
 * @title Basic Breadcrumb
 */
@Component({
  templateUrl: './breadcrumb-overview.component.html',
  styleUrls: ['./breadcrumb-overview.component.css'],
})
export class BreadcrumbOverviewComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
