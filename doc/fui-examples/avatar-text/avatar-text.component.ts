import { Component, OnInit } from '@angular/core';

/**
 * @title Avatar Text
 */
@Component({
  templateUrl: './avatar-text.component.html',
  styleUrls: ['./avatar-text.component.css'],
})
export class AvatarTextComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
