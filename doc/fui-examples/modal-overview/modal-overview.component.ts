import { Component, Injector, Inject, Injectable } from '@angular/core';

import { fuiModalService, fuiModalRef, fui_MODAL_DATA } from 'flash-ui';

@Component({
  template: '<div style="margin: 0 0 20px">Hello world</div>',
})
export class ModalHelloComponent { }

/**
 * @title Modal Overview
 */
@Component({
  templateUrl: 'modal-overview.component.html',
  styleUrls: ['modal-overview.component.css'],
})
export class ModalOverviewComponent {
  constructor(
    private modal: fuiModalService,
    private injector: Injector,
  ) { }

  openHelloModal(size: string) {
    return this.modal.open(ModalHelloComponent, {
      title: 'Hello',
      size,
    });
  }

  openSmModal() {
    this.openHelloModal('sm');
  }

  openMdModal() {
    this.openHelloModal('md');
  }

  openLgModal() {
    this.openHelloModal('lg');
  }

  openXlgModal() {
    this.openHelloModal('xlg');
  }
}
