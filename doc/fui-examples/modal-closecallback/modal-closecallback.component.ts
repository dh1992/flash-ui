import { Component, Injector, Inject, Injectable } from '@angular/core';

import { fuiModalService, fuiModalRef, fui_MODAL_DATA } from 'flash-ui';

@Component({
  template: `
  <input type="text" [(ngModel)]="word" [style.margin]="'1rem'">
  <button fuiBtn="primary" (click)="submit()" [disabled]="!word">submit</button>
  `,
})
export class ModalWithCloseCallbackComponent {
  word: string;

  constructor(
    private modal: fuiModalRef,
  ) { }

  submit() {
    this.modal.close(this.word);
  }
}

/**
 * @title Modal Close Callback
 */
@Component({
  templateUrl: 'modal-closecallback.component.html',
  styleUrls: ['modal-closecallback.component.css'],
})
export class ModalClosecallbackComponent {
  constructor(
    private modal: fuiModalService,
    private injector: Injector,
  ) { }

  openModalWithCloseCallback() {
    this.modal.open(ModalWithCloseCallbackComponent, {
      title: 'Modal With Close Callback',
    })
      .subscribe((word: string) => {
        console.log(`Your word is: ${word}`);
      });
  }
}
