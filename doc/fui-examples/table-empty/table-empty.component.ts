import { Component } from '@angular/core';

/**
 * @title Empty Table
 */
@Component({
  templateUrl: 'table-empty.component.html',
  styleUrls: ['table-empty.component.css'],
})
export class TableEmptyComponent {
  constructor() { }
}
