import { Component, OnInit } from '@angular/core';

/**
 * @title Prompt Overview
 */
@Component({
  templateUrl: 'prompt-overview.component.html',
  styleUrls: ['prompt-overview.component.css'],
})
export class PromptOverviewComponent implements OnInit {
  promptData = [
    {
      type: 'success',
      message: 'Success Text',
    }, {
      type: 'info',
      message: 'Info Text',
    }, {
      type: 'warning',
      message: 'Warning Text',
    }, {
      type: 'error',
      message: 'Error Text',
    },
  ];

  constructor() { }

  ngOnInit() {
  }
}
