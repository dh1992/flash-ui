import { Component } from '@angular/core';

/**
 * @title Selected Index Input & Output
 */
@Component({
  templateUrl: 'tab-inoutput.component.html',
})
export class TabInoutputComponent {
  selectedIndex = 1;

  tabChange(index: number) {
    console.log(index);
  }
}
