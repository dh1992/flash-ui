import { Component, OnInit } from '@angular/core';

/**
 * @title Upload Text File
 */
@Component({
  templateUrl: 'upload-textfile.component.html',
  styleUrls: ['upload-textfile.component.css'],
})
export class UploadTextfileComponent implements OnInit {
  fileContent = '';

  loading = false;

  constructor() { }

  ngOnInit() {
  }

  onUploadFileChange(fileItem) {
    const reader = new FileReader();
    if ( fileItem ) {
      reader.onload = (loadResult: any) => {
        this.fileContent = loadResult.target.result;
      };

      reader.onloadstart = () => {
        this.loading = true;
      };

      reader.onloadend = () => {
        this.loading = false;
      };

      reader.readAsText(fileItem);
    }
  }
}
