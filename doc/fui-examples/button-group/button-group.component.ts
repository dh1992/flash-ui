import { Component, OnInit } from '@angular/core';

/**
 * @title Button Group
 * @deprecated not work
 */
@Component({
  templateUrl: './button-group.component.html',
  styleUrls: ['./button-group.component.css'],
})
export class ButtonGroupComponent implements OnInit {
  btnColors = ['primary', 'success', 'danger', 'warning', 'default'];

  constructor() { }

  ngOnInit() {
  }

}
