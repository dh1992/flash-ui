import { Component, OnInit } from '@angular/core';

/**
 * @title Menu Avatar
 */
@Component({
  selector: 'fui-menu-avatar',
  templateUrl: './menu-avatar.component.html',
  styleUrls: ['./menu-avatar.component.css'],
})
export class MenuAvatarComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
