import { Component, OnInit } from '@angular/core';

/**
 * @title Tab Loading
 */
@Component({
  templateUrl: './loading-tab.component.html',
  styleUrls: ['./loading-tab.component.css'],
})
export class LoadingTabComponent implements OnInit {
  loading = false;

  constructor() { }

  ngOnInit() {
    this.enterLoading();
  }

  tabChange(index) {
    this.enterLoading();
  }

  enterLoading() {
    this.loading = true;
    setTimeout(() => {
      this.loading = false;
    }, 5000);
  }
}
