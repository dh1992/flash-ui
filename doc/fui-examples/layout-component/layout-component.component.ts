import { Component, HostBinding } from '@angular/core';

/**
 * @title Layout with Component
 * @iframe
 */
@Component({
  templateUrl: './layout-component.component.html',
  styleUrls: ['./layout-component.component.css'],
})
export class LayoutComponentComponent {
  @HostBinding('class.fui-layout2') hostClass = true;

  @HostBinding('style.height.vh') height = 100;

  @HostBinding('style.color') color = 'white';
}

@Component({
  selector: 'fui-layout-component-sider',
  template: '<div style="width: 50px; height: 100%; background: red">sider</div>',
})
export class LayoutComponentSiderComponent {
  @HostBinding('class.fui-layout2-sider') siderClass = true;
}

@Component({
  selector: 'fui-layout-component-header',
  template: '<div style="height: 40px; background: blue">header</div>',
})
export class LayoutComponentHeaderComponent {
  @HostBinding('class.fui-layout2-header') headerClass = true;
}

@Component({
  selector: 'fui-layout-component-footer',
  template: '<div style="height: 40px; background: blue">footer</div>',
})
export class LayoutComponentFooterComponent {
  @HostBinding('class.fui-layout2-footer') footerClass = true;
}

@Component({
  selector: 'fui-layout-component-container',
  template: `
    <fui-layout-component-header></fui-layout-component-header>
    <fui-layout-component-content></fui-layout-component-content>
    <fui-layout-component-footer></fui-layout-component-footer>
  `,
})
export class LayoutComponentContainerComponent {
  @HostBinding('class.fui-layout2') hostClass = true;

  @HostBinding('class.vertical') verticalClass = true;

  @HostBinding('style.background') background = 'green';
}


@Component({
  selector: 'fui-layout-component-content',
  template: '<div style="height: 200%; margin: 20px; background: purple">content</div>',
})
export class LayoutComponentContentComponent {
  @HostBinding('class.fui-layout2-content') hostClass = true;
}
