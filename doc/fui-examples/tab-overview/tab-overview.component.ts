import { Component } from '@angular/core';

/**
 * @title Basic Tab
 */
@Component({
  templateUrl: 'tab-overview.component.html',
})
export class TabOverviewComponent {
  selectedIndex = 2;

  tabChange(index: number) {
    console.log(index);
  }
}
