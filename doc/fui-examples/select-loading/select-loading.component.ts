import { AfterViewInit, Component, OnDestroy, ViewChild } from '@angular/core';
import { SelectComponent } from 'flash-ui';

/**
 * @title Loading Options
 */
@Component({
  templateUrl: 'select-loading.component.html',
})
export class SelectLoadingComponent implements AfterViewInit, OnDestroy {
  @ViewChild('select') select: SelectComponent;

  loadingOptions = ['hello', 'world'];

  loadingOption = 'hello';

  loading = false;

  constructor(
  ) {}

  ngOnDestroy() {
    if (this.select && this.select.visible) {
      this.select.visible.unsubscribe();
    }
  }

  ngAfterViewInit() {
    this.select.visible.subscribe((openStatus) => {
      if (openStatus) {
        this.loading = true;
        setTimeout(() => this.loading = false, 2000);
      }
    });
  }
}
