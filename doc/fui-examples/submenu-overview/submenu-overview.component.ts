import { Component, HostBinding } from '@angular/core';

import { SubmenuItem } from 'flash-ui';

/**
 * @title Submenu Overview
 */
@Component({
  templateUrl: 'submenu-overview.component.html',
  styleUrls: ['submenu-overview.component.css'],
})
export class SubmenuOverviewComponent {
  routePrefix = '/gallery/submenu';
  menuTitle = '用户与权限';
  backUrl = '../';
  menuItems1: SubmenuItem[] = [
    {
        name: '用户管理',
        url: `${this.routePrefix}/user`,
        icon: 'user',
        suffix: {
          icon: 'exclamation-circle',
          color: 'danger',
          size: 'md',
        },
      },
      {
        name: '组管理',
        url: `${this.routePrefix}/group`,
        icon: 'user',
      },
      {
        name: '角色管理',
        url: `${this.routePrefix}/role`,
        icon: 'user',
      },
      {
        name: '权限管理',
        url: `${this.routePrefix}/permission`,
        icon: 'user',
        count: 10,
      },
  ];
  menuItems2: SubmenuItem[] = [
    {
      name: '费用管理',
      url: `${this.routePrefix}/finance`,
      icon: 'user',
      children: [
        {
          icon: 'user',
          name: '账户总览',
          url: `${this.routePrefix}/finance/overview`,
        },
        {
          icon: 'user',
          name: '订单管理',
          url: `${this.routePrefix}/finance/orders`,
        },
      ],
    },
    {
      name: '消息通知',
      url: `${this.routePrefix}/notices`,
      icon: 'user',
      children: [
        {
          name: '全部消息',
          url: `${this.routePrefix}/notices/overview`,
        },
      ],
    },
  ];
}
