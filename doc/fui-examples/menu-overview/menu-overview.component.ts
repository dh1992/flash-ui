import { Component, OnInit } from '@angular/core';

/**
 * @title Menu Overview
 */
@Component({
  selector: 'fui-menu-overview',
  templateUrl: './menu-overview.component.html',
  styleUrls: ['./menu-overview.component.css'],
})
export class MenuOverviewComponent implements OnInit {
  mode = 'float';

  constructor() { }

  ngOnInit() {
  }

  selectChange(key) {
    console.log('selected key', key);
  }

}
