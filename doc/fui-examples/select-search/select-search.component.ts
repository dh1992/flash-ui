import { Component } from '@angular/core';

/**
 * @title Select with Search
 */
@Component({
  templateUrl: 'select-search.component.html',
  styleUrls: ['select-search.component.css'],
})
export class SelectSearchComponent {
  objectOptions = [
    {
      label: 'hello',
      value: '1',
    },
    {
      label: 'world',
      value: '2',
    },
    {
      label: 'OPTION_DISABLED',
      value: 'disabled',
      disabled: true,
    },
  ];

  objectOption = this.objectOptions[0]['value'];

  constructor(
  ) {
  }

  modelChange(value) {
    console.log('selectChange', value);
  }
}
