import { Component, OnInit } from '@angular/core';

/**
 * @title Basic Loading
 */
@Component({
  templateUrl: './loading-overview.component.html',
  styleUrls: ['./loading-overview.component.css'],
})
export class LoadingOverviewComponent implements OnInit {
  loading = false;

  constructor() { }

  ngOnInit() {
    this.enterLoading();
  }

  tabChange(index) {
    this.enterLoading();
  }

  enterLoading() {
    this.loading = true;
    setTimeout(() => {
      this.loading = false;
    }, 5000);
  }
}
