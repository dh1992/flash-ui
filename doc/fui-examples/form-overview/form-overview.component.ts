import { Component } from '@angular/core';

/**
 * @title Regular Form
 */
@Component({
  templateUrl: './form-overview.component.html',
  styleUrls: ['./form-overview.component.css'],
})
export class FormOverviewComponent {
  loginFormDisabled = false;
  loginFormReadonly = false;
  date = new Date();
  loginData = {};

  companies = [
    { name: 'Disney' },
    { name: 'Pixel' },
  ];

  constructor() {}

  dateChange(event) {
    console.log('event', event);
  }

  login(formData: any) {
    console.log(formData);
  }

  toggleLoginFormDisable() {
    this.loginFormDisabled = !this.loginFormDisabled;
  }

  toggleLoginFormReadonly() {
    this.loginFormReadonly = !this.loginFormReadonly;
  }
}
