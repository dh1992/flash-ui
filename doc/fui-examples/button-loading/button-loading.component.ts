import { Component, OnInit } from '@angular/core';

/**
 * @title Button Loading
 * @deprecated not work
 */
@Component({
  templateUrl: './button-loading.component.html',
  styleUrls: ['./button-loading.component.css'],
})
export class ButtonLoadingComponent implements OnInit {

  loading = false;
  loadingSvg = false;
  iconName: string;

  constructor() { }

  btnclick(btn) {
    if (btn === 1) {
      this.loading = true;
      setTimeout(() => {
        this.loading = false;
      }, 3000);
    } else {
      this.loadingSvg = true;
      setTimeout(() => {
        this.loadingSvg  = false;
      }, 3000);
    }
  }

  ngOnInit() {
    this.iconName = 'user-admin';
  }

}
