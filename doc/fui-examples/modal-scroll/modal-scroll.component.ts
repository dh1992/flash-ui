import { Component, Injector } from '@angular/core';

import { fuiModalService } from 'flash-ui';

@Component({
  template: `
  <div [style.height]="'100rem'">This is scrollable modal</div>
  `,
})
export class ModalScrollableComponent { }

/**
 * @title Modal Scroll
 */
@Component({
  templateUrl: 'modal-scroll.component.html',
  styleUrls: ['modal-scroll.component.css'],
})
export class ModalScrollComponent {
  constructor(
    private modal: fuiModalService,
    private injector: Injector,
  ) { }

  openScrollableModal() {
    this.modal.open(ModalScrollableComponent, {
      title: 'Scrollable',
    });
  }
}
