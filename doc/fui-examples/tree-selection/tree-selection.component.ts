import { Component } from '@angular/core';
import { NestedTreeControl } from '@angular/cdk/tree';
/**
 * @title Tree Select
 */
@Component({
  templateUrl: './tree-selection.component.html',
  styleUrls: ['./tree-selection.component.css'],
})
export class TreeSelectionComponent {
  dataSelect = [
    {
      name: 'domain 1',
      workspaces: [
        {
          name: 'workspace 1',
          workflows: [
            {
              workflowName: 'workflow 1',
            },
            {
              workflowName: 'workflow 2',
            },
          ],
        },
      ],
    },
    {
      name: 'domain 2',
      workspaces: [
        {
          name: 'workspace 2',
          workflows: [
            {
              workflowName: 'workflow 3',
            },
            {
              workflowName: 'workflow 4',
            },
          ],
        },
        {
          name: 'workspace 3',
          workflows: [],
        },
      ],
    },
  ];

  treeControl: NestedTreeControl<any>;

  constructor() {
    this.treeControl = new NestedTreeControl(this._getChildren);
  }

  hasChildren(node: any) {
    return !!this._getChildren(node);
  }

  private _getChildren(node: any) {
    return node.workspaces || node.workflows;
  }
}

