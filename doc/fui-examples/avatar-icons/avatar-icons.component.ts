import { Component, OnInit } from '@angular/core';


/**
 * @title Avatar icons
 */
@Component({
  templateUrl: './avatar-icons.component.html',
  styleUrls: ['./avatar-icons.component.css'],
})
export class AvatarIconsComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
