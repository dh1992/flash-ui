import { Component, OnInit } from '@angular/core';

/**
 * @title Popover Content
 */
@Component({
  templateUrl: 'popover-content.component.html',
  styleUrls: ['popover-content.component.css'],
})
export class PopoverContentComponent implements OnInit {
  topPositions = ['topLeft', 'top', 'topRight'];
  bottomPositions = ['bottomLeft', 'bottom', 'bottomRight'];
  leftPositions = ['leftTop', 'left', 'leftBottom'];
  rightPositions = ['rightTop', 'right', 'rightBottom'];

  constructor() { }

  ngOnInit() {
  }

}
