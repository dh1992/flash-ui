import { Component, OnInit } from '@angular/core';

/**
 * @title Property Custom
 */
@Component({
  templateUrl: 'property-custom.component.html',
  styleUrls: ['property-custom.component.css'],
})
export class PropertyCustomComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
