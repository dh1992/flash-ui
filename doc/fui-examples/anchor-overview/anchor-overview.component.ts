import { Component, OnInit } from '@angular/core';

/**
 * @title Basic Anchor
 */
@Component({
  templateUrl: './anchor-overview.component.html',
  styleUrls: ['./anchor-overview.component.css'],
})
export class AnchorOverviewComponent implements OnInit {

  container: Element | null = null;

  constructor() { }

  ngOnInit() {
    this.container = document.querySelector('#componentViewer');
  }

  getLinkHref(href: string) {
    return encodeURIComponent(href);
  }
}
