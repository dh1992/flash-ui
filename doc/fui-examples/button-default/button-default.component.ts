import { Component, OnInit } from '@angular/core';

/**
 * @title Default Button
 */
@Component({
  templateUrl: './button-default.component.html',
  styleUrls: ['./button-default.component.css'],
})
export class ButtonDefaultComponent implements OnInit {
  btnColors = ['primary', 'success', 'danger', 'warning', 'default'];

  constructor() { }

  ngOnInit() {
  }

}
