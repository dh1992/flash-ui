import { Component, OnInit } from '@angular/core';

/**
 * @title Dropdown Directions
 */
@Component({
  templateUrl: './dropdown-directions.component.html',
  styleUrls: ['./dropdown-directions.component.css'],
})
export class DropdownDirectionsComponent implements OnInit {

  directions = [
    'topLeft',
    'top',
    'topCenter',
    'topRight',
    'bottomLeft',
    'bottom',
    'bottomCenter',
    'bottomRight',
    'leftTop',
    'left',
    'leftBottom',
    'rightTop',
    'right',
    'rightBottom',
  ];

  constructor() { }

  ngOnInit() {
  }

  onDropdownVisibleChange($event) {
    console.log('visible: ', $event);
  }
}
