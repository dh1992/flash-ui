import { Component, OnInit } from '@angular/core';

/**
 * @title Basic Avatar
 */
@Component({
  templateUrl: './avatar-overview.component.html',
  styleUrls: ['./avatar-overview.component.css'],
})
export class AvatarOverviewComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
