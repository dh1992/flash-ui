import { Component, OnInit } from '@angular/core';

/**
 * @title Popover Overview
 */
@Component({
  templateUrl: 'popover-overview.component.html',
  styleUrls: ['popover-overview.component.css'],
})
export class PopoverOverviewComponent implements OnInit {
  topPositions = ['topLeft', 'top', 'topRight'];
  bottomPositions = ['bottomLeft', 'bottom', 'bottomRight'];
  leftPositions = ['leftTop', 'left', 'leftBottom'];
  rightPositions = ['rightTop', 'right', 'rightBottom'];

  constructor() { }

  ngOnInit() {
  }

}
