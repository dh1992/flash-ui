import { Component, OnInit } from '@angular/core';

/**
 * @title Dropdown default open
 */
@Component({
  templateUrl: './dropdown-default-open.component.html',
})
export class DropdownDefaultOpenComponent implements OnInit {

  open = true;

  constructor() { }

  ngOnInit() {
  }

  noTriggerOpen() {
    this.open = !this.open;
  }

}
