import { Component, Injector, Inject, Injectable } from '@angular/core';

import { fuiModalService, fuiModalRef, fui_MODAL_DATA } from 'flash-ui';

@Injectable()
export class CounterService {
  private _value = 0;

  get value() {
    return this._value;
  }

  add() {
    this._value++;
  }
}

@Component({
  template: `
  <div>{{counter.value}}</div>
  <button style="margin: 8px 20px 8px 0" fuiBtn="primary" (click)="counter.add()">add</button>
  `,
})
export class ModalWithInjectorComponent {
  constructor(
    public counter: CounterService,
  ) { }
}

/**
 * @title Modal Injector
 */
@Component({
  templateUrl: 'modal-injector.component.html',
  styleUrls: ['modal-injector.component.css'],
  providers: [
    CounterService,
  ],
})
export class ModalInjectorComponent {
  constructor(
    private modal: fuiModalService,
    private injector: Injector,
  ) { }

  openModalWithInjector() {
    this.modal.open(ModalWithInjectorComponent, {
      title: 'Modal With Injector',
      injector: this.injector,
    });
  }
}
