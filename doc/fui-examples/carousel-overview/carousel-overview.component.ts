import { Component, OnInit } from '@angular/core';

/**
 * @title Carousel Overview
 */
@Component({
  templateUrl: './carousel-overview.component.html',
  styleUrls: ['./carousel-overview.component.css'],
})
export class CarouselOverviewComponent implements OnInit {
  array = [1, 2, 3, 4];

  constructor() { }

  ngOnInit() {
  }

}
