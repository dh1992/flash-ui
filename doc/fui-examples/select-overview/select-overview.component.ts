import { Component } from '@angular/core';

/**
 * @title Object Options
 */
@Component({
  templateUrl: 'select-overview.component.html',
  styleUrls: ['select-overview.component.css'],
})
export class SelectOverviewComponent {
  objectOptions = [
    {
      label: 'OPTION_A',
      value: ['a'],
    },
    {
      label: 'OPTION_B',
      value: 'b',
    },
    {
      label: '',
      value: null,
    },
    {
      label: 'OPTION_DISABLED',
      value: 'disabled',
      disabled: true,
    },
  ];

  objectOption = this.objectOptions[0]['value'];

  constructor(
  ) {
  }

  modelChange(value) {
    console.log('modelChange', value);
  }
}
