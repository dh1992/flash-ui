import { Component } from '@angular/core';

/**
 * @title Multiple Select
 */
@Component({
  templateUrl: 'select-multiple.component.html',
  styleUrls: ['select-multiple.component.css'],
})
export class SelectMultipleComponent {
  objectOptions = [
    {
      label: 'OPTION_A',
      value: 'a',
    },
    {
      label: 'OPTION_B',
      value: 'b',
    },
    {
      label: 'OPTION_C',
      value: 'c',
    },
    {
      label: 'OPTION_D',
      value: 'd',
    },
  ];

  objectOption = this.objectOptions[0]['value'];

  constructor(
  ) {
  }

  onClick(event) {
    console.log('event', event);
  }
}
