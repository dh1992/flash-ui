import { Component, OnInit } from '@angular/core';
import { of } from 'rxjs';

import { Pagination } from 'flash-ui';
/**
 * @title Pagination Sizes
 */
@Component({
  templateUrl: './pagination-sizes.component.html',
  styleUrls: ['./pagination-sizes.component.css'],
})
export class PaginationSizesComponent implements OnInit {
  pagination: Pagination = new Pagination(1, 10, 205);

  constructor() { }

  ngOnInit() {
    this.fetchData();
  }

  fetchDataApi(pagination?) {
    return of({
      data: [],
      pagination: this.pagination,
    });
  }

  fetchData() {
    console.log('pagination', this.pagination);
    this.fetchDataApi(this.pagination)
      .subscribe((data) => {
        this.pagination = data.pagination;
      });
  }
}
