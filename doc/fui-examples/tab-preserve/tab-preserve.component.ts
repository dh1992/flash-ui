import { Component, OnInit, OnDestroy } from '@angular/core';

/**
 * @title Preserve tab content
 */
@Component({
  templateUrl: 'tab-preserve.component.html',
})
export class TabPreserveComponent {
  selectedIndex = 2;

  tabChange(index: number) {
    console.log(index);
  }
}

@Component({
  selector: 'fui-tab-preserve-world',
  template: `
    <fui-checkbox [(ngModel)]="showTime">show time</fui-checkbox>
    <div *ngIf="showTime">time: {{timer}}</div>
  `,
})
export class TabPreserveWorldComponent implements OnInit, OnDestroy {
  content = 'world';

  timer = 0;

  interval;

  showTime = true;

  ngOnInit() {
    this.interval = setInterval(() => {
      this.timer ++;
    }, 1000);
  }

  ngOnDestroy() {
    clearInterval(this.interval);
  }
}
