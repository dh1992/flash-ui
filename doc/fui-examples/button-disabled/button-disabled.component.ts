import { Component, OnInit } from '@angular/core';

/**
 * @title Disabled Button
 */
@Component({
  templateUrl: './button-disabled.component.html',
  styleUrls: ['./button-disabled.component.css'],
})
export class ButtonDisabledComponent implements OnInit {
  btnColors = ['primary', 'success', 'danger', 'warning', 'default'];

  constructor() { }

  ngOnInit() {
  }

}
