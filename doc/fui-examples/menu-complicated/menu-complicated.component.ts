import { Component, OnInit } from '@angular/core';

/**
 * @title Complicated Menu
 */
@Component({
  selector: 'fui-menu-complicated',
  templateUrl: './menu-complicated.component.html',
  styleUrls: ['./menu-complicated.component.css'],
})
export class MenuComplicatedComponent implements OnInit {
  subItems: string[] = [];

  itemMap = {
    role: ['admin', 'guest', 'guardian', 'workflow', 'manager', 'planet-saver'],
    group: ['default', 'public'],
  };

  constructor() { }

  ngOnInit() {
  }

  hoverItem(item: string) {
    this.subItems = this.itemMap[item];
  }

  leavePanel() {
    this.subItems = [];
  }

}
