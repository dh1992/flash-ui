import { Component } from '@angular/core';

/**
 * @title Template Tab
 */
@Component({
  templateUrl: 'tab-template.component.html',
})
export class TabTemplateComponent {
  selectedIndex = 2;

  tabChange(index: number) {
    console.log(index);
  }
}
