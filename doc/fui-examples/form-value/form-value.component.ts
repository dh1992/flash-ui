import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm, FormBuilder, FormGroup } from '@angular/forms';


/**
 * @title Form with Default Value
 */
@Component({
  templateUrl: './form-value.component.html',
  styleUrls: ['./form-value.component.css'],
})
export class FormValueComponent implements OnInit {
  userForm: FormGroup;
  userFormDisabled = false;
  userFormReadonly = false;
  loginFormDisabled = false;
  date = new Date();

  loginData = {};

  companies = [
    { name: 'Disney' },
    { name: 'Pixel' },
  ];

  button = 'b1';

  constructor(private fb: FormBuilder) {
    this.userForm = fb.group({
      name: [{ value: 'Admin', disabled: true }],
      email: ['hello@world'],
      ageMax: [100],
      age: [120],
      hobbies: [['code']],
      sex: ['male'],
      color: ['red'],
      company: ['Disney'],
      birthday: [new Date('1999-9-9')],
      description: [''],
    });
  }

  ngOnInit() {
    this.userForm.controls['sex'].valueChanges.subscribe((sex: string) => {
      console.log('sex', sex);
    });
    this.userForm.controls['hobbies'].valueChanges.subscribe((hobbies: string) => {
      console.log('hobbies', hobbies);
    });
    this.userForm.controls['company'].valueChanges.subscribe((company: string) => {
      console.log('company', company);
    });
  }

  dateChange(event) {
    console.log('event', event);
  }

  login(formData: any) {
    console.log(formData);
  }

  submit() {
    console.log(this.userForm.value);
  }

  toggleLoginFormDisable() {
    this.loginFormDisabled = !this.loginFormDisabled;
  }

  toggleUserFormDisable() {
    this.userFormDisabled = !this.userFormDisabled;
    Object.keys(this.userForm.controls).forEach((controlName) => {
      const control = this.userForm.controls[controlName];
      if (this.userFormDisabled) {
        control.disable();
      } else {
        control.enable();
      }
    });
  }

  toggleUserFormReadonly() {
    this.userFormReadonly = !this.userFormReadonly;
  }
}
