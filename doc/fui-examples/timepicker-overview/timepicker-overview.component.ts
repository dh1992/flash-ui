import { Component, OnInit } from '@angular/core';

/**
 * @title Timepicker Overview
 */
@Component({
  selector: 'fui-timepicker-overview',
  templateUrl: './timepicker-overview.component.html',
  styleUrls: ['./timepicker-overview.component.css'],
})
export class TimepickerOverviewComponent implements OnInit {
  time: any;
  timeInput: any;

  constructor() { }

  ngOnInit() {
  }

}
