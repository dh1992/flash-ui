import { Component, Injector, Inject, Injectable } from '@angular/core';

import { fuiModalService, fuiModalRef, fui_MODAL_DATA } from 'flash-ui';

@Component({
  template: `
  <ul style="padding: 24px 20px; line-height: 24px">
    <li *ngFor="let book of books">{{book}}</li>
  </ul>
  `,
})
export class ModalWithDataComponent {
  books: string[];

  constructor(
    @Inject(fui_MODAL_DATA) data,
  ) {
    this.books = data.books;
  }
}

/**
 * @title Modal Data
 */
@Component({
  templateUrl: 'modal-data.component.html',
  styleUrls: ['modal-data.component.css'],
})
export class ModalDataComponent {
  constructor(
    private modal: fuiModalService,
    private injector: Injector,
  ) { }

  openModalWithData() {
    this.modal.open(ModalWithDataComponent, {
      title: 'Modal With Data',
      data: {
        books: ['hello', 'world', 'angular2'],
      },
    });
  }
}
