import { Component, OnInit } from '@angular/core';

/**
 * @title Outlined Button
 */
@Component({
  templateUrl: './button-outlined.component.html',
  styleUrls: ['./button-outlined.component.css'],
})
export class ButtonOutlinedComponent implements OnInit {
  btnColors = ['primary', 'success', 'danger', 'warning', 'default'];

  constructor() { }

  ngOnInit() {
  }

}
