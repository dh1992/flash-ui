import { Component } from '@angular/core';

/**
 * @title Select with No Options
 */
@Component({
  templateUrl: 'select-none.component.html',
})
export class SelectNoneComponent {
  constructor(
  ) {
  }
}
