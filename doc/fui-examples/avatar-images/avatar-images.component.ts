import { Component, OnInit } from '@angular/core';


/**
 * @title Avatar Images
 */
@Component({
  templateUrl: './avatar-images.component.html',
  styleUrls: ['./avatar-images.component.css'],
})
export class AvatarImagesComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
