import { Component, OnInit } from '@angular/core';

/**
 * @title simple steps
 */
@Component({
  templateUrl: './steps-overview.component.html',
  styleUrls: ['./steps-overview.component.css'],
})
export class StepsOverviewComponent implements OnInit {
  steps = ['第一步', '第二步', '第三步'];

  current = 0;

  constructor() { }

  ngOnInit() { }

  prev() {
    this.current --;
  }

  next() {
    this.current ++;
  }

}
