import { Component, OnInit } from '@angular/core';

/**
 * @title Datepicker Overview
 */
@Component({
  templateUrl: './datepicker-overview.component.html',
})
export class DatepickerOverviewComponent implements OnInit {
  maxDate = new Date();
  minDate = new Date();

  date: Date;

  constructor() { }

  ngOnInit() {
    this.maxDate.setDate(this.maxDate.getDate() + 20);
    this.minDate.setDate(this.minDate.getDate() - 20);
  }

  onDateChange(date: Date) {
    this.date = date;
  }
}
