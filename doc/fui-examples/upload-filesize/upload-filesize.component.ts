import { Component, OnInit } from '@angular/core';

/**
 * @title Upload Filesize
 */
@Component({
  templateUrl: 'upload-filesize.component.html',
  styleUrls: ['upload-filesize.component.css'],
})
export class UploadFilesizeComponent implements OnInit {
  constructor() { }

  ngOnInit() {
  }
}
