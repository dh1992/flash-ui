import { Component } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';

/**
 * @title Simple Progress
 */
@Component({
  templateUrl: 'progress-default.component.html',
  styleUrls: ['progress-default.component.css'],
})
export class ProgressDefaultComponent {

  configCtrl = new FormGroup({
    percent: new FormControl(75),
    status: new FormControl('active'),
  });

  percent = 75;

  status = 'active';

  statuses = ['active', 'success', 'exception'];

  constructor( ) { }

  submit() {
    const formValue = this.configCtrl.value;

    this.percent = formValue.percent ? formValue.percent.valueOf() : 0;
    this.status = formValue.status ? formValue.status : 'active';
  }

}

