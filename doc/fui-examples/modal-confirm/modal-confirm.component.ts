import { Component, Injector, Inject, Injectable } from '@angular/core';

import { fuiModalService, fuiModalRef, fui_MODAL_DATA } from 'flash-ui';

/**
 * @title Modal Confirm
 */
@Component({
  templateUrl: 'modal-confirm.component.html',
  styleUrls: ['modal-confirm.component.css'],
})
export class ModalConfirmComponent {
  constructor(
    private modal: fuiModalService,
    private injector: Injector,
  ) { }

  openConfirmModal() {
    this.modal.confirm({
      title: 'hello',
      message: 'world',
    })
      .subscribe((confirmed: boolean) => {
        console.log(`confirmed: ${confirmed}`);
      });
  }
}
