import { Component, OnInit } from '@angular/core';

/**
 * @title Basic Input
 */
@Component({
  templateUrl: './input-overview.component.html',
  styleUrls: ['./input-overview.component.css'],
})
export class InputOverviewComponent {
  inputNumberScale: number;
  inputNumberStep = 5;
  inputNumberDisabled = 9;

  file: any;

  files: any;

  constructor() {
  }

  onBlur($event) {
    console.log($event);
  }

  onSelectFileChange($event) {
    console.log( 'files: ', $event);
  }
}
