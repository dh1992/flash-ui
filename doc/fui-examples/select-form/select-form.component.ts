import { Component } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';

/**
 * @title Form
 */
@Component({
  templateUrl: 'select-form.component.html',
})
export class SelectFormComponent {
  form: FormGroup;

  formOptions = [
    {
      username: 'hello',
      value: '1',
    },
    {
      username: 'world',
      value: '2',
    },
  ];

  constructor(
    private fb: FormBuilder,
  ) {
    this.form = fb.group({
      username: ['1'],
    });
  }

  submit() {
    console.log(this.form.value);
  }
}
