import { Component } from '@angular/core';
import { fuiModalService } from 'flash-ui';

/**
 * @title Model Tip
 */
@Component({
  templateUrl: './modal-tip.component.html',
  styleUrls: ['./modal-tip.component.css'],
})
export class ModalTipComponent {

  constructor(
    private modal: fuiModalService,
  ) { }

  openTipModal(type?) {
    this.modal[type]({
      title: type ? `${type} tip title` : null,
      message: `${type || ''} tip content here`,
    });
  }

}
