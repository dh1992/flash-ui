import { Component, OnInit } from '@angular/core';
import { of } from 'rxjs';

import { Pagination } from 'flash-ui';
/**
 * @title Pagination Overview
 */
@Component({
  templateUrl: './pagination-overview.component.html',
  styleUrls: ['./pagination-overview.component.css'],
})
export class PaginationOverviewComponent implements OnInit {
  pagination: Pagination = new Pagination(1, 10, 205);
  externalPagination = new Pagination(1, 10, 205);
  showTotal = false;
  showSize = true;
  showGoTo = true;

  constructor() { }

  ngOnInit() {
    this.fetchData();
  }

  fetchDataApi(pagination?) {
    return of({
      data: [],
      pagination: this.pagination,
    });
  }

  fetchData() {
    console.log('pagination', this.pagination);
    this.fetchDataApi(this.pagination)
      .subscribe((data) => {
        this.pagination = data.pagination;
      });
  }

  triggerExteranl() {
    const pagination = new Pagination();
    Object.assign(pagination, this.externalPagination);
    this.pagination = pagination;
  }
}
