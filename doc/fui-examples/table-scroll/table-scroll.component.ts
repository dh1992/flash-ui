import { Component, OnInit } from '@angular/core';

/**
 * @title Scrollable Table
 */
@Component({
  selector: 'fui-table-scroll',
  templateUrl: './table-scroll.component.html',
  styleUrls: ['./table-scroll.component.css'],
})
export class TableScrollComponent implements OnInit {
  scrollTableData = [
    {name: 'amy', sex: 'female', age: '20'},
    {name: 'bella', sex: 'female', age: '32'},
    {name: 'cici', sex: 'female', age: '35'},
    {name: 'denny', sex: 'male', age: '13'},
    {name: 'eric', sex: 'male', age: '23'},
    {name: 'frank', sex: 'female', age: '46'},
  ];

  constructor() { }

  ngOnInit() {
  }

}
