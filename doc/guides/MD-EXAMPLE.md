# FUI Gallery

fui Gallery展示了所有fui支持的组件，组件的使用样例，以及组件暴露出来的API和Model。

## 添加组件页面

在组件文件夹下添加Markdown文件即可添加相应的组件页面，Markdown相关语法见[Basic Syntax](https://www.markdownguide.org/basic-syntax/)。如果需要添加与某个具体组件无关的页面，只需将相应Markdown文件添加到 */doc/guides* 文件夹下。

### 添加示例

示例需要提前写好存放在 */doc/fui-example* 下，像以下方式引用：

```markdown
<!-- example(avatar-icons) -->
```

需要注意的是，括号里的名字必须跟示例的文件名相同。展示效果如下：

<!-- example(avatar-icons) -->

### API和Model

API和Model是由系统自动提取并展示的，无需特殊处理，但请添加必要的注释。

需要注意的是，在开发API时，只有符合格式的注释才能被正确提取：

```js
/**
 * XXXXX
 */
```

或者：

```js
/* XXXXX */
```

## 代码贡献者
