/**
 * @deprecated
 */
import { task, series, parallel, watch } from 'gulp';
import { srcPath, docPath } from '../config';

const fs = require('fs');
const path = require('path');
const shell = require('shelljs');
const chalk = require('chalk');

const destPath = path.join(docPath, 'flash-ui');

function handleFileChange(filename: string) {
  const srcFilePath = path.join(srcPath, filename);
  const destFilePath = path.join(destPath, filename);

  if (fs.existsSync(srcFilePath)) {
    shell.echo(chalk.green(`File changes: ${srcFilePath}`));
    shell.cp(srcFilePath, destFilePath);
  } else {
    shell.echo(chalk.red(`File deletes: ${srcFilePath}`));
    shell.rm(destFilePath);
  }
}

task('watch-docs', () => {
  watch(srcPath, series('docs-inc'));
});

task('watch-copy-fui', () => {
  fs.watch(srcPath, { recursive: true }, (_eventType: string, filename: string) => {
    handleFileChange(filename);
  });
});

task('watch-fui', parallel('watch-copy-fui', 'watch-docs'));
