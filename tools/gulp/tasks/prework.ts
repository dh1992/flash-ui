import { task, src, dest, series } from 'gulp';
import * as fs from 'fs';
import * as path from 'path';

import { autoPath, srcPath, docPath } from '../config';

// rimraf 的作用：以包的形式包装rm -rf命令，用来删除文件和文件夹的，不管文件夹是否为空，都可删除；
const rm = require('rimraf').sync;

// 清空要打包的文件夹
task('delete-pre-files', (done) => {
  if (fs.existsSync(autoPath)) {
    rm(autoPath);
  }

  if (fs.existsSync(path.join(docPath, 'assets/examples'))) {
    rm(path.join(docPath, 'assets/examples'));
  }

  if (fs.existsSync(path.join(docPath, 'assets/markdown'))) {
    rm(path.join(docPath, 'assets/markdown'));
  }
  done();
});

// 创建__auto__文件夹，放置文档界面要显示数据的数据结构
task('create-auto-dir', (done) => {
  if (!fs.existsSync(autoPath)) {
    fs.mkdirSync(path.join(autoPath));
  }
  done();
});

// 串行执行任务
task('prework', series('delete-pre-files', 'create-auto-dir'));
