const klawSync = require('klaw-sync');
const path = require('path');


/**
 * @param {String} paths - walk dir
 * @param {String} filterExtraName - do something with extraName
 * @param {function} fn - callback sync
 */
export const walkDirAndFilter = (paths: string, filterExtraName: string, fn: (ele: any, index?: number) => any) => {
  try {
    const pathList = klawSync(paths);
    pathList.forEach((ele: any, index: number) => {
      if (path.extname(ele.path) === filterExtraName) {
        fn(ele, index);
      }
    });
  } catch (e) {
    console.error(e);
    process.exit(1);
  }
};

export const getBaseNameWithoutExt = (paths: string) => {
  return path.basename(paths).split('.')[0];
};
