import * as path from 'path';

// __dirname为当前文件所在目录
export const srcPath = path.resolve(__dirname, '../../src');
export const docPath = path.resolve(__dirname, '../../doc');
export const autoPath = path.resolve(__dirname, '../../doc/app/__auto__');
export const docAppPath = path.resolve(__dirname, '../../doc/app');
