# fui

本组件库由星环科技数据平台部门前端小组开发，供本部门以及公司内部所有相关前端产品使用。

## 安装

```
npm install flash-ui
```

导入完成模块：
```ts
import { FuiModule } from 'flash-ui';

@NgModule({
  imports: [
    FuiModule,
    ...
  ],
  ...
})
```

也可以导入单个模块：
```ts
import { BtnModule } from 'flash-ui';
```

导入样式:
```css
@import '~flash-ui/styles/theme'
```

导入样式util，包含variable、mixin等：
```css
@import '~flash-ui/styles/util'
```
