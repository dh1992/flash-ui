# 贡献指南

## 工作流

### 新增特性/修复bug

- 在JIRA将相应task assign给自己，如果没有现成的task，新建一个。从master分支切出自己的分支：

  ```
  git checkout -b my-branch master
  ```
- 添加相应[代码](#开发组件)和[测试](#测试)
- 提交改变，提交message遵从[提交信息规范](#提交信息规范)
- 更新当前分支到gitlab
  ```
  git push origin my-branch
  ```
- 发送一个merge request到master分支
- 如果reviewer有改进意见，则修改代码并`rebase`提交：
  ```
  git rebase master -i
  git push -f
  ```

### 开发组件

1. clone 本 repo
2. `npm run doc`
3. 在`src/fui`目录下添加组件以及文档
4. 执行`npm run build:fui`和`npm run build:doc`都需成功

### 测试

1. 对组件编写单元测试
2. `npm test`

### 发布新版本

在master分支上，当当前版本所有的特性和已知bug都已修复，运行：
```shell
git fetch --tags
# 选择发布版本的粒度，参见semver语义化版本规则
npm version major|minor|patch
git push --follow-tags origin master
```

tag push到gitlab之后，ci会执行以下脚本，无需在本地执行。
```
npm run build:fui
npm run publish:fui
```

### 版本约定

目前flash-ui还处于开发中，不稳定。兼容的改进，或者不对flash-ui本身产生巨大变化（比如样式）的提交，一律打patch，否则打minor。下游依赖通过`~0.x`来确定flash-ui在某个minor版本。比如`~0.3`会约束依赖的版本在`0.3`到 `0.4`之间。
 - flash-ui不发生breaking changes，则会一直打patch，下游依赖只需重新`npm install`就可以。
 - 如果发生breaking changes，flash-ui则会迭代一个minor版本，下游依赖则需更新版本号到`~0.4`。

## 提交信息规范

CHANGELOG利用提交信息（commit message）产生。有规则的提交信息可读性更好，CHANGELOG也会更清楚。一个典型的提交信息如下：
```
feat: WARP-12345 some description
```

### 提交信息格式

每个提交信息都是由以下部分组成的：**header**、**body**(可选) 和 **footer**(可选). 其中header包含**type**、 **scope**(可选) 和 **subject**：

```
<type>(<scope>): <subject>
<BLANK LINE>
<body>
<BLANK LINE>
<footer>
```

为了便于阅读，以及适配相关工具，每一行提交信息不得超过100个字符。

### Type
必须从以下字段中选择:

* **feat**: 新特性
* **fix**: Bug修复
* **docs**: 仅对文档做出的修改
* **style**: 样式修改，且不影响代码逻辑（white-space, formatting, missing semi-colons等）
* **refactor**: 当前功能的重构
* **perf**: 性能提升
* **test**: 添加单元测试
* **build**: 影响构建系统，包括CI、npm包等
* **chore**: 其他修改，不影响组件代码或测试

### Scope
scope是用来指定提交的具体范围的，比如：`datepicker`、`dialog`等。

### Subject
subject包含对改动的简短描述，且必须包含JIRA号，格式如下：
```
WARP-12345 some description
```

## Gitlab CI

### 制作基础镜像

基础镜像为如下地址，由 ci/base/Dockerfile制作得到。该镜像包含`chrome`和`yarn`，是包括该组件库等其他一切产品镜像的基础。

ci-base-后面的数字为当前环境所用nodeJS的版本，命令中的版本号仅供参考，请根据实际版本更改。

```
docker build . -f ci/base/Dockerfile -t frontend/flash-ui/build/ci-base-10.16
docker tag frontend/flash-ui/build/ci-base-10.16 172.16.1.99/frontend/flash-ui/build/ci-base-10.16:latest
docker push 172.16.1.99/frontend/flash-ui/build/ci-base-10.16:latest
```

### 制作CI镜像

参照 ci/Dockerfile制作CI镜像。该镜像包含当前项目所有node依赖，如果项目依赖更改，则需要重新制作并推到`172.16.1.99`私有仓库上。

flash-ui-后面的数字一般为当前组件库Angular的版本，请根据实际版本更改。

```
docker build . -f ci/Dockerfile -t flash/flash-ui-11
docker tag flash/flash-ui-11 172.16.1.99/frontend/flash-ui/build/flash-ui-11:latest
docker push 172.16.1.99/frontend/flash-ui/build/flash-ui-11:latest
```

### gitlab-ci.yaml

precommit测试分别为`build-aot`, `unit-test`。`build-aot`包含`build:fui`和`build:doc`, `build:fui`确保fui库能跑过aot build，但这不保证下游依赖能成功build aot。`build:doc`来保证下游依赖能成功build aot。而`unit-test`保证fui库单元测试成功通过。

`publish`由tag操作触发，该命令会构建组件库的包并发布。

