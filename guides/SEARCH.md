# 全站搜索

支持在本站所有文档和组件页面进行搜索，并且可以通过搜索结果跳转相关页面。

## 生成搜索信息

为了能对页面内容进行文本搜索，首先需要获取每个页面可以被搜索的信息，直观的方法是通过爬虫爬取所有页面的文本内容。

### 爬取懒加载的页面

鉴于本网站各个页面均基于懒加载来渲染，每个页面都需要先进行渲染，然后再爬取内容。我们使用Selenium来达到这个目的：

1. 安装Selenium
2. 安装渲染网页的浏览器（chrome、firefox）的webdriver
3. 新建Selenium driver渲染需要爬取内容的页面

```js
const {Builder} = require('selenium-webdriver');
const chrome = require('selenium-webdriver/chrome');

const driver = await new Builder()
  .forBrowser('chrome')
  .setChromeOptions(new chrome.Options().headless().windowSize({width: 640, height: 480}))
  .build();
```

### 获取子页面

利用Selenium的wait\until功能，我们可以等到导航栏加载完成之后，依次点击导航栏的每一个条目，跳转到相应子页面：

```js
await driver.wait(until.elementLocated(By.css('ul.nav')), 10);
const navItems = await driver.findElements(By.css('ul.nav li'));

for (let index = 0; index < navItems.length; index++) {
  const navItem = navItems[index];
  navItem.click();

  await driver.wait(until.elementLocated(By.css('.docs-container')), 10);
  ...
}
```

之后便可以获取相关页面的全部内容生成搜索信息。

## 支持模糊搜索

在页面加载搜索信息之后，我们利用fuse.js这个库来实现相关信息的模糊搜索，相关文档见[fusejs.io](https://fusejs.io/)。
