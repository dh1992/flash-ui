/**
 * 定义 public api
 */

export * from './fui/fui.module';
export { fuiTranslateService } from './fui/i18n/i18n.module';
export * from './fui/shared/shared';

/**
 * 组件模块
 */
export * from './fui/anchor/anchor.module';
export * from './fui/avatar/avatar.module';
export * from './fui/breadcrumb/breadcrumb.module';
export * from './fui/btn/btn.module';
export * from './fui/carousel/carousel.module';
export * from './fui/datepicker/datepicker.module';
export * from './fui/default/default.module';
export * from './fui/dropdown/dropdown.module';
export * from './fui/file-dialog/file-dialog.module';
export * from './fui/form/form.module';
export * from './fui/icon/icon.module';
export * from './fui/loading/loading.module';
export * from './fui/menu/menu.module';
export * from './fui/message/message.module';
export * from './fui/modal/modal.module';
export * from './fui/pagination/pagination.module';
export * from './fui/popover/popover.module';
export * from './fui/property/property.module';
export * from './fui/search/search.module';
export * from './fui/select/select.module';
export * from './fui/submenu/submenu.module';
export * from './fui/tab/tab.module';
export * from './fui/table/table.module';
export * from './fui/tooltip/tooltip.module';
export * from './fui/timepicker/timepicker.module';
export * from './fui/tree/tree.module';
