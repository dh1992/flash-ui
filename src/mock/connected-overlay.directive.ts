import {
  Directive,
  Input,
  Output,
  EventEmitter,
  NgModule,
} from '@angular/core';

@Directive({
  // eslint-disable-next-line
  selector: '[cdkConnectedOverlay]',
})
export class CdkConnectedOverlayDirective {
  @Output() detach = new EventEmitter();
  @Output() positionChange = new EventEmitter();
  @Output() backdropClick = new EventEmitter();
  @Input() cdkConnectedOverlayOrigin: any;
  @Input() cdkConnectedOverlayHasBackdrop: any;
  @Input() cdkConnectedOverlayPositions: any;
  @Input() cdkConnectedOverlayOpen: any;
}

@NgModule({
  declarations: [CdkConnectedOverlayDirective],
})
export class OverlayModule {}
