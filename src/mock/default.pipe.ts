import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'fuiDefault',
})
export class DefaultPipeStub implements PipeTransform {
  transform(value) {
    return value;
  }
}
