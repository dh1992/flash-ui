import { Component, Input, forwardRef } from '@angular/core';
import { NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';

@Component({
  selector: 'fui-input-number',
  template: '<input type="number">',
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => InputNumberComponent),
      multi: true,
    },
  ],
})
export class InputNumberComponent implements ControlValueAccessor {
  @Input() fuiPlaceHolder = '';
  @Input() fuiMin = -Infinity;
  @Input() fuiMax = Infinity;
  @Input() errorMsgs: string[] = [];
  @Input() step: number;

  writeValue() {}

  registerOnChange() {}

  registerOnTouched() {}
}
