import { Component, Input } from '@angular/core';

@Component({
  selector: 'fui-radio-item',
  template: '<ng-content></ng-content>',
})
export class RadioItemComponent {
  @Input() value: string;
}
