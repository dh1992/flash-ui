import {
  Directive,
  Input,
} from '@angular/core';

@Directive({
  selector: '[fuiTabPane]',
})
export class TabPaneDirective {
  @Input() fuiTabPane: string;

  @Input() fuiTabPaneTemplate;

  @Input() fuiTabPaneIndex;

  @Input() fuiTabPaneForceReload;
}
