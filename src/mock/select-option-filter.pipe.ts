import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'fuiSelectOptionFilterPipe',
})
export class SelectOptionFilterPipeStub implements PipeTransform {
  transform(value) {
    return value;
  }
}
