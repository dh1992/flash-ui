import { NgModule } from '@angular/core';

import {
  SelectComponent,
  InputComponent,
  InputNumberComponent,
  FormItemComponent,
  CheckboxComponent,
  CheckboxGroupComponent,
  CheckboxItemComponent,
  RadioGroupComponent,
  RadioItemComponent,
  SelectOptionComponent,
  SwitchComponent,
  TextareaComponent,
} from './form/index';
import { SelectOptionFilterPipeStub } from './select-option-filter.pipe';
import { TabPaneDirective } from './tab-pane.directive';
import { SearchComponent } from './search.component';
import { DefaultPipeStub } from './default.pipe';
import { TranslatePipeStub } from './i18n/index';

@NgModule({
  declarations: [
    DefaultPipeStub,
    SelectComponent,
    InputComponent,
    InputNumberComponent,
    TextareaComponent,
    FormItemComponent,
    CheckboxComponent,
    CheckboxGroupComponent,
    CheckboxItemComponent,
    RadioGroupComponent,
    RadioItemComponent,
    SelectOptionComponent,
    TabPaneDirective,
    TranslatePipeStub,
    SearchComponent,
    SwitchComponent,
    SelectOptionFilterPipeStub,
  ],
  exports: [
    DefaultPipeStub,
    SelectComponent,
    InputComponent,
    InputNumberComponent,
    TextareaComponent,
    FormItemComponent,
    CheckboxComponent,
    CheckboxGroupComponent,
    CheckboxItemComponent,
    RadioGroupComponent,
    RadioItemComponent,
    SelectOptionComponent,
    TabPaneDirective,
    TranslatePipeStub,
    SearchComponent,
    SwitchComponent,
    SelectOptionFilterPipeStub,
  ],
})
export class MockModule { }
