import { EventEmitter } from '@angular/core';

export class TranslateServiceStub {
  onLangChange = new EventEmitter();
  langBCP47 = 'en-US';
  lang = '';
  setLocale() {}
  translateKey() {}
}
