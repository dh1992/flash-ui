import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CdkTreeModule } from '@angular/cdk/tree';
import { FormsModule, NgModel } from '@angular/forms';
import { FormModule } from '../form/form.module';

import { TreeComponent } from './tree.component';
import { TreeNodeDirective } from './tree-node/tree-node.directive';
import { TreeNodeDefDirective } from './tree-node/tree-node-def.directive';
import { TreeNodeOutletDirective } from './tree-node/tree-node-outlet.directive';
import { TreeNodeToggleDirective } from './tree-node/tree-node-toggle.directive';
import { TreeSelectComponent } from './tree-select/tree-select.component';

export * from './tree.component';

const fui_TREE_DECLARATIONS = [
  TreeComponent,
  TreeNodeDirective,
  TreeNodeDefDirective,
  TreeNodeOutletDirective,
  TreeNodeToggleDirective,
  TreeSelectComponent,
];

@NgModule({
  imports: [
    CommonModule,
    CdkTreeModule,
    FormsModule,
    FormModule,
  ],
  declarations: fui_TREE_DECLARATIONS,
  exports: fui_TREE_DECLARATIONS,
  providers: [
    TreeNodeDefDirective,
  ],
})
export class TreeModule {}

