import { CdkTreeNodeToggle } from '@angular/cdk/tree';
import { coerceBooleanProperty } from '@angular/cdk/coercion';
import {
  Directive,
  Input,
  HostBinding,
} from '@angular/core';

@Directive({
  selector: '[fuiTreeNodeToggle]',
  providers: [{provide: CdkTreeNodeToggle, useExisting: TreeNodeToggleDirective}],
})
export class TreeNodeToggleDirective<T> extends CdkTreeNodeToggle<T> {
  @HostBinding('class.fui-tree-node-toggle') hostClass = true;

  /** Whether expand/collapse the node recursively. */
  @Input ('fuiTreeNodeToggleRecursive')
  get recursive(): boolean {
    return this._recursive;
  }
  set recursive(value: boolean) {
    // TODO: when we remove support for ViewEngine, change this setter to an input
    // alias in the decorator metadata.
    this._recursive = coerceBooleanProperty(value);
  }
}


