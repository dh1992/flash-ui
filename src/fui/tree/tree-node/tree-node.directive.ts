import {
  CDK_TREE_NODE_OUTLET_NODE,
  CdkNestedTreeNode,
  CdkTreeNode,
  CdkTree,
} from '@angular/cdk/tree';
import {
  AfterContentInit,
  ContentChildren,
  Directive,
  ElementRef,
  Input,
  IterableDiffers,
  OnDestroy,
  QueryList,
  HostBinding,
} from '@angular/core';

import { TreeNodeOutletDirective } from './tree-node-outlet.directive';

/**
 * Wrapper for the CdkTree nested node with studio ui styles.
 */
@Directive({
  // eslint-disable-next-line
  selector: 'fui-tree-node',
  exportAs: 'fuiTreeNode',
  providers: [
    {provide: CdkNestedTreeNode, useExisting: TreeNodeDirective},
    {provide: CdkTreeNode, useExisting: TreeNodeDirective},
    {provide: CDK_TREE_NODE_OUTLET_NODE, useExisting: TreeNodeDirective},
  ],
})
export class TreeNodeDirective<T> extends CdkNestedTreeNode<T> implements AfterContentInit, OnDestroy {
  @HostBinding('class.fui-tree-node') hostClass = true;

  /** Tree node data object. */
  @Input('fuiTreeNode') node: T;

  @ContentChildren(TreeNodeOutletDirective, {
    // We need to use `descendants: true`, because Ivy will no longer match
    // indirect descendants if it's left as false.
    descendants: true,
  })
  nodeOutlet: QueryList<TreeNodeOutletDirective>;

  constructor(
    protected _elementRef: ElementRef<HTMLElement>,
    protected _tree: CdkTree<T>,
    protected _differs: IterableDiffers,
  ) {
    super(_elementRef, _tree, _differs);
  }

  ngAfterContentInit() {
    super.ngAfterContentInit();
  }

  ngOnDestroy() {
    super.ngOnDestroy();
  }
}
