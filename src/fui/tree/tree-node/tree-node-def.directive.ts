import { CdkTreeNodeDef } from '@angular/cdk/tree';
import { Input, Directive } from '@angular/core';

@Directive({
  selector: '[fuiTreeNodeDef]',
  providers: [{provide: CdkTreeNodeDef, useExisting: TreeNodeDefDirective}],
})
export class TreeNodeDefDirective<T> extends CdkTreeNodeDef<T> {
  /**
   * Function that should return true if this node template should be used for the provided node
   * data and index.
   */
  @Input('when') fuiTreeNodeDefWhen: (index: number, nodeData: T) => boolean;

  /** Tree node data object. */
  @Input('TreeNode') data: T;
}

