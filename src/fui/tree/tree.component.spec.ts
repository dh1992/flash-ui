import { Component, NO_ERRORS_SCHEMA } from '@angular/core';
import { NestedTreeControl } from '@angular/cdk/tree';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { TreeNodeDefDirective, TreeNodeDirective, TreeNodeOutletDirective, TreeNodeToggleDirective  } from './tree-node';
import { TreeComponent } from './tree.component';

@Component({
  template: `
    <fui-tree [dataSource]="data" [treeControl]="treeControl">
      <fui-tree-node *fuiTreeNodeDef="let node">
        {{node.name}}
        <div>
          <ng-container fuiTreeNodeOutlet></ng-container>
        </div>
      </fui-tree-node>
    </fui-tree>
  `,
})
class TestTreeComponent {
  treeControl: NestedTreeControl<any>;

  data = [
    {
      name: 'domain 1',
      children: [],
    },
  ];

  constructor() {
    this.treeControl = new NestedTreeControl(this._getChildren);
  }
  private _getChildren(node: any) {
    return node.children;
  }
}

describe('TreeComponent', () => {
  let testComponent: TestTreeComponent;
  let component: TreeComponent<any>;
  let fixture: ComponentFixture<TestTreeComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      schemas: [NO_ERRORS_SCHEMA],
      declarations: [
        TreeComponent,
        TestTreeComponent,
        TreeNodeDefDirective,
        TreeNodeDirective,
        TreeNodeOutletDirective,
        TreeNodeToggleDirective,
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestTreeComponent);
    testComponent = fixture.componentInstance;
    component = fixture.debugElement.children[0].componentInstance;
    fixture.detectChanges();
  });

  xit('should create', () => {
    expect(component).toBeTruthy();
  });
});
