import { Component, HostBinding, Input } from '@angular/core';

import { TreeComponent } from '../tree.component';
import { TreeNodeDirective } from '../tree-node/tree-node.directive';
import { TreeSelectControl } from './tree-select.control';

@Component({
  selector: 'fui-tree-select',
  templateUrl: './tree-select.component.html',
})
export class TreeSelectComponent<T> {
  @HostBinding('class.fui-tree-select') hostClass = true;

  /** Disable select if set true. */
  @Input() disabled = false;

  get treeNodeData() {
    return this._treeNode.data;
  }

  select: TreeSelectControl<T>;

  constructor(
    protected _tree: TreeComponent<T>,
    protected _treeNode: TreeNodeDirective<T>,
  ) {
    this.select = this._tree.select();
  }

  onSelected() {
    this.select.refreshSelectState(this.treeNodeData);
    this._tree.onSelected();
  }
}

