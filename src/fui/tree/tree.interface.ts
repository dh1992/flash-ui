import { CdkTree } from '@angular/cdk/tree';

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface TreeSelectControlInterface<T> {
}

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface TreeComponentInterface<T> extends CdkTree<T> {
}

