import {
  OnInit,
  Component,
  Input,
} from '@angular/core';
import {
  animate,
  state,
  style,
  transition,
  trigger,
} from '@angular/animations';

import { OverlayEntry, composePositionSet, Direction } from '../shared/shared';

@Component({
  selector: 'fui-tooltip',
  templateUrl: './tooltip.component.html',
  animations: [
    trigger('popAnimation', [
      state('void', style({opacity: 0, transform: 'scale(0)'})),
      state('true', style({opacity: 1, transform: 'scale(1)'})),
      state('false', style({opacity: 0, transform: 'scale(0)'})),
      transition('* => true', animate('100ms cubic-bezier(0.0, 0.0, 0.2, 1)')),
      transition('* => void', animate('100ms cubic-bezier(0.4, 0.0, 1, 1)')),
    ]),
  ],
  preserveWhitespaces: false,
})
export class TooltipComponent extends OverlayEntry implements OnInit {
  classMap = {};

  /**
   * Tooltip text value.
   * Support multiple lines by useing '\n' in the string.
   */
  @Input() tooltip: string;

  /** Tooltip placement. */
  @Input() placement: Direction = 'top';

  ngOnInit() {
    this.positions = composePositionSet(this.placement);
    this.setPosition(this.placement);
  }

  setPosition(placement: string) {
    this.setClassMap(placement);
  }

  setClassMap(placement: string): void {
    this.classMap = {
      ['fui-tooltip']: true,
      [`fui-tooltip-placement-${placement}`]: true,
    };
  }
}
