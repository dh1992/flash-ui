import { NgModule, Component, EventEmitter } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { By } from '@angular/platform-browser';

import { TooltipDirective } from './tooltip.directive';
import { TooltipModule } from './tooltip.module';

@Component({
  template: '<span [fuiTooltip]="tooltip" [fuiTooltipPlacement]="placement"></span>',
})
class TooltipTestComponent {
  tooltip: string;
  placement: string;
}

describe('TooltipDirective', () => {
  let component: TooltipTestComponent;
  let fixture: ComponentFixture<TooltipTestComponent>;
  let directive: TooltipDirective;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [TooltipModule],
      declarations: [TooltipTestComponent],
      providers: [],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TooltipTestComponent);
    component = fixture.componentInstance;
    directive = fixture.debugElement.query(By.directive(TooltipDirective)).injector.get(TooltipDirective);
    fixture.detectChanges();
  });

  it('should create', () => {
    component.tooltip = 'hello';
    component.placement = 'right';
    fixture.detectChanges();

    expect(directive.tooltipComponent.tooltip).toEqual('hello');
    expect(directive.tooltipComponent.placement).toEqual('right');
  });
});
