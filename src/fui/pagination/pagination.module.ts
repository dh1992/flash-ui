import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { I18nModule } from '../i18n/i18n.module';
import { IconModule } from '../icon/icon.module';
import { BtnModule } from '../btn/btn.module';
import { SelectModule } from '../select/select.module';

import { PaginationComponent } from './pagination.component';

export * from './pagination.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    I18nModule,
    IconModule,
    BtnModule,
    SelectModule,
  ],
  declarations: [
    PaginationComponent,
  ],
  exports: [
    PaginationComponent,
  ],
})
export class PaginationModule { }
