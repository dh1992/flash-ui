import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { I18nModule } from '../i18n/i18n.module';
import { IconModule } from '../icon/icon.module';

import { CarouselContentDirective } from './carousel-content.directive';
import { CarouselComponent } from './carousel.component';

export * from './carousel-content.directive';
export * from './carousel.component';

@NgModule({
  imports: [
    CommonModule,
    I18nModule,
    IconModule,
  ],
  declarations: [
    CarouselContentDirective,
    CarouselComponent,
  ],
  exports: [
    CarouselContentDirective,
    CarouselComponent,
  ],
})
export class CarouselModule { }
