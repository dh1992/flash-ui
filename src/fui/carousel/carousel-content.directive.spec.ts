
import { Component } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { CarouselContentDirective } from './carousel-content.directive';

@Component({
  template: `<div fuiCarouselContent></div>`,
})
class CarouselContentComponent {
}

describe('CarouselContentDirective', () => {
  let component: CarouselContentComponent;
  let fixture: ComponentFixture<CarouselContentComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [
        CarouselContentDirective,
        CarouselContentComponent,
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CarouselContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create an instance', () => {
    const directive = new CarouselContentDirective(fixture);
    expect(directive).toBeTruthy();
  });
});
