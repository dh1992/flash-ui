import {
  Directive,
  HostBinding,
  ElementRef,
} from '@angular/core';

@Directive({
  selector: '[fuiCarouselContent]',
})
export class CarouselContentDirective {
  @HostBinding('class.slick-slide') slickSlide = true;

  @HostBinding('style.flex-shrink') flexShrink = 0;

  @HostBinding('class.slick-active')
  get setActiveClass() {
    return this.isActive === true;
  }

  @HostBinding('style.width.px')
  get setWidth() {
    return this.width;
  }

  width = 0;
  isActive = false;
  nativeElement: HTMLElement;

  constructor(private _el: ElementRef) {
    this.nativeElement = this._el.nativeElement;
  }
}
