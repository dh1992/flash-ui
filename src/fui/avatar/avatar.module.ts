import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BtnModule } from '../btn/btn.module';
import { IconModule } from '../icon/icon.module';

import { AvatarComponent } from './avatar.component';

export * from './avatar.component';

@NgModule({
  imports: [
    CommonModule,
    BtnModule,
    IconModule,
  ],
  declarations: [
    AvatarComponent,
  ],
  exports: [
    AvatarComponent,
  ],
})
export class AvatarModule { }
