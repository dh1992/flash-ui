import { Component, NO_ERRORS_SCHEMA } from '@angular/core';
import {By} from '@angular/platform-browser';
import { ComponentFixture, TestBed, fakeAsync, tick, waitForAsync } from '@angular/core/testing';

import { AvatarComponent } from './avatar.component';

@Component({
  selector: 'fui-avatar-wrapper',
  template: '<fui-avatar [size]="size" [text]="text" [icon]="icon" [src]="src"></fui-avatar>',
})
class TestWrapperComponent {
  size = 'lg';
  text: string;
  icon: string;
  src: string;
}

describe('AvatarComponent', () => {
  let component: AvatarComponent;
  let testComponent: TestWrapperComponent;
  let fixture: ComponentFixture<TestWrapperComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      schemas: [NO_ERRORS_SCHEMA],
      declarations: [
        AvatarComponent,
        TestWrapperComponent,
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestWrapperComponent);
    testComponent = fixture.componentInstance;
    component = fixture.debugElement.children[0].componentInstance;
    fixture.detectChanges();
  });

  it('should set size', () => {
    const avatarDebugElement = fixture.debugElement.query(By.css('.fui-avatar'));
    expect(avatarDebugElement.nativeElement.classList.contains('fui-avatar-lg')).toBe(true);

    testComponent.size = 'sm';
    fixture.detectChanges();
    expect(avatarDebugElement.nativeElement.classList.contains('fui-avatar-sm')).toBe(true);
  });

  it('should set test/icon/src', () => {
    expect(component.displayMode).toEqual('text');

    testComponent.text = 'Test Avatar';
    fixture.detectChanges();
    expect(component.displayMode).toEqual('text');

    testComponent.icon = 'test-icon';
    fixture.detectChanges();
    expect(component.displayMode).toEqual('icon');

    testComponent.src = 'http://img.png';
    fixture.detectChanges();
    expect(component.displayMode).toEqual('img');
  });

  it('should calculate text size', fakeAsync(() => {
    testComponent.text = 'Text Avatar';
    fixture.detectChanges();
    tick(1);
    expect(component.textStyles.display).toEqual('inline-block');

    testComponent.text = 'Text';
    fixture.detectChanges();
    tick(1);
    expect(component.textStyles).toEqual({});
  }));

  it('should deal with img error', () => {
    testComponent.icon = 'test-icon';
    testComponent.src = 'http://img.png';
    fixture.detectChanges();
    const imgDebugElement = fixture.debugElement.query(By.css('img'));
    expect(imgDebugElement.nativeElement.getAttribute('src')).toEqual('http://img.png');

    component.imgError();
    expect(component.displayMode).toEqual('icon');

    testComponent.text = 'Test Avatar';
    testComponent.icon = '';
    fixture.detectChanges();
    component.imgError();
    expect(component.displayMode).toEqual('text');
  });
});
