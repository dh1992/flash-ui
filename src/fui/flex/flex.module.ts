import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FlexDirective } from './flex.directive';

@NgModule({
  imports: [
    CommonModule,
  ],
  declarations: [
    FlexDirective,
  ],
  exports: [
    FlexDirective,
  ],
})
export class FlexModule { }
