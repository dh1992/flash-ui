import { Component } from '@angular/core';

import { FlexDirective } from './flex.directive';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

@Component({
  template: '<div [fuiFlex]="flex"></div>',
})
class TestComponent {
  flex = '1';
}

describe('FlexDirective', () => {
  let component: TestComponent;
  let fixture: ComponentFixture<TestComponent>;
  let element: HTMLElement;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ TestComponent, FlexDirective ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestComponent);
    component = fixture.componentInstance;
    element = fixture.debugElement.nativeElement.querySelector('div');
    fixture.detectChanges();
  });

  it('should set flex', () => {
    expect(element.style.flex).toBe('1 1 0%');
  });

  it('should set flex grow, shrink and basis', () => {
    component.flex = '2 0 auto';
    fixture.detectChanges();
    expect(element.style.flex).toBe('2 0 auto');
  });
});
