import {
  Directive,
  ElementRef,
  Host,
  Input,
} from '@angular/core';

@Directive({
  selector: '[fuiFlex]',
})
export class FlexDirective {

  /** Flex style value. */
  @Input() set fuiFlex(value) {
    this.setFlex(value);
  }

  constructor(@Host() private elementRef: ElementRef) { }

  setFlex(value) {
    const element: HTMLElement = this.elementRef.nativeElement;
    element.style.flex = value;
    // 兼容IE10
    element.style['msFlex'] = value;
  }
}
