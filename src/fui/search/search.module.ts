import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { FormModule } from '../form/form.module';

import { SearchComponent } from './search.component';

export * from './search.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    FormModule,
  ],
  declarations: [
    SearchComponent,
  ],
  exports: [
    SearchComponent,
  ],
})
export class SearchModule { }
