import {
  animate,
  AnimationTriggerMetadata,
  state,
  style,
  transition,
  trigger,
} from '@angular/animations';

export const DropdownAnimation: AnimationTriggerMetadata = trigger('dropdownAnimation', [
  state('bottom', style({
    opacity        : 1,
    transform      : 'scaleY(1)',
    transformOrigin: '0% 0%',
  })),
  transition('* => bottom', [
    style({
      opacity        : 0,
      transform      : 'scaleY(0)',
      transformOrigin: '0% 0%',
    }),
    animate('150ms cubic-bezier(0.25, 0.8, 0.25, 1)'),
  ]),
  state('top', style({
    opacity        : 1,
    transform      : 'scaleY(1)',
    transformOrigin: '0% 100%',
  })),
  transition('* => top', [
    style({
      opacity        : 0,
      transform      : 'scaleY(0)',
      transformOrigin: '0% 100%',
    }),
    animate('150ms cubic-bezier(0.25, 0.8, 0.25, 1)'),
  ]),
  state('right', style({
    opacity        : 1,
    transform      : 'scaleX(1)',
    transformOrigin: '0% 100%',
  })),
  transition('* => right', [
    style({
      opacity        : 0,
      transform      : 'scaleX(0)',
      transformOrigin: '0% 100%',
    }),
    animate('150ms cubic-bezier(0.25, 0.8, 0.25, 1)'),
  ]),
  state('left', style({
    opacity        : 1,
    transform      : 'scaleX(1)',
    transformOrigin: '100% 0%',
  })),
  transition('* => left', [
    style({
      opacity        : 0,
      transform      : 'scaleX(0)',
      transformOrigin: '100% 0%',
    }),
    animate('150ms cubic-bezier(0.25, 0.8, 0.25, 1)'),
  ]),
  state('none', style({
    opacity: 0,
  })),
  transition('* => void', [
    animate('100ms 50ms linear', style({ opacity: 0 })),
  ]),
]);

export const FadeAnimation: AnimationTriggerMetadata = trigger('fadeAnimation', [
  state('void', style({ opacity: 0 })),
  state('true', style({ opacity: 1 })),
  state('false', style({ opacity: 0 })),
  transition('* => true', animate('150ms cubic-bezier(0.0, 0.0, 0.2, 1)')),
  transition('* => void', animate('150ms cubic-bezier(0.4, 0.0, 1, 1)')),
]);

export const HorizontalShowHideAnimation: AnimationTriggerMetadata = trigger('horizontalShowHide', [
  state('void', style({
    transform: 'scaleX(0.8)',
    opacity: 0,
  })),
  state('true', style({
    opacity: 1,
    transform: 'scaleX(1)',
  })),
  transition('void => *', animate('120ms cubic-bezier(0, 0, 0.2, 1)')),
  transition('* => void', style({opacity: 0})),
]);

export const VerticalShowHideAnimation: AnimationTriggerMetadata = trigger('verticalShowHide', [
  state('void', style({
    transform: 'scaleY(0.8)',
    opacity: 0,
  })),
  state('true', style({
    opacity: 1,
    transform: 'scaleY(1)',
  })),
  transition('void => *', animate('120ms cubic-bezier(0, 0, 0.2, 1)')),
  transition('* => void', style({opacity: 0})),
]);
