import { ComponentFixture, TestBed, fakeAsync, flush, tick, waitForAsync } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { Component } from '@angular/core';

import { TranslatePipeStub, DefaultPipeStub } from '../../mock';
import { TabComponent } from './tab.component';
import { TabPaneDirective } from './tab-pane.directive';
import { AnimationBuilder } from '@angular/animations';

@Component({
  template: `
    <fui-tab type="card">
      <div *fuiTabPane="'tab1'">tab1 content</div>
      <div *fuiTabPane="'tab2'">tab2 content</div>
    <fui-tab>
  `,
})
class SimpleTabComponent {
}

@Component({
  selector: 'fui-force-reload-tab-content',
  template: '{{id}}',
})
class ForceReloadTabContentComponent {
  static id = 0;

  id = ++ ForceReloadTabContentComponent.id;
}

@Component({
  template: `
    <fui-tab type="card">
      <fui-force-reload-tab-content *fuiTabPane="'tab1'"></fui-force-reload-tab-content>
      <fui-force-reload-tab-content *fuiTabPane="'tab2'; forceReload: true"></fui-force-reload-tab-content>
    <fui-tab>
  `,
})
class ForceReloadTabComponent {
}


describe('TabComponent', () => {
  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [
        TabComponent,
        TabPaneDirective,
        TranslatePipeStub,
        DefaultPipeStub,
        SimpleTabComponent,
        ForceReloadTabContentComponent,
        ForceReloadTabComponent,
      ],
      providers: [
        {
          provide: AnimationBuilder,
          useValue: {},
        },
      ],
    })
    .compileComponents();
  }));

  describe('basic behaviors', () => {
    let component: TabComponent;
    let fixture: ComponentFixture<SimpleTabComponent>;
    let element: HTMLElement;

    beforeEach(() => {
      fixture = TestBed.createComponent(SimpleTabComponent);
      const debugElement = fixture.debugElement.query(By.directive(TabComponent));
      component = debugElement.componentInstance;
      element = debugElement.nativeElement;
      fixture.detectChanges();
    });

    it('should add fui-tab class to host element', () => {
      expect(element.classList).toContain('fui-tab');
    });

    describe('select pane', () => {
      it('should set default selected pane to 0', fakeAsync(() => {
        flush();
        component.ngAfterViewInit();
        flush();
        expect(component.paneArr[0].active).toBe(true);
        expect(component.paneArr[1].active).toBe(false);
      }));

      it('should select by call selectPane', () => {
        const pane = component.paneArr[1];
        component.selectPane(pane);
        expect(component.paneArr[0].active).toBe(false);
        expect(component.paneArr[1].active).toBe(true);
      });
    });
  });

  describe('force reload', () => {
    let component: TabComponent;
    let fixture: ComponentFixture<ForceReloadTabComponent>;
    let element: HTMLElement;
    // let paneEles: HTMLElement[];

    beforeEach(() => {
      fixture = TestBed.createComponent(ForceReloadTabComponent);
      const debugElement = fixture.debugElement.query(By.directive(TabComponent));
      component = debugElement.componentInstance;
      element = debugElement.nativeElement;
      fixture.detectChanges();
    });

    beforeEach(fakeAsync(() => {
      component.ngAfterViewInit();
      flush();
      fixture.detectChanges();
    }));

    it('should reload pane if set forceReload to true', fakeAsync(() => {
      const [normalPane, forceReloadPane] = component.paneArr;
      expectPaneContent('1');

      component.selectPane(forceReloadPane);
      flush();
      fixture.detectChanges();
      expectPaneContent('2');

      component.selectPane(normalPane);
      flush();
      fixture.detectChanges();
      expectPaneContent('1');

      component.selectPane(forceReloadPane);
      flush();
      fixture.detectChanges();
      expectPaneContent('3');

      function expectPaneContent(content) {
        const paneEles: HTMLElement[] = fixture.debugElement.queryAll(By.directive(ForceReloadTabContentComponent))
          .map((d) => d.nativeElement);
        expect(paneEles.length).toBe(1);
        expect(paneEles[0].innerText.trim()).toBe(content);
      }
    }));
  });
});
