import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DefaultModule } from '../default/default.module';

import { TabComponent } from './tab.component';
import { TabPaneDirective } from './tab-pane.directive';

export * from './tab.component';
export * from './tab-pane.directive';

@NgModule({
  imports: [
    CommonModule,
    DefaultModule,
  ],
  declarations: [
    TabComponent,
    TabPaneDirective,
  ],
  exports: [
    TabComponent,
    TabPaneDirective,
  ],
})
export class TabModule { }
