import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IconModule } from '../icon/icon.module';

import { PropertyComponent } from './property.component';

export * from './property.component';

@NgModule({
  imports: [
    CommonModule,
    IconModule,
  ],
  declarations: [
    PropertyComponent,
  ],
  exports: [
    PropertyComponent,
  ],
})
export class PropertyModule { }
