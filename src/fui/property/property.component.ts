import { Component, OnInit, HostBinding, Input, ElementRef, OnChanges } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'fui-property',
  templateUrl: './property.component.html',
})
export class PropertyComponent implements OnInit, OnChanges {
  @HostBinding('class.fui-property') hostClass = true;

  /** Property icon name. */
  @Input() icon: string;

  /** Property label text. */
  @Input() label: string;

  /** Property value. */
  @Input() value: string;

  /** Property color. */
  @Input() color: 'danger' | 'normal' = 'normal';

  /** Change value to a link if set. */
  @Input() link: string;

  classMap = {};

  constructor(
    private elementRef: ElementRef,
    private router: Router,
  ) { }

  ngOnInit() {
  }

  ngOnChanges(changes) {
    if (this.color && changes.color) {
      this.setClassMap(changes.color);
    }
  }

  setClassMap(colorChange) {
    if (colorChange.currentValue && colorChange.previousValue) {
      this.elementRef.nativeElement.classList.remove(`fui-property-${colorChange.previousValue}`);
      this.elementRef.nativeElement.classList.add(`fui-property-${colorChange.currentValue}`);
    } else {
      this.elementRef.nativeElement.classList.add(`fui-property-${colorChange.currentValue}`);
    }
  }

  goToLink() {
    if (this.link) {
      this.router.navigateByUrl(this.link);
    }
  }
}
