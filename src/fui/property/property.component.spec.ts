import {Component, NO_ERRORS_SCHEMA} from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { Router } from '@angular/router';

import { TranslatePipeStub } from '../../mock';

import { PropertyComponent } from './property.component';

class RouterStub {
  navigateByUrl() {}
}

@Component({
  template: `
    <fui-property
      icon="pile"
      value="商务部仓储项目"
      label="项目项目项目"
      link="/"
      [color]="color"
    ></fui-property>
  `,
})
class TestComponent {
  color: string;
}

describe('PropertyComponent', () => {
  let component: TestComponent;
  let fixture: ComponentFixture<TestComponent>;
  let property: HTMLElement;
  let router: Router;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      schemas: [NO_ERRORS_SCHEMA],
      declarations: [
        TestComponent,
        PropertyComponent,
        TranslatePipeStub,
      ],
      providers: [
        {
          provide: Router,
          useClass: RouterStub,
        },
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    router = TestBed.inject(Router);
    fixture = TestBed.createComponent(TestComponent);
    component = fixture.componentInstance;
    component.color = 'danger';
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('should change color when color type change', () => {
    property = fixture.debugElement.query(By.css('fui-property')).nativeElement;
    expect(property.classList.contains('fui-property-danger')).toBeTruthy();

    component.color = 'normal';
    fixture.detectChanges();
    expect(property.classList.contains('fui-property-normal')).toBeTruthy();
    expect(property.classList.contains('fui-property-danger')).toBeFalsy();
  });

  it('should gotoLink when value click and have line', () => {
    spyOn(router, 'navigateByUrl');
    property = fixture.debugElement.query(By.css('fui-property')).nativeElement;
    const link: HTMLElement = property.querySelector('.link');
    link.click();
    expect(router.navigateByUrl).toHaveBeenCalledWith('/');
  });
});
