import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FileDialogDirective } from './file-dialog.directive';

export * from './file-dialog.directive';

@NgModule({
  imports: [
    CommonModule,
  ],
  declarations: [
    FileDialogDirective,
  ],
  exports: [
    FileDialogDirective,
  ],
})
export class FileDialogModule { }
