import { ChangeDetectorRef } from '@angular/core';
import { Subject } from 'rxjs';

import { fuiTranslateService } from './translate.service';
import { TranslatePipe } from './translate.pipe';

class fuiTranslateServiceStub {
  onLangChange = new Subject();
  translateKey(key: string): any {}
}

class ChangeDetectorRefStub {
  markForCheck() {}
}

describe('FormErrorPipe', () => {
  it('create an instance', () => {
    const translateService = new fuiTranslateServiceStub();
    const ref = new ChangeDetectorRefStub();
    const pipe = new TranslatePipe(translateService as any, ref as ChangeDetectorRef);

    spyOn(translateService, 'translateKey').and.returnValue('test key');
    pipe.lastKey = 'TEST_KEY';
    translateService.onLangChange.next(translateService);
    expect(translateService.translateKey).toHaveBeenCalledWith('TEST_KEY');
    expect(pipe.transform('TEST_KEY')).toEqual('test key');

    pipe.ngOnDestroy();
  });
});
