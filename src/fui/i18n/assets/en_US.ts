export default {
  fui: {
    CHART: {
      NO_DATA: 'No Data',
    },
    TABLE: {
      NO_CONTENT: 'No Content',
    },
    PAGINATION: {
      SHOW_ROW_COUNT: 'Row count',
      GO_TO: 'Go to',
      GO_TO_PAGE: '',
      CONFIRM: 'Confirm',
      PER_PAGE: '',
      ROW: 'per page',
      TOTAL: '',
      TOTAL_ROW: 'in total',
    },
    FORM: {
      REQUIRED: 'Required',
      EMAIL_INVALID_ERROR: 'Invalid Email',
      MINLENGTH_ERROR: 'Min Length Restrict',
      MAXLENGTH_ERROR: 'Max Length Restrict',
      CHECKBOX_COMPEL_ERROR: 'Check it',
      VALIDATION_FAILED: 'Validation Failed',
      INPUT_MAX_VALUE_ERROR: 'No bigger than',
      INPUT_MIN_VALUE_ERROR: 'No smaller than',
      EQUAL_ERROR: 'Not Equal',
      EQUAL_PASSWORD_ERROR: 'Password not equal',
      TIME_SELECT: 'Select time',
    },
    MODAL: {
      CONFIRM: 'Confirm',
      CANCEL: 'Cancel',
      UNKNOWN_ERROR: 'Unknown Error',
      ERROR: 'Error',
      SYSTEM_TIP: 'System Tip',
    },
    NAV: {
      MORE: 'More',
    },
    SELECT: {
      ALL: '(Select all)',
      NO_OPTIONS: '-- No options --',
      LOADING_OPTIONS: 'Loading...',
    },
    DATEPICKER: {
      CHOOSE_DATE: 'Choose date',
      CHOOSE_TIME: 'Choose time',
      THIS_MOMENT: 'This moment',
      TODAY: 'Today',
      WEEK_NAMES: 'Mo,Tu,We,Th,Fr,Sa,Su',
      MONTH_NAMES: 'January,February,March,April,May,June,July,August,September,October,November,December',
    },
  },
};
