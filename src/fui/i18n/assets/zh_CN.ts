export default {
  fui: {
    CHART: {
      NO_DATA: '没有数据',
    },
    TABLE: {
      NO_CONTENT: '没有内容',
    },
    PAGINATION: {
      SHOW_ROW_COUNT: '显示行数',
      GO_TO: '前往',
      GO_TO_PAGE: '页',
      CONFIRM: '确定',
      PER_PAGE: '每页',
      ROW: '条',
      TOTAL: '共',
      TOTAL_ROW: '条',
    },
    FORM: {
      REQUIRED: '必填',
      EMAIL_INVALID_ERROR: '邮箱格式错误',
      MINLENGTH_ERROR: '最小长度限制',
      MAXLENGTH_ERROR: '最大长度限制',
      CHECKBOX_COMPEL_ERROR: '此项必选',
      VALIDATION_FAILED: '非法字段',
      INPUT_MAX_VALUE_ERROR: '不得大于',
      INPUT_MIN_VALUE_ERROR: '不得小于',
      EQUAL_ERROR: '不相等',
      EQUAL_PASSWORD_ERROR: '密码不一致',
      TIME_SELECT: '选择时间',
    },
    MODAL: {
      CONFIRM: '确认',
      CANCEL: '取消',
      UNKNOWN_ERROR: '未知错误',
      ERROR: '错误',
      SYSTEM_TIP: '系统提示',
    },
    NAV: {
      MORE: '更多',
    },
    SELECT: {
      ALL: '(全选)',
      NO_OPTIONS: '-- 没有选项 --',
      LOADING_OPTIONS: '加载中...',
    },
    DATEPICKER: {
      CHOOSE_DATE: '选择日期',
      CHOOSE_TIME: '选择时间',
      THIS_MOMENT: '此刻',
      TODAY: '今天',
      WEEK_NAMES: '一,二,三,四,五,六,日',
      MONTH_NAMES: '一月,二月,三月,四月,五月,六月,七月,八月,九月,十月,十一月,十二月',
    },
  },
};
