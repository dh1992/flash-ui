import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TranslatePipe } from './translate.pipe';
import { fui_I18N_SERVICE_PROVIDER } from './translate.service';
import { fui_I18N_LANGE } from './i18n-token';

export * from './translate.service';
export * from './i18n-token';
export * from './translate.pipe';


@NgModule({
  imports: [
    CommonModule,
  ],
  declarations: [
    TranslatePipe,
  ],
  providers: [
    { provide: fui_I18N_LANGE, useValue: 'zh_CN' },
    fui_I18N_SERVICE_PROVIDER,
  ],
  exports: [
    TranslatePipe,
  ],
})
export class I18nModule { }
