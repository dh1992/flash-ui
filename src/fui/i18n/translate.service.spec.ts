import { TestBed, inject } from '@angular/core/testing';

import { fuiTranslateService } from './translate.service';

describe('TranslateService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [fuiTranslateService],
    });
  });

  it('should be created', inject([fuiTranslateService], (service: fuiTranslateService) => {
    service.setLocale('en_US');
    expect(service.lang).toEqual('en_US');
    expect(service.langBCP47).toEqual('en-US');
  }));

  it('should be created', inject([fuiTranslateService], (service: fuiTranslateService) => {
    expect(service.translateKey('')).toEqual('');

    service.setLocale('es_PT');
    expect(service.translateKey('test')).toEqual('test');

    service.setLocale('en_US');
    expect(service.translateKey('fui.CHART.NO_DATA')).toEqual('No Data');
    expect(service.translateKey('fui.CHART.NO_DATA_FAKE')).toEqual('fui.CHART.NO_DATA_FAKE');
    expect(service.translateKey('fui.CHART_FAKE.NO_DATA_FAKE')).toEqual('fui.CHART_FAKE.NO_DATA_FAKE');
  }));
});
