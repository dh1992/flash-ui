import {
  Pipe,
  PipeTransform,
  OnDestroy,
  OnInit,
  EventEmitter,
  ChangeDetectorRef,
} from '@angular/core';
import { Subscription } from 'rxjs';

import { fuiTranslateService } from './translate.service';

@Pipe({
  name: 'translate',
  pure: false,
})
export class TranslatePipe implements PipeTransform, OnDestroy {
  onLangChange: Subscription;
  lastKey: string;
  value: string;

  constructor(
    private translate: fuiTranslateService,
    private ref: ChangeDetectorRef,
  ) {
    this.onLangChange = this.translate.onLangChange.subscribe(() => {
      this.value = this.translate.translateKey(this.lastKey);
      this.ref.markForCheck();
    });
  }

  transform(key: string): any {
    if (key === this.lastKey) {
      return this.value;
    }
    this.value = this.translate.translateKey(key);
    this.lastKey = key;
    return this.value;
  }

  ngOnDestroy() {
    if (this.onLangChange) {
      this.onLangChange.unsubscribe();
      this.onLangChange = undefined;
    }
  }

}
