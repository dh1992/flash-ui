import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { TranslatePipeStub } from '../../mock';
import { SubmenuComponent } from './submenu.component';

describe('SubmenuComponent', () => {
  let component: SubmenuComponent;
  let fixture: ComponentFixture<SubmenuComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      schemas: [ NO_ERRORS_SCHEMA ],
      declarations: [
        SubmenuComponent,
        TranslatePipeStub,
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubmenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should toggle component when clicking head', () => {
    expect(fixture.debugElement.classes['fui-submenu-collapsed']).toBe(false);
    component.toggle();
    fixture.detectChanges();
    expect(fixture.debugElement.classes['fui-submenu-collapsed']).toBe(true);
    component.toggle();
    fixture.detectChanges();
    expect(fixture.debugElement.classes['fui-submenu-collapsed']).toBe(false);
  });
});
