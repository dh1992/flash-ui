import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { I18nModule } from '../i18n/i18n.module';
import { IconModule } from '../icon/icon.module';
import { AvatarModule } from '../avatar/avatar.module';
import { DropdownModule } from '../dropdown/dropdown.module';

import { SubmenuComponent } from './submenu.component';
import { SubmenuListComponent } from './submenu-list/submenu-list.component';

export * from './submenu.component';
export * from './submenu-list/submenu-list.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    I18nModule,
    AvatarModule,
    DropdownModule,
    IconModule,
  ],
  declarations: [
    SubmenuComponent,
    SubmenuListComponent,
  ],
  exports: [
    SubmenuComponent,
    SubmenuListComponent,
  ],
})
export class SubmenuModule { }

