import {
  Component,
  Input,
  Output,
  HostBinding,
  EventEmitter,
} from '@angular/core';

@Component({
  selector: 'fui-submenu',
  templateUrl: './submenu.component.html',
})
export class SubmenuComponent {
  @HostBinding('class.fui-submenu-collapsed') collapsed = false;
  @HostBinding('class.fui-submenu') hostClass = true;

  /** Nav back url. */
  @Input() backUrl: string;

  collapseChange = new EventEmitter();

  constructor() {}

  toggle() {
    this.collapsed = !this.collapsed;
    this.collapseChange.emit(this.collapsed);
  }
}
