import {
  Component,
  OnInit,
  Input,
  Host,
} from '@angular/core';
import { Router } from '@angular/router';
import { SafeHtml } from '@angular/platform-browser';

import { SubmenuComponent } from '../submenu.component';
import { DropdownComponent } from '../../dropdown/dropdown.component';

class Suffix {
  icon?: string;
  color?: string;
  size?: string;
}

export interface SubmenuItem {
  count?: number;
  name: string;
  url: string;
  icon?: string;
  img?: string;
  suffix?: Suffix;
  children?: SubmenuItem[];
  childrenOpen?: boolean;
}

@Component({
  selector: 'fui-submenu-list',
  templateUrl: './submenu-list.component.html',
})
export class SubmenuListComponent implements OnInit {

  /** Menu items. */
  @Input() items: SubmenuItem[];

  /** Menu title. */
  @Input() menuTitle: string;

  constructor(private router: Router, @Host() private submenu: SubmenuComponent) { }

  ngOnInit() {
    // 展开对应的项目
    this.items.forEach((item: SubmenuItem) => {
      if (this.isActive(item)) {
        item.childrenOpen = true;
      }
    });
  }

  clickItem(item) {
    this.closeAllChildren(item);
    if (item.children) {
      item.childrenOpen = !item.childrenOpen;
    } else {
      this.navigate(item.url);
    }
  }

  navigate(url) {
    this.router.navigateByUrl(url);
  }

  isActive(item) {
    if (!item.url) {
      return false;
    }
    return this.router.isActive(item.url, false);
  }

  getAvatar(item) {
    return item.name.substr(0, 1);
  }

  isCollapsed() {
    return this.submenu.collapsed;
  }

  showToggle(item) {
    return item.children && !this.submenu.collapsed;
  }

  mouseEnter(item) {
    if (this.isCollapsed()) {
      this.closeAllChildren(item);
      if (item.children) {
        item.childrenOpen = true;
      }
    }
  }

  /**
   * 折叠所有项目
   *
   * @param SubmenuItem 指定不用折叠的某个项目
   */
  private closeAllChildren(item?: SubmenuItem) {
    this.items.forEach((_item: SubmenuItem) => {
      if (item !== _item && _item.children) {
        _item.childrenOpen = false;
      }
    });
  }
}
