import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { HttpClient } from '@angular/common/http';

import { fuiMessageService } from '../message/message.module';
import { UploadComponent } from './upload.component';

class HttpClientStub {
  request() {}
}

class fuiMessageServiceStub {

}

describe('UploadComponent', () => {
  let component: UploadComponent;
  let fixture: ComponentFixture<UploadComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      schemas: [NO_ERRORS_SCHEMA],
      declarations: [ UploadComponent ],
      providers: [
        {
          provide: HttpClient,
          useClass: HttpClientStub,
        },
        {
          provide: fuiMessageService,
          useClass: fuiMessageServiceStub,
        },
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UploadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
