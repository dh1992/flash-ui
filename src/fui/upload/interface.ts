/** 状态 */
export type UploadFileStatus = 'exception' | 'success' | 'active';

/** 文件对象 */
export interface UploadFile {
  uid: string;
  size: number;
  name: string;
  filename?: string;
  lastModified?: string;
  lastModifiedDate?: Date;
  url?: string;
  status?: UploadFileStatus;
  originFileObj?: File;
  percent?: number;
  response?: any;
  error?: any;
  errorText: string;
  linkProps?: any;
  type: string;
  subscriptions: any[];

  [key: string]: any;
}

export interface UploadOptions {
  action: string;
  fileTypes: string[];
  fileSize: number;
  fileSizeErrorText: string;
  fileTypesErrorText: string;
}
