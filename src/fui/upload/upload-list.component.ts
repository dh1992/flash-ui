import {
  Component,
  OnInit,
  Input,
} from '@angular/core';
import { UploadFile } from './interface';
import { UploaderService } from './upload.service';

@Component({
  selector: 'fui-upload-list',
  templateUrl: './upload-list.component.html',
})

export class UploadListComponent implements OnInit {
  /** Upload tip message. */
  @Input() uploadTips: string;

  /** Show upload tip message. */
  @Input() showUploadTip = true;

  constructor(
    public uploaderService: UploaderService,
  ) { }

  handleRemove(file: UploadFile): void {
    file.subscriptions.pop().unsubscribe();
    this.uploaderService.removeFileItem(file);
  }

  handleResend(file: UploadFile): void {
    file.status = 'active';
    file.error = null;
    file.errorText = '';
    file.percent = 0;
    file = Object.assign({}, file);
    file.subscriptions.pop().unsubscribe();
    this.uploaderService.upload(file);
  }

  ngOnInit() {
  }
}
