import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UploadComponent } from './upload.component';
import { UploadBtnComponent } from './upload-btn.component';
import { UploadListComponent } from './upload-list.component';
import { UploaderService } from './upload.service';
import { IconModule } from '../icon/icon.module';
import { MessageModule } from '../message/message.module';
import { ProgressModule } from '../progress/progress.module';

@NgModule({
  imports: [
    CommonModule,
    IconModule,
    MessageModule,
    ProgressModule,
  ],
  declarations: [
    UploadComponent,
    UploadBtnComponent,
    UploadListComponent,
  ],
  exports: [
    UploadComponent,
    UploadBtnComponent,
    UploadListComponent,
  ],
  entryComponents: [
    UploadComponent,
    UploadBtnComponent,
    UploadListComponent,
  ],
  providers: [
    UploaderService,
  ],
})
export class UploadModule { }
