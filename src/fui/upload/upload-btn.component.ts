import {
  Component,
  OnInit,
  HostListener,
  ViewChild,
  ElementRef,
} from '@angular/core';
import { UploaderService } from './upload.service';

@Component({
  selector: 'fui-upload-btn',
  templateUrl: './upload-btn.component.html',
})

export class UploadBtnComponent implements OnInit {
  @ViewChild('file') file: ElementRef;

  @HostListener('click')
  onClick(): void {
    (this.file.nativeElement as HTMLInputElement).click();
  }

  constructor(
    private uploaderService: UploaderService,
  ) { }

  onChange(e: Event): void {
    const fileItem = this.file.nativeElement.files[0];
    if (fileItem) {
      this.uploaderService.upload(fileItem);
    }
    this.file.nativeElement.value = '';
  }

  ngOnInit() {
  }
}
