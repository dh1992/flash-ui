import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IconModule } from '../icon/icon.module';
import { OverlayModule } from '@angular/cdk/overlay';
import { AvatarModule } from '../avatar/avatar.module';

import { MenuComponent } from './menu.component';
import { MenuItemGroupComponent } from './menu-item-group.component';
import { MenuItemComponent } from './menu-item.component';
import { MenuRouterHandler } from './menu-state.service';
import { SubMenuComponent } from './sub-menu.component';

export * from './menu.component';
export * from './menu-item-group.component';
export * from './menu-item.component';
export * from './menu-state.service';
export * from './sub-menu.component';

@NgModule({
  imports: [
    CommonModule,
    OverlayModule,
    IconModule,
    AvatarModule,
  ],
  declarations: [
    MenuComponent,
    MenuItemGroupComponent,
    MenuItemComponent,
    SubMenuComponent,
  ],
  providers: [
    MenuRouterHandler,
  ],
  exports: [
    MenuComponent,
    MenuItemGroupComponent,
    MenuItemComponent,
    SubMenuComponent,
  ],
})
export class MenuModule { }
