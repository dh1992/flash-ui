import { NO_ERRORS_SCHEMA } from '@angular/core';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { ComponentFixture, TestBed, fakeAsync, tick, waitForAsync } from '@angular/core/testing';
import { Router } from '@angular/router';
import { OverlayModule } from '@angular/cdk/overlay';
import { By } from '@angular/platform-browser';
import { of } from 'rxjs';

import { MenuComponent } from './menu.component';
import { SubMenuComponent } from './sub-menu.component';
import { MenuStateService, MenuRouterHandler } from './menu-state.service';

class RouterStub {
  events = of();
  isActive() {
    return true;
  }
  navigateByUrl() {}
}

class MenuComponentStub {
  mode = 'float';
}

describe('MenuComponent', () => {
  let component: SubMenuComponent;
  let fixture: ComponentFixture<SubMenuComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      schemas: [NO_ERRORS_SCHEMA],
      imports: [
        NoopAnimationsModule,
        OverlayModule,
      ],
      declarations: [
        SubMenuComponent,
      ],
    })
    .overrideComponent(SubMenuComponent, {
      set: {
        providers: [
          {
            provide: Router,
            useClass: RouterStub,
          },
          {
            provide: MenuComponent,
            useClass: MenuComponentStub,
          },
          MenuRouterHandler,
          MenuStateService,
        ],
      },
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  afterEach(() => {
    fixture.destroy();
  });

  it('should handle mouseenter/mouseleave', fakeAsync(() => {
    spyOn(component, 'show');
    spyOn(component, 'hide');

    fixture.nativeElement.dispatchEvent(new Event('mouseenter'));
    tick(200);
    fixture.detectChanges();
    expect(component.hovered).toEqual(true);
    expect(component.show).toHaveBeenCalled();

    component.visible.next(true);
    fixture.nativeElement.dispatchEvent(new Event('mouseleave'));
    tick(200);
    fixture.detectChanges();
    expect(component.hovered).toEqual(false);
    expect(component.hide).toHaveBeenCalled();
  }));

  it('should handle mouseenterfloat/mouseleavefloat', () => {
    spyOn(component, 'hideFloatChildren');

    component.mouseenterFloat();
    expect(component.floatHovered).toEqual(true);

    component.mouseleaveFloat();
    expect(component.floatHovered).toEqual(false);
    expect(component.hideFloatChildren).toHaveBeenCalled();
  });

  it('should toggle open', () => {
    component.key = 'test';

    component.toggleOpen();
    expect(component.state.submenuOpens['test']).toEqual(true);
    component.toggleOpen();
    expect(component.state.submenuOpens['test']).toEqual(false);
  });
});
