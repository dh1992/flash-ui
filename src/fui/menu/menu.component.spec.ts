import { Component, NO_ERRORS_SCHEMA } from '@angular/core';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { Router } from '@angular/router';
import { OverlayModule } from '@angular/cdk/overlay';
import { By } from '@angular/platform-browser';
import { of } from 'rxjs';

import { MenuComponent } from './menu.component';
import { SubMenuComponent } from './sub-menu.component';
import { MenuItemComponent } from './menu-item.component';
import { MenuStateService, MenuRouterHandler } from './menu-state.service';

@Component({
  template: `
  <fui-menu selectedKey="user" routeHandler="route" (selectChange)="selectChange($event)">
    <fui-menu-item icon="user" key="user">
      user
    </fui-menu-item>
    <fui-sub-menu icon="user-group" menuTitle="group">
      <fui-menu-item key="role">
        role
      </fui-menu-item>
      <fui-menu-item key="group">
        group
      </fui-menu-item>
    </fui-sub-menu>
  </fui-menu>
  `,
})
class TestComponent {}

class RouterStub {
  events = of();
  isActive() {
    return true;
  }
  navigateByUrl() {}
}

describe('MenuComponent', () => {
  let testComponent: TestComponent;
  let component: MenuComponent;
  let fixture: ComponentFixture<TestComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      schemas: [NO_ERRORS_SCHEMA],
      imports: [
        NoopAnimationsModule,
        OverlayModule,
      ],
      declarations: [
        TestComponent,
        MenuComponent,
        SubMenuComponent,
        MenuItemComponent,
      ],
    })
    .overrideComponent(MenuComponent, {
      set: {
        providers: [
          {
            provide: Router,
            useClass: RouterStub,
          },
          MenuRouterHandler,
          MenuStateService,
        ],
      },
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestComponent);
    testComponent = fixture.componentInstance;
    component = fixture.debugElement.query(By.directive(MenuComponent)).componentInstance;
    fixture.detectChanges();
  });

  afterEach(() => {
    fixture.destroy();
  });

  it('should toggle', () => {
    component.toggle();
    expect(component.collapsed).toEqual(true);
  });
});
