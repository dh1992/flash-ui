import { Component, OnInit, HostBinding, Input, TemplateRef } from '@angular/core';
import { MenuComponent } from './menu.component';

@Component({
  selector: 'fui-menu-item-group',
  templateUrl: './menu-item-group.component.html',
})
export class MenuItemGroupComponent implements OnInit {
  @HostBinding('class.fui-menu-item-group') hostClass = true;

  @HostBinding('class.collapsed') get collapsedClass() {
    return this.menu.collapsed;
  }

  /** Menu title. */
  @Input() menuTitle: string;

  constructor(private menu: MenuComponent) { }

  ngOnInit() {
  }

}
