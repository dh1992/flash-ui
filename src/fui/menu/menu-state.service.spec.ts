import { TestBed, inject } from '@angular/core/testing';

import { MenuStateService, MenuRouterHandler } from './menu-state.service';

describe('MenuStateService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        MenuStateService,
        {
          provide: MenuRouterHandler,
          useValue: {},
        },
      ],
    });
  });

  it('should be created', inject([MenuStateService], (service: MenuStateService) => {
    expect(service).toBeTruthy();
  }));
});
