# Menu

绑定`selectedKey`和`selectChange`可以传入选中的menu item和获取menu选择变化。menu item有`key`和`icon`输入，`key`为必填。`icon`为选填。

<!-- example(menu-overview) -->

绑定`routeHandler`可以让menu自动处理路由，此时`key`为每个menu item的路由绑定，类似于`[routerLink]`。

<!-- example(menu-route) -->

为了让没有icon输入的menu在折叠的状态下有缩略图的效果，加入了avatar

<!-- example(menu-avatar) -->

对于更加复杂的情形menu，可以对`fui-menu-item`和`fui-sub-menu`传入`ng-template`。模板里注入了submenu/item的context，可用于获取`active`和`collpased`状态并且定制里面的内容。

<!-- example(menu-custom) -->

<!-- example(menu-complicated) -->
