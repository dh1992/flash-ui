import {
  Component,
  OnInit,
  OnChanges,
  Input,
  HostBinding,
} from '@angular/core';

@Component({
  selector: 'fui-prompt',
  templateUrl: './prompt.component.html',
})
export class PromptComponent implements OnInit, OnChanges {
  @HostBinding('class') hostClass: string;

  /** Prompt type, support 'success', 'error', 'warning', 'info'. */
  @Input() type: 'success' | 'error' | 'warning' | 'info';

  constructor() { }

  ngOnInit() {
  }

  ngOnChanges() {
    this.hostClass = 'fui-prompt fui-prompt-' + this.type;
  }
}
