import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IconModule } from '../icon/icon.module';

import { PromptComponent } from './prompt.component';

@NgModule({
  imports: [
    CommonModule,
    IconModule,
  ],
  declarations: [
    PromptComponent,
  ],
  exports: [
    PromptComponent,
  ],
})
export class PromptModule { }
