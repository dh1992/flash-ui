import { ConnectionPositionPair } from '@angular/cdk/overlay';

export const POSITION_MAP = {
  top         : {
    originX : 'center',
    originY : 'top',
    overlayX: 'center',
    overlayY: 'bottom',
  },
  topCenter   : {
    originX : 'center',
    originY : 'top',
    overlayX: 'center',
    overlayY: 'bottom',
  },
  topLeft     : {
    originX : 'start',
    originY : 'top',
    overlayX: 'start',
    overlayY: 'bottom',
  },
  topRight    : {
    originX : 'end',
    originY : 'top',
    overlayX: 'end',
    overlayY: 'bottom',
  },
  right       : {
    originX : 'end',
    originY : 'center',
    overlayX: 'start',
    overlayY: 'center',
  },
  rightTop    : {
    originX : 'end',
    originY : 'top',
    overlayX: 'start',
    overlayY: 'top',
  },
  rightBottom : {
    originX : 'end',
    originY : 'bottom',
    overlayX: 'start',
    overlayY: 'bottom',
  },
  bottom      : {
    originX : 'center',
    originY : 'bottom',
    overlayX: 'center',
    overlayY: 'top',
  },
  bottomCenter: {
    originX : 'center',
    originY : 'bottom',
    overlayX: 'center',
    overlayY: 'top',
  },
  bottomLeft  : {
    originX : 'start',
    originY : 'bottom',
    overlayX: 'start',
    overlayY: 'top',
  },
  bottomRight : {
    originX : 'end',
    originY : 'bottom',
    overlayX: 'end',
    overlayY: 'top',
  },
  left        : {
    originX : 'start',
    originY : 'center',
    overlayX: 'end',
    overlayY: 'center',
  },
  leftTop     : {
    originX : 'start',
    originY : 'top',
    overlayX: 'end',
    overlayY: 'top',
  },
  leftBottom  : {
    originX : 'start',
    originY : 'bottom',
    overlayX: 'end',
    overlayY: 'bottom',
  },
};

export const DEFAULT_POSITIONS = [
  {
    originX : 'center',
    originY : 'top',
    overlayX: 'center',
    overlayY: 'bottom',
  },
  {
    originX : 'end',
    originY : 'center',
    overlayX: 'start',
    overlayY: 'center',
  },
  {
    originX : 'center',
    originY : 'bottom',
    overlayX: 'center',
    overlayY: 'top',
  },
  {
    originX : 'start',
    originY : 'center',
    overlayX: 'end',
    overlayY: 'center',
  },
] as ConnectionPositionPair[];
