import {
  Directive,
  Component,
  Output,
  Input,
  EventEmitter,
  NO_ERRORS_SCHEMA,
} from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ComponentFixture, TestBed, inject, waitForAsync } from '@angular/core/testing';
import { OverlayModule, OverlayContainer } from '@angular/cdk/overlay';

import { PopoverComponent } from './popover.component';
import { PopoverDirective } from './popover.directive';

@Directive({
  // eslint-disable-next-line
  selector: '[cdkConnectedOverlay]',
})
class CdkConnectedOverlayDirective {
  @Output() detach = new EventEmitter();
  @Output() positionChange = new EventEmitter();
  @Input() cdkConnectedOverlayOrigin: any;
  @Input() cdkConnectedOverlayHasBackdrop: any;
  @Input() cdkConnectedOverlayPositions: any;
  @Input() cdkConnectedOverlayOpen: any;
}

@Component({
  template: `
    <div [fuiPopover]="popover"></div>
    <fui-popover #popover [placement]="placement"></fui-popover>
  `,
})
class TestWrapperComponent {
  placement = 'top';
}

describe('PopoverComponent', () => {
  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [OverlayModule, BrowserAnimationsModule],
      declarations: [
        PopoverComponent,
        PopoverDirective,
        TestWrapperComponent,
      ],
    })
    .compileComponents();
  }));

  describe('component', () => {
    let component: PopoverComponent;
    let fixture: ComponentFixture<PopoverComponent>;

    beforeEach(() => {
      fixture = TestBed.createComponent(PopoverComponent);
      component = fixture.componentInstance;
      fixture.detectChanges();
    });

    it('should set classmap initially', () => {
      expect(component.classMap).toEqual({
        'fui-popover': true,
        'fui-popover-placement-top': true,
      });
    });

    it('should set position when position is different', () => {
      component.setPosition('bottom');
      expect(component.classMap).toEqual({
        'fui-popover': true,
        'fui-popover-placement-bottom': true,
      });
    });
  });

  describe('behaviors', () => {
    let component: PopoverComponent;
    let triggerEle: HTMLElement;
    let testComponent: TestWrapperComponent;
    let fixture: ComponentFixture<TestWrapperComponent>;
    let overlayContainer: OverlayContainer;
    let overlayContainerElement: HTMLElement;

    beforeEach(() => {
      fixture = TestBed.createComponent(TestWrapperComponent);
      testComponent = fixture.componentInstance;
      triggerEle = fixture.debugElement.children[0].nativeElement;
      component = fixture.debugElement.children[1].componentInstance;
      fixture.detectChanges();
    });

    beforeEach(waitForAsync(() => {
      inject([OverlayContainer], (oc: OverlayContainer) => {
        overlayContainer = oc;
        overlayContainerElement = oc.getContainerElement();
      })();
    }));

    it('should show/hide when mouse enter/leave', () => {
      let popover = overlayContainerElement.querySelector('.fui-popover');
      expect(popover).toBeFalsy();
      triggerEle.dispatchEvent(new Event('mouseenter'));
      fixture.detectChanges();

      popover = overlayContainerElement.querySelector('.fui-popover');
      expect(popover).toBeTruthy();
    });

  });
});
