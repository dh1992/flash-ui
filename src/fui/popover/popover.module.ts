import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OverlayModule } from '@angular/cdk/overlay';

import { PopoverComponent } from './popover.component';
import { PopoverDirective } from './popover.directive';

export * from './popover.component';
export * from './popover.directive';

@NgModule({
  imports: [
    CommonModule,
    OverlayModule,
  ],
  declarations: [
    PopoverComponent,
    PopoverDirective,
  ],
  exports: [
    PopoverComponent,
    PopoverDirective,
  ],
})
export class PopoverModule { }
