import { Component, forwardRef, NO_ERRORS_SCHEMA } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { ComponentFixture, TestBed, fakeAsync, tick, waitForAsync } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';

import { TimepickerInnerComponent } from './timepicker-inner.component';

@Component({
  template: '<fui-timepicker-inner [(ngModel)]="value"></fui-timepicker-inner>',
})
class TestWrapperComponent {
  value = new Date();
}

describe('TimepickerInnerComponent', () => {
  let component: TimepickerInnerComponent;
  let testComponent: TestWrapperComponent;
  let fixture: ComponentFixture<TestWrapperComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      schemas: [NO_ERRORS_SCHEMA],
      imports: [
        FormsModule,
      ],
      declarations: [
        TimepickerInnerComponent,
        TestWrapperComponent,
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestWrapperComponent);
    component = fixture.debugElement.children[0].componentInstance;
    testComponent = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should init position', () => {
    spyOn(component, 'scrollTo');
    component.initPosition();
    expect(component.scrollTo).toHaveBeenCalled();
  });

  it('should select hour', () => {
    spyOn(component, 'buildMinutes');
    component.selectHour(null, 1, true);
    expect(component.buildMinutes).not.toHaveBeenCalled();

    const selector = fixture.debugElement.queryAll(By.css('.fui-timepicker-select'))[0];
    component.selectHour(selector.nativeElement, 1, false);
    expect((new Date(component.value)).getHours()).toEqual(1);
  });

  it('should select hour', () => {
    component.selectHour(null, 1, true);
    expect((new Date(component.value)).getHours()).toEqual((new Date()).getHours());

    const selector = fixture.debugElement.queryAll(By.css('.fui-timepicker-select'))[0];
    component.selectHour(selector.nativeElement, 1, false);
    expect((new Date(component.value)).getHours()).toEqual(1);
  });

  it('should select minute', () => {
    component.selectMinute(null, 1, true);
    expect((new Date(component.value)).getMinutes()).toEqual((new Date()).getMinutes());

    const selector = fixture.debugElement.queryAll(By.css('.fui-timepicker-select'))[1];
    component.selectMinute(selector.nativeElement, 1, false);
    expect((new Date(component.value)).getMinutes()).toEqual(1);
  });
});
