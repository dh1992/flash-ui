import { Component, forwardRef, NO_ERRORS_SCHEMA } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ComponentFixture, TestBed, fakeAsync, tick, waitForAsync } from '@angular/core/testing';
import { OverlayModule } from '@angular/cdk/overlay';
import { FormsModule } from '@angular/forms';

import { TimepickerInnerComponent } from './timepicker-inner.component';
import { TimepickerComponent } from './timepicker.component';

@Component({
  template: '<fui-timepicker [(ngModel)]="value"></fui-timepicker>',
})
class TestWrapperComponent {
  value = new Date();
}

describe('TimepickerComponent', () => {
  let component: TimepickerComponent;
  let testComponent: TestWrapperComponent;
  let fixture: ComponentFixture<TestWrapperComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      schemas: [NO_ERRORS_SCHEMA],
      imports: [
        OverlayModule,
        FormsModule,
        BrowserAnimationsModule,
      ],
      declarations: [
        TimepickerComponent,
        TimepickerInnerComponent,
        TestWrapperComponent,
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestWrapperComponent);
    component = fixture.debugElement.children[0].componentInstance;
    testComponent = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should show and init timepicker', fakeAsync(() => {
    component.show();
    fixture.detectChanges();
    spyOn(component.timepicker, 'initPosition');
    tick(1000);
    expect(component.timepicker.initPosition).toHaveBeenCalled();
  }));

  it('should trigger onChange', fakeAsync(() => {
    spyOn(component, 'onChange');
    component.onTimeChange();
    expect(component.onChange).toHaveBeenCalled();
  }));
});
