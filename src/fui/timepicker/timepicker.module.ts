import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { OverlayModule } from '@angular/cdk/overlay';
import { I18nModule } from '../i18n/i18n.module';
import { IconModule } from '../icon/icon.module';

import { TimepickerComponent } from './timepicker.component';
import { TimepickerInnerComponent } from './timepicker-inner.component';

export * from './timepicker.component';
export * from './timepicker-inner.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    OverlayModule,
    I18nModule,
    IconModule,
  ],
  declarations: [
    TimepickerComponent,
    TimepickerInnerComponent,
  ],
  exports: [
    TimepickerComponent,
    TimepickerInnerComponent,
  ],
})
export class TimepickerModule { }
