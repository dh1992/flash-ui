import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OverlayModule } from '@angular/cdk/overlay';

import { CascadeComponent } from './cascade.component';
import { CascadeItemComponent } from './cascade-item.component';
import { CascadeTriggerDirective } from './cascade-trigger.directive';

export * from './cascade.component';
export * from './cascade-item.component';
export * from './cascade-trigger.directive';

@NgModule({
  imports: [
    CommonModule,
    OverlayModule,
  ],
  declarations: [
    CascadeComponent,
    CascadeItemComponent,
    CascadeTriggerDirective,
  ],
  exports: [
    CascadeComponent,
    CascadeItemComponent,
    CascadeTriggerDirective,
  ],
})
export class CascadeModule { }
