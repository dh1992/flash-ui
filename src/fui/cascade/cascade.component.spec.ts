import {
  Directive,
  Component,
  Output,
  Input,
  EventEmitter,
  NO_ERRORS_SCHEMA,
} from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { of as observableOf } from 'rxjs';

import { DEFAULT_CASCADE_POSITIONS } from '../shared/shared';
import { CascadeComponent, CascadeItemComponent } from './cascade.module';

@Directive({
  // eslint-disable-next-line
  selector: '[cdkConnectedOverlay]',
})
class CdkConnectedOverlayDirective {
  @Output() detach = new EventEmitter();
  @Output() positionChange = new EventEmitter();
  @Output() backdropClick = new EventEmitter();
  @Input() cdkConnectedOverlayOrigin: any;
  @Input() cdkConnectedOverlayHasBackdrop: any;
  @Input() cdkConnectedOverlayPositions: any;
  @Input() cdkConnectedOverlayOpen: any;
}

@Component({
  template: `<fui-cascade [visible]="visible | async" [direction]="direction"></fui-cascade>`,
})
class TestWrapperComponent {
  visible = observableOf(true);
  direction = 'topRight';
}

describe('CascadeComponent', () => {
  let component: CascadeComponent;
  let testComponent: TestWrapperComponent;
  let fixture: ComponentFixture<TestWrapperComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      schemas: [NO_ERRORS_SCHEMA],
      declarations: [
        CascadeComponent,
        CdkConnectedOverlayDirective,
        TestWrapperComponent,
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestWrapperComponent);
    testComponent = fixture.componentInstance;
    component = fixture.debugElement.children[0].componentInstance;
    fixture.detectChanges();
  });

  it('should add/remove item', () => {
    const cascadeItem = new CascadeItemComponent(component);
    expect(component.items.length).toEqual(1);

    cascadeItem.ngOnDestroy();
    expect(component.items.length).toEqual(0);
  });

  it('should set subCascade position', () => {
    component.setSubCascadePosition();
    expect(component.positions).toEqual(DEFAULT_CASCADE_POSITIONS);
    expect(component.direction).toEqual('right');
  });

  it('should handle cascade click', () => {
    spyOn(component, 'hide');

    component.clickCascade(false);
    expect(component.hide).toHaveBeenCalled();
  });
});
