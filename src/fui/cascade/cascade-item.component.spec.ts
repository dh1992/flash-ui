import {
  Component,
  Output,
  Input,
  EventEmitter,
  NO_ERRORS_SCHEMA,
} from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { Subject } from 'rxjs';

import { CascadeComponent, CascadeItemComponent } from './cascade.module';

@Component({
  selector: 'fui-cascade',
  template: '<ng-content></ng-content>',
  providers: [
    {
      provide: CascadeComponent,
      useClass: CascadeComponentStub,
    },
  ],
})
// eslint-disable-next-line
class CascadeComponentStub {
  visible = new Subject();
  closed = new Subject();
  setOverlayOrigin() {}
  addItem() {}
  removeItem() {}
}

@Component({
  template: `
    <fui-cascade>
      <fui-cascade-item>Ground</fui-cascade-item>
    </fui-cascade>
  `,
})
class TestWrapperComponent {}

describe('CascadeItemComponent', () => {
  let component: CascadeItemComponent;
  let testComponent: TestWrapperComponent;
  let fixture: ComponentFixture<TestWrapperComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      schemas: [NO_ERRORS_SCHEMA],
      declarations: [
        CascadeComponentStub,
        CascadeItemComponent,
        TestWrapperComponent,
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestWrapperComponent);
    testComponent = fixture.componentInstance;
    component = fixture.debugElement.query(By.directive(CascadeItemComponent)).componentInstance;
    fixture.detectChanges();
  });

  it('should handler hover', () => {
    spyOn(component.hovered, 'next');

    const cascadeItem: Element = fixture.debugElement.query(By.css('fui-cascade-item')).nativeElement;
    cascadeItem.dispatchEvent(new Event('mouseenter'));
    expect(component.hovered.next).toHaveBeenCalledWith(component);
  });
});
