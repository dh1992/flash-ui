import {
  Component,
  NO_ERRORS_SCHEMA,
} from '@angular/core';
import { ComponentFixture, TestBed, fakeAsync, tick, waitForAsync } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { of as observableOf, Subject } from 'rxjs';

import {
  CascadeTriggerDirective,
  CascadeComponent,
  CascadeItemComponent,
} from './cascade.module';

@Component({
  selector: 'fui-cascade',
  template: '<ng-content></ng-content>',
  providers: [
    {
      provide: CascadeComponent,
      useClass: CascadeComponentStub,
    },
  ],
})
// eslint-disable-next-line
class CascadeComponentStub {
  visible = new Subject();
  closed = new Subject();
  setOverlayOrigin() {}
  show() {}
  hide() {}
  hovered() {
    return observableOf();
  }
}

@Component({
  selector: 'fui-cascade-item',
  template: '<ng-content></ng-content>',
  providers: [
    {
      provide: CascadeItemComponent,
      useClass: CascadeItemComponentStub,
    },
  ],
})
// eslint-disable-next-line
class CascadeItemComponentStub {

}

@Component({
  template: `
    <button [fuiCascadeTrigger]="fuiCascade"></button>
    <fui-cascade #fuiCascade>
      <fui-cascade-item [fuiCascadeTrigger]="subCascade">SubCascade</fui-cascade-item>
    </fui-cascade>

    <fui-cascade #subCascade></fui-cascade>
  `,
})
class TestWrapperComponent {}

describe('CascadeTriggerDirective', () => {
  let component: CascadeComponent;
  let testComponent: TestWrapperComponent;
  let fixture: ComponentFixture<TestWrapperComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [
        CascadeComponentStub,
        CascadeItemComponentStub,
        CascadeTriggerDirective,
        TestWrapperComponent,
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestWrapperComponent);
    testComponent = fixture.componentInstance;
    component = fixture.debugElement.query(By.directive(CascadeComponent)).componentInstance;
    fixture.detectChanges();
  });

  it('should append open class', () => {
    const button: Element = fixture.debugElement.query(By.css('button')).nativeElement;
    expect(button.classList.contains('fui-cascade-open')).toBeFalsy();

    component.visible.next(true);
    fixture.detectChanges();
    expect(button.classList.contains('fui-cascade-open')).toBeTruthy();
  });

  it('should handle click', fakeAsync(() => {
    const button: Element = fixture.debugElement.query(By.css('button')).nativeElement;

    button.dispatchEvent(new Event('click'));
    tick();
    fixture.detectChanges();
    expect(button.classList.contains('fui-cascade-open')).toBeTruthy();

    button.dispatchEvent(new Event('click'));
    tick();
    fixture.detectChanges();
    expect(button.classList.contains('fui-cascade-open')).toBeFalsy();
  }));
});
