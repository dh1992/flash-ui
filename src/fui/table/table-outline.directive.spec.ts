import { Component } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { TableOutlineDirective } from './table-outline.directive';

@Component({
  template: '<table fuiTableOutline></table>',
})
class TableComponent {
}

describe ( 'TableOutlineDirective', () => {
  let component: TableComponent;
  let fixture: ComponentFixture<TableComponent>;
  let table: HTMLElement;

  beforeEach( waitForAsync( () => {
    TestBed.configureTestingModule({
      declarations: [ TableComponent, TableOutlineDirective ],
    })
    .compileComponents();
  }));

  beforeEach( () => {
    fixture = TestBed.createComponent(TableComponent);
    component = fixture.componentInstance;
    table = fixture.debugElement.query(By.css('table')).nativeElement;
    fixture.detectChanges();
  });

  it ('should add .fui-table-outline class', () => {
    expect(table.className).toBe('fui-table-outline');
  });

});
