import { TestBed, inject } from '@angular/core/testing';

import { TableSelectService } from './table-select.service';

describe('TableSelectService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TableSelectService],
    });
  });

  it('should register/unregister', inject([TableSelectService], (service: TableSelectService) => {
    const data1 = {name: 'test1'};
    const data2 = {name: 'test2'};

    service.register(data1);
    service.register(data2);
    service.muteSelect(data1, true);
    service.unregister(data1);
  }));

  it('should select/batchSelect', inject([TableSelectService], (service: TableSelectService) => {
    const data1 = {name: 'test1'};
    const data2 = {name: 'test2'};
    service.register(data1);
    service.register(data2);

    service.select(data1);
    expect(data1['_selected']).toEqual(true);
    service.select(data1, false);
    expect(data1['_selected']).toEqual(false);

    service.batchSelect(true);
    expect(data1['_selected']).toEqual(true);
    expect(data2['_selected']).toEqual(true);
    expect(service.isAllSelected()).toEqual(true);
  }));
});
