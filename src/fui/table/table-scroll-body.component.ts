import {
  Component,
  OnInit,
  HostBinding,
  Input,
  ElementRef,
  AfterContentInit,
  NgZone,
} from '@angular/core';
import { TableScrollService } from './table-scroll.service';
import { first } from 'rxjs/operators';

@Component({
  selector: 'fui-table-scroll-body',
  template: '<ng-content></ng-content>',
})
export class TableScrollBodyComponent implements OnInit {
  @HostBinding('class.fui-table-scroll-body') hostClass = true;

  @HostBinding('style.max-height.px') get hostHeight() {
    return this.maxHeight;
  }

  /** Scroll body max height in px. */
  @Input() maxHeight: number;

  constructor(
    private tableScroll: TableScrollService,
    private elementRef: ElementRef,
    private ngZone: NgZone,
  ) { }

  ngOnInit() {
    this.ngZone.onStable.asObservable().pipe(first()).subscribe(() => {
      const element: HTMLElement = this.elementRef.nativeElement;
      const scrollbarWidth = element.offsetWidth - element.clientWidth;
      this.tableScroll.enableScroll(scrollbarWidth);
    });
  }

}
