import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { I18nModule } from '../i18n/i18n.module';
import { FormModule } from '../form/form.module';
import { IconModule } from '../icon/icon.module';

import { TableHeadComponent } from './table-head.component';
import { TableRowComponent } from './table-row.component';
import { TableComponent } from './table.component';
import { TableFlexService } from './table-flex.service';
import { TableScrollBodyComponent } from './table-scroll-body.component' ;
import { TableHeadCellComponent } from './table-head-cell.component';
import { TableOutlineDirective } from './table-outline.directive';

export * from './table-head.component';
export * from './table-row.component';
export * from './table.component';
export * from './table-scroll-body.component';
export * from './table-head-cell.component';
export * from './table-outline.directive';
export * from './table-sort.service';

@NgModule({
  imports: [
    CommonModule,
    I18nModule,
    FormsModule,
    FormModule,
    IconModule,
  ],
  declarations: [
    TableComponent,
    TableHeadComponent,
    TableRowComponent,
    TableScrollBodyComponent,
    TableHeadCellComponent,
    TableOutlineDirective,
  ],
  providers: [
    TableFlexService,
  ],
  exports: [
    TableComponent,
    TableHeadComponent,
    TableRowComponent,
    TableScrollBodyComponent,
    TableHeadCellComponent,
    TableOutlineDirective,
  ],
})
export class TableModule { }
