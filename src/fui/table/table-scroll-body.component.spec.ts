import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { TableScrollBodyComponent } from './table-scroll-body.component';
import { TableScrollService } from './table-scroll.service';

describe('TableScrollBodyComponent', () => {
  let component: TableScrollBodyComponent;
  let fixture: ComponentFixture<TableScrollBodyComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ TableScrollBodyComponent ],
      providers: [
        {
          provide: TableScrollService,
          useValue: {
            enableScroll() {},
          },
        },
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableScrollBodyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
