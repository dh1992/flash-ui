import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';

import { CheckboxComponent } from '../../mock';
import { TableHeadComponent } from './table-head.component';
import { TableComponent } from './table.component';
import { TableFlexService } from './table-flex.service';
import { TableScrollService } from './table-scroll.service';
import { of as observableOf, Subject } from 'rxjs';
import { TableSelectService } from './table-select.service';
import { TableSortService } from './table-sort.service';

class TableScrollServiceStub {
  scrollSubject = new Subject();
}

describe('TableHeadComponent', () => {
  let component: TableHeadComponent;
  let fixture: ComponentFixture<TableHeadComponent>;
  let tableScrollService: TableScrollService;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      schemas: [ NO_ERRORS_SCHEMA ],
      imports: [ FormsModule ],
      declarations: [
        TableHeadComponent,
        CheckboxComponent,
      ],
      providers: [
        {
          provide: TableComponent,
          useValue: {},
        },
        {
          provide: TableFlexService,
          useValue: {
            register() {},
            unregister() {},
          },
        },
        {
          provide: TableScrollService,
          useClass: TableScrollServiceStub,
        },
        {
          provide: TableSelectService,
          useValue: {
            batchSelect() {},
            register() {},
            unregister() {},
          },
        },
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableHeadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    tableScrollService = TestBed.inject(TableScrollService);
  });

  it('should be created', () => {
    const element = fixture.nativeElement;
    component.tableScroll.scrollSubject.next({
      enabled: true,
      scrollbarWidth: 15,
    });
    fixture.detectChanges();
    expect(element.style['padding-right']).toEqual('15px');

    spyOn(component.sortChange, 'emit');
    component.tableSort.sortChangeSubject.next({
      sortedBy: 'test',
      order: 'asc',
    });
    expect(component.sortChange.emit).toHaveBeenCalledWith({
      sortedBy: 'test',
      order: 'asc',
    });
  });

  it('should update select', () => {
    spyOn(component.tableSelect, 'batchSelect');

    component.updateData(true);
    expect(component.tableSelect.batchSelect).toHaveBeenCalledWith(true);
  });
});
