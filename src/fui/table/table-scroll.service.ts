import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

export class TableScrollData {
  enabled: boolean;
  scrollbarWidth: number;
}

@Injectable()
export class TableScrollService {
  /** Subject for TableScrollData. */
  scrollSubject = new Subject<TableScrollData>();

  private scrollbarWidth: number;

  constructor() { }

  /** Enable scroll. */
  enableScroll(scrollbarWidth: number) {
    this.scrollbarWidth = scrollbarWidth;
    this.scrollSubject.next({
      enabled: true,
      scrollbarWidth,
    });
  }

}
