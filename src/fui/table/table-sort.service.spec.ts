import { TestBed, inject } from '@angular/core/testing';

import { TableSortService, SortItem } from './table-sort.service';

describe('TableSortService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TableSortService],
    });
  });

  it('should set mode', inject([TableSortService], (service: TableSortService) => {
    service.setMode('multiple');
    expect(service.sortMode).toEqual('multiple');
  }));

  it('should register/unregister', inject([TableSortService], (service: TableSortService) => {
    const sortItem = new SortItem('test', 'asc');
    service.registerSortItem(sortItem);
    expect(service.sortItems.length).toEqual(1);

    service.unregisterSortItem('test');
    expect(service.sortItems.length).toEqual(0);
  }));

  it('on sort change', inject([TableSortService], (service: TableSortService) => {
    spyOn(service.sortChangeSubject, 'next');
    const sortItem1 = new SortItem('test1', null);
    const sortItem2 = new SortItem('test2', null);
    service.sortItems = [sortItem1, sortItem2];

    sortItem1.nextOrder();
    service.sortChange('test1');
    expect(service.sortChangeSubject.next).toHaveBeenCalledWith({
      sortedBy: 'test1',
      order: 'desc',
    });

    sortItem1.nextOrder();
    service.sortChange('test1');
    expect(service.sortChangeSubject.next).toHaveBeenCalledWith({
      sortedBy: 'test1',
      order: 'asc',
    });

    sortItem1.nextOrder();
    service.sortChange('test1');
    expect(service.sortChangeSubject.next).toHaveBeenCalledWith({
      sortedBy: '',
      order: '',
    });
  }));

});
