import { TestBed, inject } from '@angular/core/testing';

import { TableScrollService } from './table-scroll.service';

describe('TableScrollService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TableScrollService],
    });
  });

  it('should be created', inject([TableScrollService], (service: TableScrollService) => {
    expect(service).toBeTruthy();
  }));
});
