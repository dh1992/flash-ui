import { Input, Directive } from '@angular/core';
import { Observable, Subject, BehaviorSubject } from 'rxjs';

@Directive()
export class TableSelectBase {

  /** Enable select functionality. */
  @Input() set fuiTableSelect(value: boolean) {
    this._fuiTableSelect = value !== false;
  }

  get fuiTableSelect() {
    return this._fuiTableSelect;
  }

  /** Hide select checkbox for certain rows. */
  @Input() set fuiTableSelectHide(value: boolean) {
    const hide = value === true;
    this.hideSelectSuject.next(hide);
  }

  get fuiTableSelectHide() {
    return this.hideSelectSuject.getValue();
  }

  private _fuiTableSelect;

  /**
   * 初始化假如select列出现，不隐藏勾选框。
   */
  private hideSelectSuject = new BehaviorSubject<boolean>(false);

  protected hideSelectObservable: Observable<boolean> = this.hideSelectSuject.asObservable();

  constructor() {}
}
