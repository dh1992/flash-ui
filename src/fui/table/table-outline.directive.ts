import { Directive, HostBinding, Input } from '@angular/core';

@Directive({
  selector: '[fuiTableOutline]',
})
export class TableOutlineDirective {
  @HostBinding('class.fui-table-outline') hostClass = true;

  /** Add outline style to the table. */
  @Input() set fuiTableOutline( outline: boolean ) {
    if (typeof outline === 'boolean') {
      this.hostClass = !!outline;
    }
  }

  constructor() {}
}

