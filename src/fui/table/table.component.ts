import {
  Component,
  HostBinding,
  Input,
  Output,
  EventEmitter,
  OnDestroy,
} from '@angular/core';
import { Subscription } from 'rxjs';

import { TableFlexService } from './table-flex.service';
import { TableScrollService } from './table-scroll.service';
import { TableSelectService } from './table-select.service';

@Component({
  selector: 'fui-table',
  templateUrl: './table.component.html',
  providers: [
    TableFlexService,
    TableScrollService,
    TableSelectService,
  ],
})
export class TableComponent implements OnDestroy {
  @HostBinding('class.fui-table') hostClass = true;

  /** 标识当前表格是否处于loading状态 */
  @HostBinding('class.loading') get loadingClass() {
    return this.loading;
  }

  /** 标识当前表格是否处于无数据状态 */
  @HostBinding('class.empty') get emptyClass() {
    return this.empty;
  }

  /** Table flex style. Pass in valid flex values. */
  @Input() set fuiTableFlex(flex: (number | string)[]) {
    this.tableFlex.updateFlex(flex);
  }

  /** Show loading state. */
  @Input() loading = false;

  /** Emit select changes. */
  @Output() selectChange = new EventEmitter();

  get empty() {
    return this.tableFlex.empty;
  }

  selectChangeSub: Subscription;

  constructor(
    private tableFlex: TableFlexService,
    private tableSelect: TableSelectService,
  ) {
    this.selectChangeSub = this.tableSelect.selectChangeObservable.subscribe((tableData) => {
      this.selectChange.emit(tableData);
    });
  }

  ngOnDestroy() {
    this.selectChangeSub.unsubscribe();
  }
}
