import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IconModule } from '../icon/icon.module';
import { ProgressComponent } from './progress.component';

export * from './progress.component';

@NgModule({
  imports: [
    CommonModule,
    IconModule,
  ],
  declarations: [
    ProgressComponent,
  ],
  exports: [
    ProgressComponent,
  ],
})
export class ProgressModule {}
