import { NO_ERRORS_SCHEMA, DebugElement } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { By } from '@angular/platform-browser';

import { ProgressComponent } from './progress.component';

describe('ProgressComponent', () => {
  let component: ProgressComponent;
  let fixture: ComponentFixture<ProgressComponent>;

  beforeEach( waitForAsync( () => {
    TestBed.configureTestingModule({
      schemas: [NO_ERRORS_SCHEMA],
      declarations: [
        ProgressComponent,
      ],
      providers: [],
    })
    .compileComponents();
  }));

  beforeEach( () => {
    fixture = TestBed.createComponent( ProgressComponent );
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('should change primary bar width accroding with percent', () => {
    const pBarElement = fixture.debugElement;
    const primaryBarDe: DebugElement = pBarElement.query(By.css('.fui-progress-bar-primary'));
    const primaryBar: HTMLElement = primaryBarDe.nativeElement;

    // default
    expect(primaryBar.style.width).toEqual('0%');

    // The primaryBar width changes as percent changes
    component.percent = 30;
    fixture.detectChanges();
    expect(primaryBar.style.width).toEqual('30%');

    component.percent = 1100;
    fixture.detectChanges();
    expect(primaryBar.style.width).toEqual('100%');

    component.percent = -900;
    fixture.detectChanges();
    expect(primaryBar.style.width).toEqual('0%');
  });

  it('should show different info when status changes', () => {
    const pBarElementDe = fixture.debugElement;
    const primaryBarDe: DebugElement = pBarElementDe.query(By.css('.fui-progress-bar-primary'));

    const infoDe: DebugElement = pBarElementDe.query(By.css('.fui-progress-status'));
    const info: HTMLElement = infoDe.nativeElement;

    // when 'active'
    const textElement = info.querySelector('p');
    expect(textElement.textContent).toContain('%');

    // when 'exception'
    component.status = 'exception';
    fixture.detectChanges();

    const exceptElement = info.querySelector('svg');
    expect(exceptElement.getAttribute('fuiIcon')).toEqual('cross-circle');

    // when 'success'
    component.status = 'success';
    fixture.detectChanges();

    const successElement = info.querySelector('svg');
    expect(successElement.getAttribute('fuiIcon')).toEqual('check-circle');
  });

});


