import { Component, OnInit, Input, HostBinding } from '@angular/core';

@Component({
  selector: 'fui-progress',
  templateUrl: './progress.component.html',
})
export class ProgressComponent implements OnInit {
  @HostBinding('class.fui-progress') host = true;

  /** Progress percent from 0 to 100. */
  @Input()
  set percent(value: number) {
    // clamp percent to fall between lower and upper
    this._percent = Math.max(this.lower, Math.min(value, this.upper)) || 0;
  }
  get percent() {
    return this._percent;
  }

  /** Progress bar status, support 'active', 'exception' and 'success'. */
  @Input() status: 'active' | 'exception' | 'success' = 'active';

  private lower = 0;
  private upper = 100;

  private _percent = 0;

  ngOnInit() {}

  barTransform() {
    return {
      width: `${this._percent}%`,
    };
  }

}
