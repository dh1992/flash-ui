import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { OverlayModule } from '@angular/cdk/overlay';
import { I18nModule } from '../i18n/i18n.module';
import { IconModule } from '../icon/icon.module';
import { FormModule } from '../form/form.module';

import { SelectComponent } from './select.component';
import { SelectOptionComponent } from './select-option.component';
import { SelectOptionAllComponent } from './select-option-all.component';
import { SelectOptionFilterPipe } from './select-option-filter.pipe';
import { SelectOptionItemComponent } from './select-option-item.component';

export * from './select.component';
export * from './select-option.component';
export * from './select-option-all.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    OverlayModule,
    I18nModule,
    IconModule,
    FormModule,
  ],
  declarations: [
    SelectComponent,
    SelectOptionComponent,
    SelectOptionAllComponent,
    SelectOptionItemComponent,
    SelectOptionFilterPipe,
  ],
  exports: [
    SelectComponent,
    SelectOptionComponent,
    SelectOptionAllComponent,
    SelectOptionItemComponent,
  ],
})
export class SelectModule { }
