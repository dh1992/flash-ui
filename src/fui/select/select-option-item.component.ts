import {
  Component,
  HostListener,
  HostBinding,
  Input,
  OnInit,
} from '@angular/core';

import { SelectStoreService } from './select-store.service';

import { SelectOptionComponent } from './select-option.component';

@Component({
  selector: 'fui-select-option-item',
  templateUrl: './select-option-item.component.html',
})
export class SelectOptionItemComponent implements OnInit {
  @HostBinding('class.fui-select-option') hostClass = true;

  @HostBinding('class.active') get activeClass() {
    return this.selectOption && this.selectOption.option.selected;
  }

  @HostBinding('attr.disabled') get attrDisabled() {
    return this.selectOption && this.selectOption.disabled;
  }

  /** Option component. */
  @Input() selectOption: SelectOptionComponent;

  @HostListener('click', ['$event']) clickHandler(event: MouseEvent) {
    event.preventDefault();
    if (!this.selectOption.disabled) {
      this.store.toggleSelect(this.selectOption.option);
    }
  }

  constructor(
    private store: SelectStoreService,
  ) { }

  ngOnInit() {
  }
}
