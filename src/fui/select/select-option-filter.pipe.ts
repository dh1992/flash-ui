import { Pipe, PipeTransform } from '@angular/core';

import { SelectOptionComponent } from './select-option.component';

@Pipe({
  name: 'fuiSelectOptionFilterPipe',
})
export class SelectOptionFilterPipe implements PipeTransform {

  transform(options: SelectOptionComponent[], filter: string, key: string): SelectOptionComponent[] {
    if (!filter) {
      return options;
    } else {
      const optionsArray = Array.from(options);
      if (key) {
        return optionsArray.filter((option) => option[key] && option[key].indexOf(filter) > -1);
      } else {
        return optionsArray.filter((option) => option.value && option.value.indexOf(filter) > -1 ||
          option.label && option.label.indexOf(filter) > -1);
      }
    }
  }
}
