import { Component, NO_ERRORS_SCHEMA } from '@angular/core';
import { TestBed, waitForAsync } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { OverlayModule } from '@angular/cdk/overlay';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';

import { TranslatePipeStub, CheckboxComponent, SelectOptionFilterPipeStub } from '../../mock';
import { SelectOptionAllComponent } from './select-option-all.component';
import { SelectComponent } from './select.component';

@Component({
  template: `
  <fui-select mode="single">
    <fui-select-option-all></fui-select-option-all>
  </fui-select>
  `,
})
class IllegalComponent {
}

@Component({
  template: `
  <fui-select mode="multiple">
    <fui-select-option-all></fui-select-option-all>
  </fui-select>
  `,
})
class LegalComponent {
}

describe('SelectOptionAllComponent', () => {
  let legalComponent: LegalComponent;
  let illegalComponent: IllegalComponent;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      schemas: [ NO_ERRORS_SCHEMA ],
      imports: [
        FormsModule,
        OverlayModule,
        NoopAnimationsModule,
      ],
      declarations: [
        TranslatePipeStub,
        CheckboxComponent,
        SelectOptionFilterPipeStub,
        SelectOptionAllComponent,
        SelectComponent,
        IllegalComponent,
        LegalComponent,
      ],
    })
    .compileComponents();
  }));

  it('should throw when place select-all option in single select', () => {
    const illegalFixture = TestBed.createComponent(IllegalComponent);
    illegalComponent = illegalFixture.componentInstance;
    expect(() => illegalFixture.detectChanges()).toThrow();
  });

  it('should create in multiple select', () => {
    const legalFixture = TestBed.createComponent(LegalComponent);
    legalComponent = legalFixture.componentInstance;
    legalFixture.detectChanges();
    expect(legalComponent).toBeTruthy();
  });

});
