import { Component, ElementRef, NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, inject, fakeAsync, flush, waitForAsync } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { OverlayContainer, OverlayModule } from '@angular/cdk/overlay';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';

import { TranslatePipeStub, CheckboxComponent, SelectOptionFilterPipeStub } from '../../mock';
import { SelectOptionComponent } from './select-option.component';
import { SelectComponent } from './select.component';

@Component({
  template: `
  <fui-select>
    <fui-select-option value="1" label="1">1</fui-select-option>
    <fui-select-option value="2">2</fui-select-option>
  </fui-select>
  `,
})
class TestComponent {
}

describe('SelectOptionComponent', () => {
  let component: SelectComponent;
  let fixture: ComponentFixture<TestComponent>;
  let overlayContainer: OverlayContainer;
  let overlayContainerElement: HTMLElement;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      schemas: [ NO_ERRORS_SCHEMA ],
      imports: [
        FormsModule,
        OverlayModule,
        NoopAnimationsModule,
      ],
      declarations: [
        TranslatePipeStub,
        CheckboxComponent,
        SelectOptionFilterPipeStub,
        SelectOptionComponent,
        SelectComponent,
        TestComponent,
      ],
    })
    .compileComponents();

    inject([OverlayContainer], (oc: OverlayContainer) => {
      overlayContainer = oc;
      overlayContainerElement = oc.getContainerElement();
    })();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestComponent);
    component = fixture.debugElement.query(By.directive(SelectComponent)).componentInstance;
    component.show();
    fixture.detectChanges();
  });

  afterEach(() => {
    overlayContainer.ngOnDestroy();
  });

  it('should be attached to overlay container', () => {
    const options = overlayContainerElement.querySelectorAll('.fui-select-option');
    expect(options.length).toBeGreaterThan(0);
  });

  it('should set label by inner content on select', fakeAsync(() => {
    const options = overlayContainerElement.querySelectorAll('.fui-select-option');
    expect(component.store.selectedLabel).toBeFalsy();
    options[1].dispatchEvent(new Event('click'));
    fixture.detectChanges();
    flush();
    expect(component.store.selectedLabel).toEqual('2');
  }));
});
