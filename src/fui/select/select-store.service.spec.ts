import { TestBed, inject, waitForAsync } from '@angular/core/testing';

import { SelectStoreService, SelectOption } from './select-store.service';
import { buffer, bufferCount } from 'rxjs/operators';

describe('SelectStoreService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SelectStoreService],
    });
  });

  it('should be created', inject([SelectStoreService], (service: SelectStoreService) => {
    expect(service).toBeTruthy();
  }));

  describe('manage options', () => {
    it('should add/remove option', inject([SelectStoreService], (service: SelectStoreService) => {
      const option1 = new SelectOption(1, 1);
      const option2 = new SelectOption(2, 2);
      service.addOption(option1);
      service.addOption(option2);
      expect(service.options).toEqual([option1, option2]);

      service.removeOption(option1);
      expect(service.options).toEqual([option2]);
    }));

    it('should update option', inject([SelectStoreService], (service: SelectStoreService) => {
      const option = new SelectOption(1, 1);
      service.addOption(option);
      service.updateOption(option, {value: 2});

      expect(service.options).toEqual([new SelectOption(2, 1)]);
    }));


    it('', inject([SelectStoreService], (service: SelectStoreService) => {
    }));
  });

  describe('select', () => {
    let service: SelectStoreService;
    let option1: SelectOption;
    let option2: SelectOption;
    beforeEach(inject([SelectStoreService], (svc: SelectStoreService) => {
      service = svc;
      option1 = new SelectOption(1, 1);
      option2 = new SelectOption(2, 2);
      service.addOption(option1);
      service.addOption(option2);
    }));

    describe('in single mode', () => {
      it('toggle select', () => {
        const res1 = new SelectOption(1, 1, true);
        const res2 = new SelectOption(2, 2, true);
        service.toggleSelect(option2);
        expect(service.options).toEqual([option1, res2]);
        service.toggleSelect(option1);
        expect(service.options).toEqual([res1, option2]);
      });

      it('should clear', () => {
        service.toggleSelect(option1);
        service.clear();
        expect(service.options).toEqual([option1, option2]);
      });

      it('should emit value change when select change', waitForAsync(() => {
        let values;
        service.selectValuesChange.pipe(bufferCount(3)).subscribe((vs) => {
          values = vs;
        });

        service.toggleSelect(option1);
        service.toggleSelect(option2);
        service.clear();

        expect(values).toEqual([1, 2, undefined]);
      }));
    });

    describe('in multiple mode', () => {
      beforeEach(() => {
        service.mode = 'multiple';
      });

      it('select many', () => {
        const res1 = new SelectOption(1, 1, true);
        const res2 = new SelectOption(2, 2, true);
        service.toggleSelect(option2);
        expect(service.options).toEqual([option1, res2]);
        service.toggleSelect(option1);
        expect(service.options).toEqual([res1, res2]);
      });

      it('select all', () => {
        const res1 = new SelectOption(1, 1, true);
        const res2 = new SelectOption(2, 2, true);
        service.selectAll(true);
        expect(service.options).toEqual([res1, res2]);
        service.selectAll(false);
        expect(service.options).toEqual([option1, option2]);
      });

      it('should emit value change when select change', waitForAsync(() => {
        let values;
        service.selectValuesChange.pipe(bufferCount(5)).subscribe((vs) => {
          values = vs;
        });

        service.toggleSelect(option1);
        service.toggleSelect(option2);
        service.clear();
        service.selectAll(true);
        service.selectAll(false);

        expect(values).toEqual([
          [1],
          [1, 2],
          [],
          [1, 2],
          [],
        ]);
      }));
    });
  });
});
