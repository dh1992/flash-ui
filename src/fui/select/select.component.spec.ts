import { Component, NO_ERRORS_SCHEMA } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ComponentFixture, TestBed, inject, fakeAsync, tick, flush, waitForAsync } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { OverlayModule, OverlayContainer } from '@angular/cdk/overlay';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';

import { TranslatePipeStub, CheckboxComponent, SelectOptionFilterPipeStub } from '../../mock';
import { SelectComponent } from './select.component';
import { SelectOptionComponent } from './select-option.component';
import { SelectOption } from './select-store.service';
import { SelectOptionAllComponent } from './select-option-all.component';

@Component({
  template: '<fui-select [placeholder]="placeholder"></fui-select>',
})
class SelectWithPlaceholderComponent {
  placeholder: string;
}

@Component({
  template: `
    <fui-select [(ngModel)]="model" [mode]="mode">
      <fui-select-option *ngFor="let option of options" [value]="option" [label]="option">{{option}}</fui-select-option>
    </fui-select>
  `,
})
class SelectWithOptionsComponent {
  options = [];
  model: any;
  mode: string;
}

describe('SelectComponent', () => {
  let fixture: ComponentFixture<any>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      schemas: [ NO_ERRORS_SCHEMA ],
      imports: [
        FormsModule,
        OverlayModule,
        NoopAnimationsModule,
      ],
      declarations: [
        CheckboxComponent,
        TranslatePipeStub,
        SelectOptionFilterPipeStub,
        SelectComponent,
        SelectOptionComponent,
        SelectOptionAllComponent,
        SelectWithPlaceholderComponent,
        SelectWithOptionsComponent,
      ],
    })
    .compileComponents();
  }));

  describe('basic behaviors', () => {
    let component: SelectComponent;
    let element: HTMLElement;

    beforeEach(() => {
      fixture = TestBed.createComponent(SelectComponent);
      component = fixture.componentInstance;
      element = fixture.debugElement.nativeElement;
      fixture.detectChanges();
    });

    it('should add fui-select class to host element', () => {
      expect(element.classList).toContain('fui-select');
    });

    it('should add tabindex = 0 to attributes of host element', () => {
      expect(element.getAttribute('tabindex')).toBe('0');
    });
  });

  describe('head label&placeholder', () => {
    let testComponent: SelectWithPlaceholderComponent;
    let component: SelectComponent;
    let element: HTMLElement;

    beforeEach(() => {
      fixture = TestBed.createComponent(SelectWithPlaceholderComponent);
      testComponent = fixture.componentInstance;
      component = fixture.debugElement.query(By.directive(SelectComponent)).componentInstance;
      element = fixture.debugElement.nativeElement;
      fixture.detectChanges();
    });

    it('should show placeholder when label is empty', () => {
      component.placeholder = 'placeholder';
      fixture.detectChanges();
      const placeholder = element.querySelector('.fui-select-placeholder');
      const label = element.querySelector('.fui-select-label');
      expect(placeholder).toBeTruthy();
      expect(placeholder.textContent.trim()).toBe('placeholder');
      expect(label).toBeNull();
    });

    it('should remove placeholder when label is defined', fakeAsync(() => {
      const option = new SelectOption('world', 'hello');
      component.store.addOption(option);
      component.store.toggleSelect(option);
      tick(150);
      fixture.detectChanges();
      const placeholder = element.querySelector('.fui-select-placeholder');
      const label = element.querySelector('.fui-select-label');
      expect(placeholder).toBeNull();
      expect(label).toBeTruthy();
      expect(label.textContent.trim()).toBe('hello');
    }));
  });

  describe('with options', () => {
    let testComponent: SelectWithOptionsComponent;
    let component: SelectComponent;
    let element: HTMLElement;

    beforeEach(() => {
      fixture = TestBed.createComponent(SelectWithOptionsComponent);
      testComponent = fixture.componentInstance;
      component = fixture.debugElement.query(By.directive(SelectComponent)).componentInstance;
      component.show();
      element = fixture.debugElement.nativeElement;
      fixture.detectChanges();
    });

    it('should not display no options message when there are options', () => {
      testComponent.options = ['hello'];
      fixture.detectChanges();
      const noOptionsElement = document.querySelector('.fui-select-no-options');
      waitForAsync(() => {
        expect(noOptionsElement).toBeNull();
      });
    });

    it('should display no options message when options are empty', () => {
      fixture.detectChanges();
      const noOptionsElement = document.querySelector('.fui-select-no-options');
      waitForAsync(() => {
        expect(noOptionsElement).not.toBeNull();
      });
    });

    it('should set options', () => {
      testComponent.options = [1, 2];
      fixture.detectChanges();
      expect(component.store.options.length).toEqual(2);
    });

    describe('with clear operation', () => {
      beforeEach(() => {
        testComponent.options = [1, 2, 3];
        fixture.detectChanges();
      });

      it('should clear', fakeAsync(() => {
        testComponent.model = 1;
        fixture.detectChanges();
        tick();
        component.clear(new MouseEvent('click'));
        tick(150);
        expect(component.store.selectedOptions.length).toBe(0);
        expect(testComponent.model).toBeFalsy();
      }));
    });
  });
});
