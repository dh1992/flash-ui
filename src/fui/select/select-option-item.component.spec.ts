import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { SelectOptionItemComponent } from './select-option-item.component';
import { SelectOptionComponent } from './select-option.component';

import { SelectStoreService, SelectOption } from './select-store.service';

class SelectStoreServiceStub {
  toggleSelect() {
  }
}

describe('SelectOptionItemComponent', () => {
  let component: SelectOptionItemComponent;
  let fixture: ComponentFixture<SelectOptionItemComponent>;
  let store: SelectStoreService;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      schemas: [ NO_ERRORS_SCHEMA ],
      imports: [
        FormsModule,
      ],
      providers: [
        {
          provide: SelectStoreService,
          useClass: SelectStoreServiceStub,
        },
      ],
      declarations: [
        SelectOptionItemComponent,
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    store = TestBed.inject(SelectStoreService);

    fixture = TestBed.createComponent(SelectOptionItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should click', () => {
    spyOn(store, 'toggleSelect');

    const selectOption = new SelectOption('test', 'test');
    component.selectOption = {option: selectOption} as SelectOptionComponent;
    component.clickHandler(new MouseEvent('click'));
    expect(store.toggleSelect).toHaveBeenCalledWith(selectOption);
  });
});
