import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BtnDirective } from './btn.directive';
import { BtnOutlineDirective } from './btn-outline.directive';

export * from './btn.directive';
export * from './btn-outline.directive';

@NgModule({
  imports: [
    CommonModule,
  ],
  declarations: [
    BtnDirective,
    BtnOutlineDirective,
  ],
  exports: [
    BtnDirective,
    BtnOutlineDirective,
  ],
})
export class BtnModule { }
