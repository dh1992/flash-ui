export const makeIcon = (iconName: string) => {
  const icon = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
  const iconUse: SVGUseElement = document.createElementNS('http://www.w3.org/2000/svg', 'use');
  iconUse.setAttributeNS('http://www.w3.org/1999/xlink', 'xlink:href', '#' + iconName );
  icon.classList.add('fui-icon');
  icon.appendChild(iconUse);
  return icon;
};
