import { Directive, HostBinding, Input } from '@angular/core';

@Directive({
  selector: '[fuiBtnOutline]',
})
export class BtnOutlineDirective {
  @HostBinding('class.fui-btn-outline') hostClass = true;

  /** Add outline to button if set true. */
  @Input() set fuiBtnOutline(outline: boolean) {
    if (typeof outline === 'boolean') {
      this.hostClass = !!outline;
    }
  }

  constructor() { }

}
