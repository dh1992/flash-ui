import { BtnDirective } from './btn.directive';
import {Input, Component, SimpleChanges, SimpleChange, Renderer2} from '@angular/core';
import { ComponentFixture, fakeAsync, TestBed, tick, waitForAsync } from '@angular/core/testing';
import { By } from '@angular/platform-browser';

@Component({
  template: '<button [fuiBtn]="color">btn</button>',
})
class ButtonComponent {
  color: string;
  loading: boolean;
}

@Component({
  template: '<button [fuiBtn]="color" size="sm">btn</button>',
})
class SmButtonComponent {
  color: string;
  loading: boolean;
}

@Component({
  template: '<button [fuiBtn]="color" [fuiBtnLoading]="loading" fuiBtnIcon="user">btn</button>',
})
class LoadingButtonComponent {
  color: string;
  loading: boolean;
}

@Component(({
  template: '<button [fuiBtn]="color" fuiBtnIcon="user"></button>',
}))
class IconButtonComponent {
  color: string;
  loading: boolean;
}


describe('BtnDirective', () => {
  let component: ButtonComponent;
  let fixture: ComponentFixture<ButtonComponent>;
  let button: HTMLElement;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [
        ButtonComponent,
        BtnDirective,
        SmButtonComponent,
        LoadingButtonComponent,
        IconButtonComponent,
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ButtonComponent);
    component = fixture.componentInstance;
    button = fixture.debugElement.query(By.css('button')).nativeElement;
    fixture.detectChanges();
  });

  it('should render button', () => {
    expect(button).toBeDefined();
  });

  it('should have .fui-btn class', () => {
    expect(button.classList.contains('fui-btn')).toBeTruthy();
  });

  it('should set color class', () => {
    component.color = 'danger';
    fixture.detectChanges();
    expect(button.classList.contains('fui-btn-danger')).toBeTruthy();
  });

  it('should set btn md size on creation', () => {
    expect(button.classList.contains('fui-btn-md')).toBeTruthy();
  });

  it('should override size class when size input is valid', () => {
    fixture = TestBed.createComponent(SmButtonComponent);
    component = fixture.componentInstance;
    button = fixture.debugElement.query(By.css('button')).nativeElement;
    fixture.detectChanges();
    expect(button.classList.contains('fui-btn-sm')).toBeTruthy();
    expect(button.classList.contains('fui-btn-md')).toBeFalsy();
  });

  it('should have loading icon when fuiBtnLoading is true', () => {
    fixture = TestBed.createComponent(LoadingButtonComponent);
    component = fixture.componentInstance;
    component.loading = true;
    button = fixture.debugElement.query(By.css('button')).nativeElement;
    fixture.detectChanges();
    expect(button.classList.contains('fui-btn-loading')).toBeTruthy();

    component.loading = false;
    fixture.detectChanges();
    expect(button.classList.contains('fui-btn-loading')).toBeFalsy();
  });

  it('should have icon when fuiBtnIcon has value', () => {
    fixture = TestBed.createComponent(IconButtonComponent);
    component = fixture.componentInstance;
    button = fixture.debugElement.query(By.css('button')).nativeElement;
    fixture.detectChanges();
    expect(button.querySelector('.fui-icon')).toBeTruthy();
  });

  it('should have RippleCircle when click', fakeAsync(() => {
    fixture = TestBed.createComponent(IconButtonComponent);
    component = fixture.componentInstance;
    button = fixture.debugElement.query(By.css('button')).nativeElement;
    fixture.detectChanges();
    button.click();
    expect(button.querySelector('.fui-btn-ripple-circle')).toBeTruthy();
    tick(500);
    expect(button.querySelector('.fui-btn-ripple-circle')).toBeFalsy();
  }));
});
