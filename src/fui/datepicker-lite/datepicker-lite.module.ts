import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { OverlayModule } from '@angular/cdk/overlay';
import { PortalModule } from '@angular/cdk/portal';
import { I18nModule } from '../i18n/i18n.module';
import { IconModule } from '../icon/icon.module';
import { CalendarLiteModule } from '../calendar-lite/calendar-lite.module';
import { TimepickerModule } from '../timepicker/timepicker.module';

import { DatepickerLiteComponent } from './datepicker-lite.component';
import { DatepickerLiteContentDirective } from './datepicker-lite-content.directive';

export * from './datepicker-lite.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    OverlayModule,
    PortalModule,
    I18nModule,
    IconModule,
    CalendarLiteModule,
    TimepickerModule,
  ],
  declarations: [
    DatepickerLiteComponent,
    DatepickerLiteContentDirective,
  ],
  exports: [
    DatepickerLiteComponent,
    DatepickerLiteContentDirective,
  ],
})
export class DatepickerLiteModule { }
