import { Component, NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, fakeAsync, tick, waitForAsync } from '@angular/core/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { OverlayModule } from '@angular/cdk/overlay';
import { PortalModule } from '@angular/cdk/portal';
import { FormsModule } from '@angular/forms';

import { TranslatePipeStub } from '../../mock';

import { TimepickerInnerComponent } from '../timepicker/timepicker.module';
import { DatepickerLiteComponent } from './datepicker-lite.component';
import { CalendarDay, CalendarMonth } from '../calendar-lite/calendar-lite.module';

@Component({
  template: `
    <fui-datepicker-lite
      [(ngModel)]="date"
      [showTime]="true"
      format="yyyy-MM-dd"
    >
    </fui-datepicker-lite>
  `,
})
class TestWrapperComponent {
  date = new Date('2018-01-10');
}

describe('DatepickerLiteComponent', () => {
  let component: DatepickerLiteComponent;
  let testComponent: TestWrapperComponent;
  let fixture: ComponentFixture<TestWrapperComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      schemas: [NO_ERRORS_SCHEMA],
      imports: [
        BrowserAnimationsModule,
        OverlayModule,
        PortalModule,
        FormsModule,
      ],
      declarations: [
        TimepickerInnerComponent,
        DatepickerLiteComponent,
        TranslatePipeStub,
        TestWrapperComponent,
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestWrapperComponent);
    component = fixture.debugElement.children[0].componentInstance;
    testComponent = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should clear calendar', () => {
    expect(component.value).toBeTruthy();
    component.clearCalendar(new Event('click'));
    expect(component.valueSource).toBeFalsy();
  });

  it('should set time', fakeAsync(() => {
    component.show();
    component.changeTimeView(new Event('click'));
    fixture.detectChanges();
    spyOn(component.timepicker, 'initPosition');
    tick(1000);
    expect(component.timepicker.initPosition).toHaveBeenCalled();

    const newTime = new Date();
    component.changeTime(newTime);
    expect(component.value).toEqual(newTime);
  }));

  it('should set year', () => {
    component.changeYearView(new Event('click'));
    expect(component.mode).toEqual('year');

    const currentYear = (new Date()).getFullYear();
    expect(component.showYear).toEqual(currentYear);

    component.preYear();
    expect(component.showYear).toEqual(currentYear - 1);
    component.nextYear();
    expect(component.showYear).toEqual(currentYear);

    component.setShowYear(2008, new Event('click'));
    expect(component.showYear).toEqual(2008);
  });

  it('should set decade', () => {
    component.changeDecadeView(new Event('click'));
    expect(component.mode).toEqual('decade');

    const currentYear = (new Date()).getFullYear();
    const currentDecade = Math.floor(currentYear / 10) * 10;
    expect(component.startDecade).toEqual(currentDecade);

    component.preDecade();
    expect(component.startDecade).toEqual(currentDecade - 10);
    component.nextDecade();
    expect(component.startDecade).toEqual(currentDecade);
  });

  it('should set month', () => {
    component.changeMonthView();
    expect(component.mode).toEqual('month');

    const currentMonth = (new Date()).getMonth();
    expect(component.showMonth).toEqual(currentMonth);

    component.preMonth();
    expect(component.showMonth).toEqual((currentMonth + 11) % 12);
    component.nextMonth();
    expect(component.showMonth).toEqual(currentMonth);
    component.nextMonth();
    expect(component.showMonth).toEqual((currentMonth + 1) % 12);
    component.preMonth();
    expect(component.showMonth).toEqual(currentMonth);

    const month = new CalendarMonth();
    month.index = 6;
    component.clickMonth(month);
    expect(component.showMonth).toEqual(6);
    expect(component.mode).toEqual('year');
  });

  it('should set day', () => {
    component.value = new Date('2018-01-01');
    component.changeToToday();
    expect(component.value.getDay()).toEqual(new Date().getDay());

    const day = new CalendarDay();
    day.date = new Date('2018-01-01');
    component.clickDay(day);

    component.showTime = false;
    component.clickDay(day);
    expect(component.value).toEqual(day.date);
  });
});
