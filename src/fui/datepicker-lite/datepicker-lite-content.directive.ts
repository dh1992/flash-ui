import {Directive, TemplateRef, ViewContainerRef} from '@angular/core';
import {CdkPortal} from '@angular/cdk/portal';

@Directive({
  // eslint-disable-next-line
  selector: '[fui-datepicker-lite-content]'
})
export class DatepickerLiteContentDirective extends CdkPortal {

  constructor(
    templateRef: TemplateRef<any>,
    viewContainerRef: ViewContainerRef,
  ) {
    super(templateRef, viewContainerRef);
  }

}
