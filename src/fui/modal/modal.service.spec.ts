import { Router } from '@angular/router';
import { TestBed, inject } from '@angular/core/testing';

import { of } from 'rxjs';

import { fuiModalService } from './modal.service';

describe('ModaService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        fuiModalService,
        {
          provide: Router,
          useValue: {
            events: of(null),
          },
        },
      ],
    });
  });

  it('should be created', inject([fuiModalService], (service: fuiModalService) => {
    expect(service).toBeTruthy();
  }));
});
