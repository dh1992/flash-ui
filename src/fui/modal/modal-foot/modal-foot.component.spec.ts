import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ModalFootComponent } from './modal-foot.component';

describe('ModalFootComponent', () => {
  let component: ModalFootComponent;
  let fixture: ComponentFixture<ModalFootComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalFootComponent ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalFootComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
