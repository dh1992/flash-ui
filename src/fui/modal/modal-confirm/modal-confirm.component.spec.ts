import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';

import { TranslatePipeStub } from '../../../mock';
import { ModalConfirmComponent } from './modal-confirm.component';
import { fui_MODAL_DATA, fuiModalRef } from '../modal.module';

class fuiModalRefStub {
  close() {}
}

describe('ModalConfirmComponent', () => {
  let component: ModalConfirmComponent;
  let fixture: ComponentFixture<ModalConfirmComponent>;
  let modalRef: fuiModalRef;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [
        ModalConfirmComponent,
        TranslatePipeStub,
      ],
      providers: [
        {
          provide: fuiModalRef,
          useClass: fuiModalRefStub,
        },
        {
          provide: fui_MODAL_DATA,
          useValue: {},
        },
      ],
      schemas: [
        NO_ERRORS_SCHEMA,
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    modalRef = TestBed.inject(fuiModalRef);
    fixture = TestBed.createComponent(ModalConfirmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should confirm/cancel', () => {
    spyOn(modalRef, 'close');
    component.cancel();
    expect(modalRef.close).toHaveBeenCalledWith(false);
    component.confirm();
    expect(modalRef.close).toHaveBeenCalledWith(true);
  });
});
