import { Injector } from '@angular/core';

export interface ConfirmModalConfig {
  title?: string;
  message: string;
  backdropClose?: boolean;
}

export interface fuiModalConfig {
  data?: any;
  title?: string;
  size?: 'sm' | 'md' | 'lg' | 'xlg' | any;
  backdropClose?: boolean;
  injector?: Injector;
}

export type ComponentType<T> = new (...args: any[]) => T;
