import {
  Injectable,
  ComponentFactoryResolver,
  ApplicationRef,
  Injector,
  ComponentRef,
} from '@angular/core';
import { Router, Event, NavigationStart } from '@angular/router';
import { Observable } from 'rxjs';

import { ModalConfirmComponent } from './modal-confirm/modal-confirm.component';
import { ModalContainerComponent } from './modal-container/modal-container.component';
import { ModalTipComponent } from './modal-tip/modal-tip.component';
import {
  ConfirmModalConfig,
  fuiModalConfig,
  ComponentType,
} from './modal.model';

@Injectable()
export class fuiModalService {

  // 缓存已经打开的对话框的ref引用，对话框关闭后需要清理`cache`
  private cache: {[modalId: number]: ModalContainerComponent} = {};

  constructor(
    private componentFactoryResolver: ComponentFactoryResolver,
    private appRef: ApplicationRef,
    private injector: Injector,
    private router: Router,
  ) {
    // 在路由开始变化时关闭当前所有对话框
    router.events.subscribe((event: Event) => {
      if (!(event instanceof NavigationStart)) {
        return;
      }
      this.closeAll();
    });
  }

  private addCache(modalContainerInstance: ModalContainerComponent) {
    const {id, modalRef} = modalContainerInstance;
    this.cache[id] = modalContainerInstance;
  }

  private removeCache(modalId: number) {
    delete this.cache[modalId];
  }

  /** Close all modals. */
  closeAll() {
    Object.keys(this.cache).forEach((modalId) => {
      const modal: ModalContainerComponent = this.cache[modalId];
      modal.modalRef.close();
    });
  }

  /** Open a new modal. */
  open<T>(Component: ComponentType<T>, config: fuiModalConfig = {}): Observable<any> {
    const modalContainerFactory = this.componentFactoryResolver.resolveComponentFactory(ModalContainerComponent);
    const modalContainerRef: ComponentRef<ModalContainerComponent> = modalContainerFactory.create(this.injector);
    const {instance: modalContainerInstance} = modalContainerRef;

    // 为modalContainer实例传入需要挂载的组件，以及组件的配置项
    modalContainerInstance.modalBodyComponent = Component;
    modalContainerInstance.config = config;

    // 在applicationRef上挂载fuiModalContainer实例
    this.appRef.attachView(modalContainerRef.hostView);

    this.addCache(modalContainerInstance);

    const complete = () => {
      modalContainerRef.destroy();
      this.removeCache(modalContainerInstance.id);
    };

    // 在对话框关闭时，销毁fuiModalContainer实例
    modalContainerInstance.modalRef.afterClose.subscribe({ complete });

    // 在实现了ControlValueAccessor接口的组件（即能使用ngModel指令的组件）上触发blur事件，
    // 会在同一轮循环内改变ngModel的值，从而抛出ExpressionChangedAfterItHasBeenCheckedError
    setTimeout(() => {
      (document.activeElement as HTMLElement).blur();
    });

    return modalContainerInstance.modalRef.afterClose;
  }

  /** Open confirm modal. Return true if confirmed, false otherwise. */
  confirm(config: ConfirmModalConfig): Observable<boolean> {
    const {title, message, backdropClose} = config;
    return this.open(ModalConfirmComponent, {
      title,
      data: {message},
      size: 'sm',
      backdropClose,
    });
  }

  /** Open a success style modal. Return true if confirmed, false otherwise. */
  success(config: ConfirmModalConfig): Observable<boolean> {
    const {title = 'fui.MODAL.SYSTEM_TIP', message, backdropClose} = config;
    return this.open(ModalTipComponent, {
      data: {
        type: 'success',
        title,
        message,
      },
      size: 'sm',
      backdropClose,
    });
  }

  /** Open a info style modal. Return true if confirmed, false otherwise. */
  info(config: ConfirmModalConfig): Observable<boolean> {
    const {title = 'fui.MODAL.SYSTEM_TIP', message, backdropClose} = config;
    return this.open(ModalTipComponent, {
      data: {
        type: 'info',
        title,
        message,
      },
      size: 'sm',
      backdropClose,
    });
  }

  /** Open a warning style modal. Return true if confirmed, false otherwise. */
  warning(config: ConfirmModalConfig): Observable<boolean> {
    const {title = 'fui.MODAL.SYSTEM_TIP', message, backdropClose} = config;
    return this.open(ModalTipComponent, {
      data: {
        type: 'warning',
        title,
        message,
      },
      size: 'sm',
      backdropClose,
    });
  }

  /** Open a error style modal. Return true if confirmed, false otherwise. */
  error(config: ConfirmModalConfig): Observable<boolean> {
    const {title = 'fui.MODAL.ERROR', message = 'fui.MODAL.UNKNOWN_ERROR', backdropClose} = config;
    return this.open(ModalTipComponent, {
      data: {
        type: 'error',
        title,
        message,
      },
      size: 'sm',
      backdropClose,
    });
  }

}
