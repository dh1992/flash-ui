import {
  Injectable,
} from '@angular/core';
import { Subject } from 'rxjs';

@Injectable()
export class fuiModalRef {
  /** 关闭对话框的操作都需要通过`afterClose`进行 */
  afterClose = new Subject();

  /**
   * Close current modal
   *
   * @param data Any data need to pass on
   */
  close(data?: any) {
    this.afterClose.next(data);
    this.afterClose.complete();
  }

}
