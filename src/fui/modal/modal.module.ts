import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { I18nModule } from '../i18n/i18n.module';
import { IconModule } from '../icon/icon.module';
import { BtnModule } from '../btn/btn.module';

import { ModalContainerComponent } from './modal-container/modal-container.component';
import { ModalConfirmComponent } from './modal-confirm/modal-confirm.component';
import { ModalCountService } from './modal-count.service';
import { fuiModalService } from './modal.service';
import { ModalFootComponent } from './modal-foot/modal-foot.component';
import { ModalTipComponent } from './modal-tip/modal-tip.component';

export * from './modal-confirm/modal-confirm.component';
export * from './modal-container/modal-container.component';
export * from './modal-count.service';
export * from './modal-ref.service';
export * from './modal-token';
export * from './modal.service';
export * from './modal.model';
export * from './modal-foot/modal-foot.component';
export * from './modal-tip/modal-tip.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    I18nModule,
    IconModule,
    BtnModule,
  ],
  declarations: [
    ModalContainerComponent,
    ModalTipComponent,
    ModalConfirmComponent,
    ModalFootComponent,
  ],
  providers: [
    ModalCountService,
    fuiModalService,
  ],
  entryComponents: [
    ModalContainerComponent,
    ModalTipComponent,
    ModalConfirmComponent,
  ],
  exports: [
    ModalFootComponent,
  ],
})
export class ModalModule { }
