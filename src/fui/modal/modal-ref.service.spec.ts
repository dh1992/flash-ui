import { TestBed, inject } from '@angular/core/testing';

import { fuiModalRef } from './modal-ref.service';

describe('fuiModalRef', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [fuiModalRef],
    });
  });

  it('should be created', inject([fuiModalRef], (service: fuiModalRef) => {
    expect(service).toBeTruthy();
  }));
});
