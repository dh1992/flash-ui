import { Component, Inject, OnInit } from '@angular/core';

import { fuiModalRef } from '../modal-ref.service';
import { fui_MODAL_DATA } from '../modal-token';

@Component({
  selector: 'fui-modal-tip',
  templateUrl: './modal-tip.component.html',
  styleUrls: ['./modal-tip.component.sass'],
})
export class ModalTipComponent implements OnInit {
  type: 'success' | 'info' | 'warning' | 'error';
  title: string;
  message: string;

  constructor(
    private modalRef: fuiModalRef,
    @Inject(fui_MODAL_DATA) data,
  ) {
    this.type = data.type;
    this.title = data.title;
    this.message = data.message;
  }

  ngOnInit() {
  }

  confirm() {
    this.modalRef.close(true);
  }

  cancel() {
    this.modalRef.close(false);
  }

}
