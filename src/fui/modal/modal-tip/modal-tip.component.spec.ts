import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { TranslatePipeStub } from '../../../mock';

import { ModalTipComponent } from './modal-tip.component';
import { fui_MODAL_DATA, fuiModalRef } from '../modal.module';

describe('ModalTipComponent', () => {
  let component: ModalTipComponent;
  let fixture: ComponentFixture<ModalTipComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      schemas: [
        NO_ERRORS_SCHEMA,
      ],
      declarations: [
        ModalTipComponent,
        TranslatePipeStub,
      ],
      providers: [
        fuiModalRef,
        {
          provide: fui_MODAL_DATA,
          useValue: {},
        },
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalTipComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
