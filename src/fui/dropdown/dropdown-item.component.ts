import {
  Component,
  HostBinding,
} from '@angular/core';

@Component({
  selector: 'fui-dropdown-item',
  template: '<ng-content></ng-content>',
})
export class DropdownItemComponent {
  @HostBinding('class.fui-dropdown-item') hostClass = true;
}
