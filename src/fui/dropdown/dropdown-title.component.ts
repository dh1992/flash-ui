import {
  Component,
  HostBinding,
} from '@angular/core';

@Component({
  selector: 'fui-dropdown-title',
  template: '<ng-content></ng-content>',
})
export class DropdownTitleComponent {
  @HostBinding('class.fui-dropdown-title') hostClass = true;
}
