import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OverlayModule } from '@angular/cdk/overlay';

import { DropdownComponent } from './dropdown.component';
import { DropdownTriggerDirective } from './dropdown-trigger.directive';
import { DropdownItemComponent } from './dropdown-item.component';
import { DropdownTitleComponent } from './dropdown-title.component';

export * from './dropdown.component';
export * from './dropdown-trigger.directive';
export * from './dropdown-item.component';
export * from './dropdown-title.component';

@NgModule({
  imports: [
    CommonModule,
    OverlayModule,
  ],
  declarations: [
    DropdownComponent,
    DropdownTriggerDirective,
    DropdownItemComponent,
    DropdownTitleComponent,
  ],
  exports: [
    DropdownComponent,
    DropdownTriggerDirective,
    DropdownItemComponent,
    DropdownTitleComponent,
  ],
})
export class DropdownModule { }
