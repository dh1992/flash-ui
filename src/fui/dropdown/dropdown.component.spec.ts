import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { CdkConnectedOverlayDirective } from '../../mock';

import { DropdownComponent } from './dropdown.component';

describe('DropdownComponent', () => {
  let component: DropdownComponent;
  let fixture: ComponentFixture<DropdownComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ DropdownComponent, CdkConnectedOverlayDirective ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DropdownComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should set open', () => {
    spyOn(component, 'show');
    spyOn(component, 'hide');

    component.open = true;
    expect(component.show).toHaveBeenCalled();
    expect(component.open).toEqual(true);

    component.open = false;
    expect(component.hide).toHaveBeenCalled();
    expect(component.open).toEqual(false);
  });

  it('should handle click event', () => {
    spyOn(component, 'hide');

    const clickEvent = new MouseEvent('click');
    component.clickDropdown(clickEvent);
    expect(component.hide).toHaveBeenCalled();
  });

  it('should handle mouse event', () => {
    spyOn(component, 'hide');

    component.mouseLeave();
    expect(component.hide).not.toHaveBeenCalled();

    component.trigger = 'hover';
    component.mouseLeave();
    expect(component.hide).toHaveBeenCalled();
  });
});
