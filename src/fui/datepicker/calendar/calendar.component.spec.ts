import { NO_ERRORS_SCHEMA } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { Observable } from 'rxjs';

import { fuiTranslateService } from '../../i18n/i18n.module';
import { MockModule, TranslateServiceStub } from '../../../mock';

import { fuiNativeDateModule } from '../datepicker.module';
import { CalendarBodyComponent } from '../calendar-body/calendar-body.component';
import { YearViewComponent } from '../year-view/year-view.component';
import { MonthViewComponent } from '../month-view/month-view.component';
import { CalendarComponent } from './calendar.component';

class DatePipeStub {
  transform() {}
}

describe('CalendarComponent', () => {
  let component: CalendarComponent<any>;
  let fixture: ComponentFixture<CalendarComponent<any>>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      schemas: [NO_ERRORS_SCHEMA],
      imports: [fuiNativeDateModule, MockModule],
      declarations: [
        CalendarBodyComponent,
        YearViewComponent,
        MonthViewComponent,
        CalendarComponent,
      ],
      providers: [
        {
          provide: DatePipe,
          useClass: DatePipeStub,
        },
        {
          provide: fuiTranslateService,
          useClass: TranslateServiceStub,
        },
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CalendarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should change view', () => {
    component._changeView('year');
    expect(component._monthView).toEqual(false);
    component._changeView('month');
    expect(component._monthView).toEqual(true);
  });

  it('on date select', () => {
    spyOn(component.selectedChange, 'emit');

    component.selected = new Date(2000, 6, 1);
    component._dateSelected(new Date(2000, 6, 30));
    expect(component.selectedChange.emit).toHaveBeenCalledWith(new Date(2000, 6, 30));
  });

  it('on user select', () => {
    spyOn(component.userSelection, 'emit');
    component._userSelected();
    expect(component.userSelection.emit).toHaveBeenCalled();
  });

  it('on month select', () => {
    component._monthSelected(new Date(2000, 6, 1));
    expect(component._monthView).toEqual(true);
  });

  it('on previous clicked', () => {
    component._activeDate = new Date(2000, 6, 1);

    component._monthView = true;
    component._previousClicked();
    expect(component._activeDate).toEqual(new Date(2000, 5, 1));

    component._monthView = false;
    component._previousClicked();
    expect(component._activeDate).toEqual(new Date(1999, 5, 1));
  });

  it('on next clicked', () => {
    component._activeDate = new Date(2000, 6, 1);

    component._monthView = true;
    component._nextClicked();
    expect(component._activeDate).toEqual(new Date(2000, 7, 1));

    component._monthView = false;
    component._nextClicked();
    expect(component._activeDate).toEqual(new Date(2001, 7, 1));
  });
});
