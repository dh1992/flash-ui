import { NgModule } from '@angular/core';
import { DateAdapter } from './date-adapter';
import { NativeDateAdapter } from './native-date-adapter';
import { fui_DATE_FORMATS } from './date-formats';
import { fui_NATIVE_DATE_FORMATS } from './native-date-formats';


export * from './date-adapter';
export * from './date-formats';
export * from './native-date-adapter';
export * from './native-date-formats';


@NgModule({
  providers: [
    {provide: fui_DATE_FORMATS, useValue: fui_NATIVE_DATE_FORMATS},
    {provide: DateAdapter, useClass: NativeDateAdapter},
  ],
})
export class fuiNativeDateModule {}
