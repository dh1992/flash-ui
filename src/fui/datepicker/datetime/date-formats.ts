import {InjectionToken} from '@angular/core';

export class fuiDateFormats {
  parse: {
    dateInput: any;
  };
  display: {
    dateInput: any;
    monthYearLabel: any;
    dateA11yLabel: any;
    monthYearA11yLabel: any;
    weekdayLabel: any;
    timeLabel: any;
    fullTimeLabel: any;
  };
}


export const fui_DATE_FORMATS = new InjectionToken<fuiDateFormats>('fui-date-formats');
