import {fuiDateFormats} from './date-formats';

export const fui_NATIVE_DATE_FORMATS: fuiDateFormats = {
  parse: {
    dateInput: null,
  },
  display: {
    dateInput: {year: 'numeric', month: 'numeric', day: 'numeric'},
    monthYearLabel: {year: 'numeric', month: 'long'},
    dateA11yLabel: {year: 'numeric', month: 'long', day: 'numeric'},
    monthYearA11yLabel: {year: 'numeric', month: 'long'},
    weekdayLabel: {weekday: 'short', month: 'numeric', day: 'numeric'},
    timeLabel: {hour: 'numeric', minute: 'numeric', second: 'numeric'},
    fullTimeLabel: {
      year: '2-digit',
      month: 'numeric',
      day: 'numeric',
      hour: 'numeric',
      minute: 'numeric',
      second: 'numeric',
      hour12: false,
    },
  },
};
