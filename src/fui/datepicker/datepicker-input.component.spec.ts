import { NO_ERRORS_SCHEMA, Component, NgModule } from '@angular/core';
import { DatePipe } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { OverlayModule } from '@angular/cdk/overlay';
import { By } from '@angular/platform-browser';
import { Observable } from 'rxjs';

import { fuiTranslateService } from '../i18n/i18n.module';
import { MockModule, TranslateServiceStub } from '../../mock';
import { fuiNativeDateModule } from './datetime/native-date.module';

import {
  DatepickerComponent,
  DatepickerContentComponent,
  fui_DATEPICKER_SCROLL_STRATEGY_PROVIDER,
} from './datepicker.component';
import { DatepickerInputComponent } from './datepicker-input.component';

class DatePipeStub {
  transform() {}
}

@Component({
  template: `
    <fui-datepicker-input [datepicker]="picker" [(ngModel)]="date"></fui-datepicker-input>
    <fui-datepicker #picker></fui-datepicker>
  `,
})
class TestComponent {
  date = new Date();
}

@NgModule({
  schemas: [NO_ERRORS_SCHEMA],
  exports: [DatepickerContentComponent],
  declarations: [DatepickerContentComponent],
  entryComponents: [DatepickerContentComponent],
})
class DatepickerInputComponentTestModule { }

describe('DatepickerInputComponent', () => {
  let testComponent: TestComponent;
  let component: DatepickerComponent<any>;
  let inputComponent: DatepickerInputComponent<any>;
  let fixture: ComponentFixture<TestComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      schemas: [ NO_ERRORS_SCHEMA ],
      imports: [
        MockModule,
        fuiNativeDateModule,
        FormsModule,
        OverlayModule,
        DatepickerInputComponentTestModule,
      ],
      declarations: [
        DatepickerComponent,
        DatepickerInputComponent,
        TestComponent,
      ],
      providers: [
        fui_DATEPICKER_SCROLL_STRATEGY_PROVIDER,
        {
          provide: DatePipe,
          useClass: DatePipeStub,
        },
        {
          provide: fuiTranslateService,
          useValue: TranslateServiceStub,
        },
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestComponent);
    testComponent = fixture.componentInstance;
    inputComponent = fixture.debugElement.children[0].componentInstance;
    component = fixture.debugElement.children[1].componentInstance;
    fixture.detectChanges();
  });

  it('should open datepicker', () => {
    expect(inputComponent.datepicker).toBeTruthy();

    spyOn(component, 'open');
    const event = new Event('click');
    inputComponent.openDatepicker(event);
    expect(component.open).toHaveBeenCalled();
  });

  it('on select change', () => {
    component.selectedChanged.emit(new Date(2000, 6, 1));
    expect(inputComponent.value).toEqual(new Date(2000, 6, 1));
  });

  it('on input change', () => {
    const event = {
      target: {
        value: '2000/06/01',
      },
    };
    inputComponent.inputChange(event);
    expect(inputComponent.value).toEqual(new Date(2000, 5, 1));
  });

  it('should open/close', () => {
    component.open();
    expect(component.opened).toBeTruthy();

    component.close();
    expect(component.opened).toBeFalsy();
  });

  it('should select date', () => {
    spyOn(component.selectedChanged, 'emit');
    component._selected = new Date(2000, 6, 1);
    component._select(new Date(2000, 6, 2));
    expect(component.selectedChanged.emit).toHaveBeenCalledWith(new Date(2000, 6, 2));
  });
});
