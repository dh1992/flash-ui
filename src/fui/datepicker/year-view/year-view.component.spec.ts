import { NO_ERRORS_SCHEMA } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { Observable } from 'rxjs';

import { fuiTranslateService } from '../../i18n/i18n.module';
import { MockModule, TranslateServiceStub } from '../../../mock';

import { fuiNativeDateModule } from '../datepicker.module';
import { CalendarBodyComponent } from '../calendar-body/calendar-body.component';
import { YearViewComponent } from './year-view.component';

class DatePipeStub {
  transform() {}
}

describe('YearViewComponent', () => {
  let component: YearViewComponent<any>;
  let fixture: ComponentFixture<YearViewComponent<any>>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      schemas: [NO_ERRORS_SCHEMA],
      imports: [fuiNativeDateModule, MockModule],
      declarations: [
        CalendarBodyComponent,
        YearViewComponent,
      ],
      providers: [
        {
          provide: DatePipe,
          useClass: DatePipeStub,
        },
        {
          provide: fuiTranslateService,
          useClass: TranslateServiceStub,
        },
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(YearViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should select month', () => {
    spyOn(component.selectedChange, 'emit');

    component.activeDate = new Date(2000, 6, 1);
    component.selected = new Date(2000, 6, 1);
    component._monthSelected(2);
    expect(component.selectedChange.emit).toHaveBeenCalledWith(new Date(2000, 2, 1));
  });
});
