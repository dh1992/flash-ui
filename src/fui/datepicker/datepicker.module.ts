import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { OverlayModule } from '@angular/cdk/overlay';
import { I18nModule } from '../i18n/i18n.module';
import { IconModule } from '../icon/icon.module';
import { FormModule } from '../form/form.module';
import { BtnModule } from '../btn/btn.module';

import { MonthViewComponent } from './month-view/month-view.component';
import { YearViewComponent } from './year-view/year-view.component';
import { CalendarBodyComponent  } from './calendar-body/calendar-body.component';
import { CalendarComponent } from './calendar/calendar.component';
import { DatepickerInputComponent } from './datepicker-input.component';
import { fui_DATEPICKER_SCROLL_STRATEGY_PROVIDER, DatepickerComponent, DatepickerContentComponent } from './datepicker.component';
import { fuiNativeDateModule } from './datetime/native-date.module';
import { TimepickerModule } from '../timepicker/timepicker.module';

export * from './datepicker.component';
export * from './datepicker-input.component';
export * from './calendar/calendar.component';
export * from './datetime/native-date.module';
export * from './month-view/month-view.component';
export * from './year-view/year-view.component';
export * from './calendar-body/calendar-body.component';
export * from './calendar-cell.model';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    OverlayModule,
    I18nModule,
    IconModule,
    FormModule,
    fuiNativeDateModule,
    TimepickerModule,
    BtnModule,
  ],
  declarations: [
    MonthViewComponent,
    YearViewComponent,
    CalendarBodyComponent,
    CalendarComponent,
    DatepickerInputComponent,
    DatepickerComponent,
    DatepickerContentComponent,
  ],
  providers: [
    fui_DATEPICKER_SCROLL_STRATEGY_PROVIDER,
    DatePipe,
  ],
  entryComponents: [
    DatepickerContentComponent,
  ],
  exports: [
    MonthViewComponent,
    YearViewComponent,
    CalendarBodyComponent,
    CalendarComponent,
    DatepickerInputComponent,
    DatepickerComponent,
    DatepickerContentComponent,
  ],
})
export class DatepickerModule { }
