import { NO_ERRORS_SCHEMA } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { Observable } from 'rxjs';

import { fuiTranslateService } from '../../i18n/i18n.module';
import { MockModule, TranslateServiceStub } from '../../../mock';

import { fuiNativeDateModule } from '../datepicker.module';
import { CalendarBodyComponent } from '../calendar-body/calendar-body.component';
import { MonthViewComponent } from './month-view.component';

class DatePipeStub {
  transform() {}
}

describe('MonthViewComponent', () => {
  let component: MonthViewComponent<any>;
  let fixture: ComponentFixture<MonthViewComponent<any>>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      schemas: [NO_ERRORS_SCHEMA],
      imports: [fuiNativeDateModule, MockModule],
      declarations: [
        CalendarBodyComponent,
        MonthViewComponent,
      ],
      providers: [
        {
          provide: DatePipe,
          useClass: DatePipeStub,
        },
        {
          provide: fuiTranslateService,
          useClass: TranslateServiceStub,
        },
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MonthViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should select date', () => {
    spyOn(component.selectedChange, 'emit');

    component.activeDate = new Date(2000, 6, 1);
    component.selected = new Date(2000, 6, 1);
    component._dateSelected(2);
    expect(component.selectedChange.emit).toHaveBeenCalledWith(new Date(2000, 6, 2));
  });
});
