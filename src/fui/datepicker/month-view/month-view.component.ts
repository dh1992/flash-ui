import {
  Component,
  AfterContentInit,
  Input,
  Output,
  EventEmitter,
  Inject,
} from '@angular/core';

import {
  DateAdapter,
  fuiDateFormats,
  fui_DATE_FORMATS,
} from '../datetime/native-date.module';

import { CalendarCell } from '../calendar-cell.model';

const DAYS_PER_WEEK = 7;

@Component({
  selector: 'fui-month-view',
  templateUrl: './month-view.component.html',
  styleUrls: ['./month-view.component.sass'],
})
export class MonthViewComponent<D> implements AfterContentInit {
  /**
   * The date to display in this month view (everything other than the month and year is ignored).
   */
  @Input()
  set activeDate(value: D) {
    const oldActiveDate = this._activeDate;
    this._activeDate = value || this._dateAdapter.today();
    if (!this._hasSameMonthAndYear(oldActiveDate, this._activeDate)) {
      this._init();
    }
  }
  get activeDate(): D {
    return this._activeDate;
  }
  private _activeDate: D;

  /** The currently selected date. */
  @Input()
  set selected(value: D) {
    this._selected = value;
    this._selectedDate = this._getDateInCurrentMonth(this.selected);
  }
  get selected(): D {
    return this._selected;
  }
  private _selected: D;

  /** A function used to filter which dates are selectable. */
  @Input() dateFilter: (date: D) => boolean;

  /** The switch of showing time. */
  @Input() showTime = false;

  /** The customed date format */
  @Input() format: string;

  /** Emits when a new date is selected. */
  @Output() selectedChange = new EventEmitter<D | null>();

  /** Emits when any date is selected. */
  @Output() userSelection = new EventEmitter<void>();

  /** The label for this month (e.g. "January 2017"). */
  _monthLabel: string;

  /** Grid of calendar cells representing the dates of the month. */
  _weeks: CalendarCell[][];

  /** The number of blank cells in the first row before the 1st of the month. */
  _firstWeekOffset: number;

  /**
   * The date of the month that the currently selected Date falls on.
   * Null if the currently selected Date is in another month.
   */
  _selectedDate: number | null;

  /** The date of the month that today falls on. Null if today is in another month. */
  _todayDate: number | null;

  /** The names of the weekdays. */
  _weekdays: {long: string; narrow: string}[];

  constructor(
    public _dateAdapter: DateAdapter<D>,
    @Inject(fui_DATE_FORMATS) private _dateFormats: fuiDateFormats,
  ) {
    const firstDayOfWeek = this._dateAdapter.getFirstDayOfWeek();
    const narrowWeekdays = this._dateAdapter.getDayOfWeekNames('narrow');
    const longWeekdays = this._dateAdapter.getDayOfWeekNames('long');

    // Rotate the labels for days of the week based on the configured first day of the week.
    const weekdays = longWeekdays.map((long, i) => ({long, narrow: narrowWeekdays[i]}));
    this._weekdays = weekdays.slice(firstDayOfWeek).concat(weekdays.slice(0, firstDayOfWeek));

    this._activeDate = this._dateAdapter.today();
  }

  ngAfterContentInit(): void {
    this._init();
  }

  /** Handles when a new date is selected. */
  _dateSelected(date: number) {
    const dateParams = [];
    if (this._selectedDate !== date) {
      dateParams.push(
        this._dateAdapter.getYear(this.activeDate),
        this._dateAdapter.getMonth(this.activeDate),
        date,
      );
      if (this.showTime) {
        dateParams.push(
          this._dateAdapter.getHours(this.activeDate),
          this._dateAdapter.getMinutes(this.activeDate),
          this._dateAdapter.getSeconds(this.activeDate),
        );
      }
      const selectedDate = this._dateAdapter.createDate.apply(this._dateAdapter, dateParams);

      this.selectedChange.emit(selectedDate);
    }

    if (!this.showTime) {
      this.userSelection.emit();
    }
  }

  /** Initializes this month view. */
  private _init() {
    this._selectedDate = this._getDateInCurrentMonth(this.selected);
    this._todayDate = this._getDateInCurrentMonth(this._dateAdapter.today());
    this._monthLabel =
        this._dateAdapter.getMonthNames('short')[this._dateAdapter.getMonth(this.activeDate)]
            .toLocaleUpperCase();

    const firstOfMonth = this._dateAdapter.createDate(this._dateAdapter.getYear(this.activeDate),
        this._dateAdapter.getMonth(this.activeDate), 1);
    this._firstWeekOffset =
        (DAYS_PER_WEEK + this._dateAdapter.getDayOfWeek(firstOfMonth) -
         this._dateAdapter.getFirstDayOfWeek()) % DAYS_PER_WEEK;

    this._createWeekCells();
  }

  /** Creates MdCalendarCells for the dates in this month. */
  private _createWeekCells() {
    const daysInMonth = this._dateAdapter.getNumDaysInMonth(this.activeDate);
    const dateNames = this._dateAdapter.getDateNames();
    this._weeks = [[]];
    for (let i = 0, cell = this._firstWeekOffset; i < daysInMonth; i++, cell++) {
      if (cell === DAYS_PER_WEEK) {
        this._weeks.push([]);
        cell = 0;
      }
      const date = this._dateAdapter.createDate(
          this._dateAdapter.getYear(this.activeDate),
          this._dateAdapter.getMonth(this.activeDate), i + 1);
      const enabled = !this.dateFilter ||
          this.dateFilter(date);
      const ariaLabel = this._dateAdapter.format(date, this.format);
      this._weeks[this._weeks.length - 1]
          .push(new CalendarCell(i + 1, dateNames[i], ariaLabel, enabled));
    }
  }

  /**
   * Gets the date in this month that the given Date falls on.
   * Returns null if the given Date is in another month.
   */
  private _getDateInCurrentMonth(date: D): number | null {
    return this._hasSameMonthAndYear(date, this.activeDate) ?
        this._dateAdapter.getDate(date) : null;
  }

  /** Checks whether the 2 dates are non-null and fall within the same month of the same year. */
  private _hasSameMonthAndYear(d1: D, d2: D): boolean {
    return !!(d1 && d2 && this._dateAdapter.getMonth(d1) === this._dateAdapter.getMonth(d2) &&
              this._dateAdapter.getYear(d1) === this._dateAdapter.getYear(d2));
  }

}
