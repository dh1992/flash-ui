import { Directive, Input, HostBinding } from '@angular/core';

@Directive({
  selector: '[fuiForm]',
})
export class FormDirective {
  @HostBinding('class.fui-form') true;

  /**
   * Label flex style.
   */
  @Input() labelFlex: number;

  /**
   * Input flex style.
   */
  @Input() inputFlex: number;
}
