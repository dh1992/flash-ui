import {
  Component,
  Optional,
  Inject,
  Input,
  ViewChild,
  HostBinding,
} from '@angular/core';

import {
  NgModel,
  NG_VALUE_ACCESSOR,
  NG_VALIDATORS,
  NG_ASYNC_VALIDATORS,
} from '@angular/forms';

import { ElementBase } from '../element-base';

@Component({
  selector: 'fui-textarea',
  templateUrl: './textarea.component.html',
  styleUrls: ['./textarea.component.sass'],
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: TextareaComponent,
    multi: true,
  }],
})
export class TextareaComponent extends ElementBase<string> {
  @HostBinding('class.fui-textarea') hostClass = true;
  @HostBinding('class.fui-textarea-resize-vertical')
  get resize() {
    return this.resizeVertical;
  }

  /** Placeholder. */
  @Input() placeholder: string;

  /** Validation error messages. */
  @Input() errorMsgs: string[] = [];

  /** Rows to display. */
  @Input() rows = 4;

  /** Max text length. */
  @Input() maxlength: number;

  /** Allow resizing vertically. (no IE support) */
  @Input() resizeVertical = false;


  @ViewChild(NgModel, { static: true }) model: NgModel;

  get parsedPlaceholder() {
    return this.placeholder || '';
  }

  constructor(
    @Optional() @Inject(NG_VALIDATORS) validators: Array<any>,
    @Optional() @Inject(NG_ASYNC_VALIDATORS) asyncValidators: Array<any>,
  ) {
    super(validators, asyncValidators);
  }

}
