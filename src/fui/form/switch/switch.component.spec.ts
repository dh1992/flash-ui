import { Component, NO_ERRORS_SCHEMA } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { By } from '@angular/platform-browser';

import { SwitchComponent } from './switch.component';

@Component({
  template: `
    <fui-switch></fui-switch>
  `,
})
class TestComponent {}

describe('SwitchComponent', () => {
  let testComponent: TestComponent;
  let component: SwitchComponent;
  let fixture: ComponentFixture<TestComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      schemas: [ NO_ERRORS_SCHEMA ],
      imports: [ FormsModule ],
      declarations: [
        SwitchComponent,
        TestComponent,
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestComponent);
    testComponent = fixture.componentInstance;
    component = fixture.debugElement.query(By.directive(SwitchComponent)).componentInstance;
    fixture.detectChanges();
  });

  it('should handle click', () => {
    const mouseEvent = new MouseEvent('click');
    component.onClick(mouseEvent);
    expect(component.value).toEqual(true);
  });
});
