import { Directive, HostBinding } from '@angular/core';

@Directive({
  selector: '[fuiFormItemAddon]',
})
export class FormItemAddonDirective {
  @HostBinding('class.fui-form-item-addon') true;

  constructor() { }

}
