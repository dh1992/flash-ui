import { Directive, Input, HostBinding, Component, NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { By } from '@angular/platform-browser';

import { FormItemComponent } from './form-item.component';
import { FormDirective } from '../form.directive';

@Component({
  template: `
    <div fuiForm labelFlex="2" inputFlex="3">
      <fui-form-item></fui-form-item>
    </div>
  `,
})
class TestComponent {}

describe('FormItemComponent', () => {
  let component: FormItemComponent;
  let fixture: ComponentFixture<FormItemComponent>;
  let element: HTMLElement;
  let itemLabel: HTMLElement;
  let itemBody: HTMLElement;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      schemas: [ NO_ERRORS_SCHEMA ],
      declarations: [ TestComponent, FormItemComponent, FormDirective ],
      providers: [ FormDirective ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormItemComponent);
    component = fixture.componentInstance;
    element = fixture.debugElement.nativeElement;
    itemLabel = fixture.debugElement.query(By.css('.form-label')).nativeElement;
    itemBody = fixture.debugElement.query(By.css('.fui-form-item-body')).nativeElement;
    fixture.detectChanges();
  });

  it('should add fui-form-item class to host element', () => {
    expect(element.classList.contains('fui-form-item')).toBeTruthy();
  });

  it('should accept label text input', () => {
    component.label = 'hello';
    fixture.detectChanges();
    expect(itemLabel.textContent.trim()).toBe('hello');
  });

  it('should set flex style from host form', () => {
    const testFixture = TestBed.createComponent(TestComponent);
    const testComponent = testFixture.componentInstance;
    component = testFixture.debugElement.query(By.directive(FormItemComponent)).componentInstance;
    testFixture.detectChanges();

    expect(component._labelFlex).toBe('2');
    expect(component._inputFlex).toBe('3');
  });

  it('should incre/decre form error', () => {
    component.increFormError();
    expect(element.classList.contains('fui-form-item-with-error')).toBeTruthy();

    component.decreFormError();
    expect(element.classList.contains('fui-form-item-with-error')).toBeFalsy();
  });
});
