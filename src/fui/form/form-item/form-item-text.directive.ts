import { Directive, HostBinding } from '@angular/core';

@Directive({
  selector: '[fuiFormItemText]',
})
export class FormItemTextDirective {
  @HostBinding('class.fui-form-item-text2') hostClass = true;

  constructor() { }

}
