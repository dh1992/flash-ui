import { Component } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { By } from '@angular/platform-browser';

import { FormDirective } from './form.directive';

@Component({
  template: `
    <div fuiForm labelFlex="1" inputFlex="2">
    </div>
  `,
})
class TestComponent {}

describe('FormDirective', () => {
  let component: TestComponent;
  let fixture: ComponentFixture<TestComponent>;
  let element: HTMLElement;
  let form: HTMLElement;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ TestComponent, FormDirective],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestComponent);
    component = fixture.componentInstance;
    element = fixture.debugElement.nativeElement;
    form = fixture.debugElement.query(By.directive(FormDirective)).nativeElement;
    fixture.detectChanges();
  });

  it('should add fui-form class to host element', () => {
    expect(form.classList.contains('fui-form')).toBeTruthy();
  });
});
