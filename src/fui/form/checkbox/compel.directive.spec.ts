import { CompelValidatorDirective } from './compel.directive';
import { FormControl } from '@angular/forms';

describe('CompelValidatorDirective', () => {
  it('should create an instance', () => {
    const directive = new CompelValidatorDirective();

    const control = new FormControl();
    control.setValue(true);
    expect(directive.validate(control)).toEqual(null);

    control.setValue(false);
    expect(directive.validate(control)).toEqual({ fuiCompel: 'Must set true' });
  });
});
