import {
  Component,
  Optional,
  Inject,
  Input,
  ViewChild,
  HostBinding,
  forwardRef,
} from '@angular/core';

import {
  NgModel,
  NG_VALUE_ACCESSOR,
  NG_VALIDATORS,
  NG_ASYNC_VALIDATORS,
} from '@angular/forms';

import { ElementBase } from '../element-base';

@Component({
  selector: 'fui-checkbox',
  templateUrl: './checkbox.component.html',
  styleUrls: ['./checkbox.component.sass'],
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => CheckboxComponent),
    multi: true,
  }],
})
export class CheckboxComponent extends ElementBase<string> {
  @HostBinding('class.fui-checkbox') hostClass = true;
  @ViewChild(NgModel, { static: true }) model: NgModel;

  @HostBinding('class.selected')
  get selected() {
    return this.value;
  }

  @HostBinding('class.disabled')
  get isDisabled() {
    return this.disabled || this.readonly;
  }

  @HostBinding('class.half-selected')
  get halfSelectedClass() {
    return this.halfSelected && !this.value;
  }

  /** Set the checkbox style to be half selected. Overriden if the checkbox is selected. */
  @Input() halfSelected = false;

  constructor(
    @Optional() @Inject(NG_VALIDATORS) validators: Array<any>,
    @Optional() @Inject(NG_ASYNC_VALIDATORS) asyncValidators: Array<any>,
  ) {
    super(validators, asyncValidators);
  }

}
