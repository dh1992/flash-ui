/**
 * reference: https://github.com/clbond/form-example
 */
import { Input, Directive } from '@angular/core';
import { ControlValueAccessor } from '@angular/forms';

@Directive()
export abstract class ValueAccessorBase<T> implements ControlValueAccessor {
  private innerValue: T;
  private innerDisabled = false;
  private innerReadonly = false;

  private changed = new Array<(value: T) => void>();
  private touched = new Array<() => void>();

  protected initialValue: T;

  get value(): T {
    return this.innerValue;
  }

  set value(value: T) {
    if (this.innerValue !== value) {
      this.innerValue = value;
      this.changed.forEach(f => f(value));
    }
  }

  get disabled() {
    return this.innerDisabled;
  }

  @Input() set disabled(value: any) {
    const disabled = !(value === false);
    this.setDisabledState(disabled);
  }

  get readonly() {
    return this.innerReadonly;
  }

  @Input() set readonly(value: any) {
    const readonly = !(value === false);
    this.setReadonlyState(readonly);
  }

  writeValue(value: T) {
    this.innerValue = value;
    this.initialValue = value;
  }

  registerOnChange(fn: (value: T) => void) {
    this.changed.push(fn);
  }

  registerOnTouched(fn: () => void) {
    this.touched.push(fn);
  }

  touch() {
    this.touched.forEach(f => f());
  }

  setDisabledState(disabled: boolean) {
    this.innerDisabled = disabled;
  }

  setReadonlyState(readonly: boolean) {
    this.innerReadonly = readonly;
  }
}
