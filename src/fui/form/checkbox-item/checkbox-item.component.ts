import {
  Component,
  Host,
  HostBinding,
  Input,
  OnInit,
  OnDestroy,
} from '@angular/core';
import { Subscription } from 'rxjs';

import { CheckboxGroupComponent } from '../checkbox-group/checkbox-group.component';

@Component({
  selector: 'fui-checkbox-item',
  templateUrl: './checkbox-item.component.html',
  styleUrls: ['./checkbox-item.component.sass'],
})
export class CheckboxItemComponent implements OnInit, OnDestroy {
  @HostBinding('class.fui-checkbox-item') hostClass = true;

  /** Checkbox value used in checkbox group. */
  // eslint-disable-next-line
  @Input('value') checkboxValue: string;

  value = false;
  checkboxGroup: CheckboxGroupComponent;
  checkboxGroupSub: Subscription;

  @HostBinding('class.selected')
  get selected() {
    return this.value;
  }

  @HostBinding('class.disabled')
  get isDisabled() {
    return this.checkboxGroup.disabled || this.checkboxGroup.readonly;
  }

  constructor(
    @Host() checkboxGroup: CheckboxGroupComponent,
  ) {
    this.checkboxGroup = checkboxGroup;
  }

  ngOnInit() {
    const {model} = this.checkboxGroup;
    // ngModel的初始值不会触发`valueChanges`事件
    this.setValue(model.value);
    this.checkboxGroupSub = model.control.valueChanges
    .subscribe((collection: any[]) => {
      this.setValue(collection);
    });
  }

  setValue(collection: any[]) {
    if (!Array.isArray(collection)) {
      return;
    }
    this.value = !!~collection.indexOf(this.checkboxValue);
  }

  ngOnDestroy() {
    this.checkboxGroupSub.unsubscribe();
  }

  updateCheckboxGroupValue() {
    if (this.value) {
      this.checkboxGroup.addItem(this.checkboxValue);
    } else {
      this.checkboxGroup.removeItem(this.checkboxValue);
    }
  }

}
