import { Component, NO_ERRORS_SCHEMA } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { By } from '@angular/platform-browser';

import {
  CheckboxGroupComponent,
  CheckboxItemComponent,
} from '../form.module';

@Component({
  template: `
    <fui-checkbox-group>
      <fui-checkbox-item value="a"></fui-checkbox-item>
    </fui-checkbox-group>
  `,
})
class TestComponent {}

describe('CheckboxItemComponent', () => {
  let testComponent: TestComponent;
  let component: CheckboxItemComponent;
  let fixture: ComponentFixture<TestComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      schemas: [ NO_ERRORS_SCHEMA ],
      imports: [ FormsModule ],
      declarations: [
        CheckboxGroupComponent,
        CheckboxItemComponent,
        TestComponent,
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestComponent);
    testComponent = fixture.componentInstance;
    component = fixture.debugElement.query(By.directive(CheckboxItemComponent)).componentInstance;
    fixture.detectChanges();
  });

  it('should update checkbox group value', () => {
    spyOn(component.checkboxGroup, 'addItem');
    spyOn(component.checkboxGroup, 'removeItem');

    component.value = true;
    component.updateCheckboxGroupValue();
    expect(component.checkboxGroup.addItem).toHaveBeenCalledWith('a');

    component.value = false;
    component.updateCheckboxGroupValue();
    expect(component.checkboxGroup.removeItem).toHaveBeenCalledWith('a');
  });
});
