import { MaxValueValidatorDirective } from './max-value.directive';
import { FormControl } from '@angular/forms';

describe('MaxValueValidatorDirective', () => {
  it('should create an instance', () => {
    const directive = new MaxValueValidatorDirective({translateKey: key => key} as any);
    directive.fuiMaxValue = 10;

    const control = new FormControl();
    control.setValue(null);
    expect(directive.validate(control)).toEqual(null);

    control.setValue('number');
    expect(directive.validate(control)).toEqual(null);

    control.setValue(5);
    expect(directive.validate(control)).toEqual(null);

    control.setValue(15);
    expect(directive.validate(control)).toEqual({ fuiMaxValue: 'fui.FORM.INPUT_MAX_VALUE_ERROR 10'});
  });
});
