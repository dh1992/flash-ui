import { Validator, AbstractControl, ValidationErrors } from '@angular/forms';

export abstract class ValidatorBase implements Validator {
  private changeCbs: Array<() => void> = [];

  abstract validate(c: AbstractControl): ValidationErrors | null;

  triggerValidate() {
    this.changeCbs.forEach((cb) => cb());
  }

  registerOnValidatorChange(fn) {
    this.changeCbs.push(fn);
  }
}
