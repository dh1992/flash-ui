import { Directive, Input } from '@angular/core';
import { NG_VALIDATORS, AbstractControl } from '@angular/forms';

import { fuiTranslateService } from '../../i18n/i18n.module';
import { ValidatorBase } from './validator-base';

@Directive({
  selector: '[fuiMinValue]',
  providers: [
    {
      provide: NG_VALIDATORS,
      useExisting: MinValueValidatorDirective,
      multi: true,
    },
  ],
})
export class MinValueValidatorDirective extends ValidatorBase {
  private minValue: number;

  constructor(private translate: fuiTranslateService) {
    super();
  }

  /** Min value for validation. */
  @Input() set fuiMinValue(value: number) {
    this.minValue = +value;
    this.triggerValidate();
  }

  validate(control: AbstractControl): {[validator: string]: string | boolean} {
    if (this.isNotValue(control.value) || this.isNotValue(this.minValue)) {
      return null;
    }

    const value = +control.value;
    if (this.isNotNumber(value) || this.isNotNumber(this.minValue)) {
      return null;
    }

    if (value >= this.minValue) {
      return null;
    }

    return { fuiMinValue: `${this.translate.translateKey('fui.FORM.INPUT_MIN_VALUE_ERROR')} ${this.minValue}` };
  }

  private isNotValue(value) {
    return [undefined, null].indexOf(value) > -1;
  }

  private isNotNumber(value) {
    return isNaN(value) || !isFinite(value);
  }
}
