import { MinValueValidatorDirective } from './min-value.directive';
import { FormControl } from '@angular/forms';

describe('MinValueValidatorDirective', () => {
  it('should create an instance', () => {
    const directive = new MinValueValidatorDirective({translateKey: key => key} as any);
    directive.fuiMinValue = 10;

    const control = new FormControl();
    control.setValue(null);
    expect(directive.validate(control)).toEqual(null);

    control.setValue('number');
    expect(directive.validate(control)).toEqual(null);

    control.setValue(15);
    expect(directive.validate(control)).toEqual(null);

    control.setValue(5);
    expect(directive.validate(control)).toEqual({ fuiMinValue: 'fui.FORM.INPUT_MIN_VALUE_ERROR 10'});
  });
});
