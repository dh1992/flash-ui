import { EqualValidatorDirective } from './equal.directive';
import { FormControl } from '@angular/forms';

describe('EqualDirective', () => {
  it('should create an instance', () => {
    const directive = new EqualValidatorDirective({translateKey: key => key} as any);

    directive.fuiEqual = 'test';
    expect(directive.equalTo).toEqual('test');

    directive.fuiEqualPassword = 'password';
    expect(directive.equalTo).toEqual('password');
    expect(directive.errorMsg).toEqual('fui.FORM.EQUAL_PASSWORD_ERROR');

    const control = new FormControl();
    control.setValue('passwrod');
    expect(directive.validate(control)).toEqual({ fuiEqual: 'fui.FORM.EQUAL_PASSWORD_ERROR' });

    control.setValue('password');
    expect(directive.validate(control)).toEqual(null);
  });
});
