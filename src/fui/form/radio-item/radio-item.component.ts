import {
  Component,
  Optional,
  Inject,
  Input,
  ViewChild,
  Host,
  HostBinding,
} from '@angular/core';

import { RadioGroupComponent } from '../radio-group/radio-group.component';

@Component({
  selector: 'fui-radio-item',
  templateUrl: './radio-item.component.html',
  styleUrls: ['./radio-item.component.sass'],
})
export class RadioItemComponent {
  @HostBinding('class.fui-radio-item') hostClass = true;

  @HostBinding('class.selected')
  get selected() {
    return this.radioValue === this.radioGroup.value;
  }

  @HostBinding('class.disabled')
  get isDisabled() {
    return this.radioGroup.disabled || this.radioGroup.readonly;
  }

  /** Radio value in a radio group. */
  // eslint-disable-next-line
  @Input('value') radioValue: string;
  radioGroup: RadioGroupComponent;

  constructor(
    @Host() radioGroup: RadioGroupComponent,
  ) {
    this.radioGroup = radioGroup;
  }

}
