import { ComponentFixture, TestBed, fakeAsync, tick, waitForAsync } from '@angular/core/testing';
import { Component, DebugElement, NO_ERRORS_SCHEMA } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';

import { InputNumberComponent } from './input-number.component';

@Component({
  selector: 'fui-input-number-component-int-spec',
  template: `
    <fui-input-number [(ngModel)]="initValue" [fuiMinValue]="1" [fuiMaxValue]="10" [step]="1" [disabled]="isDisabled"></fui-input-number>
  `,
})
export class InputNumberComponentIntSpecComponent {
  isDisabled = false;
  initValue = 1;
}

describe('InputNumberComponent', () => {
  let testComponent: InputNumberComponentIntSpecComponent;
  let fixture: ComponentFixture<InputNumberComponentIntSpecComponent>;
  let debugElement: DebugElement;

  describe('input number test int', () => {
    beforeEach(waitForAsync(() => {
      TestBed.configureTestingModule({
        schemas: [NO_ERRORS_SCHEMA],
        imports: [
          FormsModule,
        ],
        declarations: [
          InputNumberComponent,
          InputNumberComponentIntSpecComponent,
        ],
      }).compileComponents();
    }));

    beforeEach(() => {
      fixture = TestBed.createComponent(InputNumberComponentIntSpecComponent);
      testComponent = fixture.debugElement.componentInstance;
      debugElement = fixture.debugElement.query(By.directive(InputNumberComponent));
    });

    it('should disabled up and down work', fakeAsync(() => {
      fixture.detectChanges();
      const handlerDownElement = debugElement.nativeElement.querySelector('.fui-input-number-handler-down');
      expect(handlerDownElement.classList.contains('fui-input-number-handler-down-disabled')).toBe(true);
      handlerDownElement.click();
      fixture.detectChanges();
      expect(testComponent.initValue).toBe(1);
      testComponent.initValue = 9;
      fixture.detectChanges();
      tick();
      const handlerUpElement = debugElement.nativeElement.querySelector('.fui-input-number-handler-up');
      handlerUpElement.click();
      fixture.detectChanges();
      expect(handlerUpElement.classList.contains('fui-input-number-handler-up-disabled')).toBe(true);
      expect(testComponent.initValue).toBe(10);
    }));

    it('should disable style work', () => {
      testComponent.isDisabled = true;
      fixture.detectChanges();
      expect(debugElement.nativeElement.classList.contains('fui-input-number-disabled')).toBe(true);
    });
  });
});
