import { Component, NO_ERRORS_SCHEMA } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { By } from '@angular/platform-browser';

import { InputComponent } from './input.component';
import { TranslatePipeStub } from '../../../mock';

@Component({
  template: `
    <fui-input type="password"></fui-input>
  `,
})
class TestComponent {}

describe('InputComponent', () => {
  let testComponent: TestComponent;
  let component: InputComponent;
  let fixture: ComponentFixture<TestComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      schemas: [ NO_ERRORS_SCHEMA ],
      imports: [ FormsModule ],
      declarations: [
        InputComponent,
        TestComponent,
        TranslatePipeStub,
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestComponent);
    testComponent = fixture.componentInstance;
    component = fixture.debugElement.query(By.directive(InputComponent)).componentInstance;
    fixture.detectChanges();
  });

  it('should toggle password show', () => {
    component.toggleShow();
    expect(component.type).toEqual('text');
    expect(component.suffix).toEqual('eye-open');

    component.toggleShow();
    expect(component.type).toEqual('password');
    expect(component.suffix).toEqual('eye-closed');
  });
});
