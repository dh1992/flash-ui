import { Component, Input, NO_ERRORS_SCHEMA } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { By } from '@angular/platform-browser';

import {
  CheckboxGroupComponent,
  CheckboxItemComponent,
} from '../form.module';
import { template } from 'gulp-util';

@Component({
  selector: 'fui-form-error',
  template: '',
})
class FormErrorComponent {
  @Input() validationResults;
  @Input() errorMsgs;
}

@Component({
  template: `
    <fui-checkbox-group [(ngModel)]="options">
      <fui-checkbox-item value="a">a</fui-checkbox-item>
      <fui-checkbox-item value="b">b</fui-checkbox-item>
      <fui-checkbox-item value="c">c</fui-checkbox-item>
    </fui-checkbox-group>
  `,
})
class TestComponent {
  options = ['a'];
}

describe('CheckboxGroupComponent', () => {
  let testComponent: TestComponent;
  let component: CheckboxGroupComponent;
  let fixture: ComponentFixture<TestComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      schemas: [],
      imports: [ FormsModule ],
      declarations: [
        FormErrorComponent,
        CheckboxItemComponent,
        CheckboxGroupComponent,
        TestComponent,
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestComponent);
    testComponent = fixture.componentInstance;
    component = fixture.debugElement.query(By.directive(CheckboxGroupComponent)).componentInstance;
    fixture.detectChanges();
  });

  afterEach(() => {
    fixture.destroy();
  });

  it('should be add/remove item', () => {
    component.value = [];
    component.addItem('test');
    expect(component.value.length).toEqual(1);

    component.removeItem('test');
    expect(component.value.length).toEqual(0);
  });
});
