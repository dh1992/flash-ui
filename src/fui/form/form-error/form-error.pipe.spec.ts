import { FormErrorPipe } from './form-error.pipe';

describe('FormErrorPipe', () => {
  it('create an instance', () => {
    const pipe = new FormErrorPipe();
    expect(pipe.transform({key: 'test', value: 'error'}, {test: 'custom'})).toEqual('custom');
    expect(pipe.transform({key: 'required', value: 'error'}, {test: 'custom'})).toEqual('fui.FORM.REQUIRED');
    expect(pipe.transform({key: 'any', value: 'error'}, {test: 'custom'})).toEqual('error');
    expect(pipe.transform({key: 'any', value: null}, {test: 'custom'})).toEqual('fui.FORM.VALIDATION_FAILED');
  });
});
