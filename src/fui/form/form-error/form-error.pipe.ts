import { Pipe, PipeTransform } from '@angular/core';

const defaultErrorMsgs = {
  required: 'fui.FORM.REQUIRED',
  email: 'fui.FORM.EMAIL_INVALID_ERROR',
  minlength: 'fui.FORM.MINLENGTH_ERROR',
  maxlength: 'fui.FORM.MAXLENGTH_ERROR',
  fuiCompel: 'fui.FORM.CHECKBOX_COMPEL_ERROR',
};

export class ValidatorResult {
  key: string;
  value: any;
}

@Pipe({
  name: 'fuiFormError',
})
export class FormErrorPipe implements PipeTransform {

  transform(result: ValidatorResult, customMsgs: Record<string, unknown> = {}): any {
    const {key, value} = result;
    if (customMsgs[key]) {
      return customMsgs[key];
    } else if (defaultErrorMsgs[key]) {
      return defaultErrorMsgs[key];
    } else if (value) {
      return value;
    }
    return 'fui.FORM.VALIDATION_FAILED';
  }

}
