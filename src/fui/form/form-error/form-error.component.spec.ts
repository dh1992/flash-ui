import { Pipe, PipeTransform, Component } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { TranslatePipeStub } from '../../../mock';

import { ValidationObject } from '../element-base';
import { FormErrorComponent } from './form-error.component';
import { FormItemComponent } from '../form-item/form-item.component';

@Pipe({
  name: 'fuiFormError',
})
export class FormErrorPipe implements PipeTransform {
  transform(value: any) {
    return value;
  }
}

describe('FormErrorComponent', () => {
  let component: FormErrorComponent;
  let fixture: ComponentFixture<FormErrorComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [
        FormErrorComponent,
        TranslatePipeStub,
        FormErrorPipe,
      ],
      providers: [
        {
          provide: FormItemComponent,
          useValue: {
            increFormError() {},
            decreFormError() {},
          },
        },
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormErrorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should set validation results', () => {
    const validation1 =  new ValidationObject();
    validation1.key = 'required';
    validation1.value = true;
    const validation2 = new ValidationObject();
    validation2.key = 'any';

    component.validationResults = [validation1, validation2];
    expect(component.results.length).toEqual(1);

    validation1.key = 'any';
    component.validationResults = [validation1, validation2];
    expect(component.results.length).toEqual(2);
  });
});
