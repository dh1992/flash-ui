import { Directive, HostBinding } from '@angular/core';

@Directive({
  selector: '[fuiRadioBtnGroup]',
})
export class RadioBtnGroupDirective {
  @HostBinding('class.fui-radio-btn-group') hostClass = true;

  constructor() { }

}
