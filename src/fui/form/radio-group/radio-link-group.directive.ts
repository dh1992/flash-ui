import { Directive, HostBinding } from '@angular/core';

@Directive({
  selector: '[fuiRadioLinkGroup]',
})
export class RadioLinkGroupDirective {
  @HostBinding('class.fui-radio-link-group') hostClass = true;

  constructor() { }

}
