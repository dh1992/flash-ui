import {
  Component,
  Optional,
  Inject,
  Input,
  ViewChild,
  HostBinding,
} from '@angular/core';

import {
  NgModel,
  NG_VALUE_ACCESSOR,
  NG_VALIDATORS,
  NG_ASYNC_VALIDATORS,
} from '@angular/forms';

import { ElementBase } from '../element-base';

@Component({
  selector: 'fui-radio-group',
  templateUrl: './radio-group.component.html',
  styleUrls: ['./radio-group.component.sass'],
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: RadioGroupComponent,
    multi: true,
  }],
})
export class RadioGroupComponent extends ElementBase<string> {
  @HostBinding('class.fui-radio-group') hostClass = true;

  @ViewChild(NgModel, { static: true }) model: NgModel;

  /** Name for radio items. */
  @Input() name: string;

  /** Validation error messages. */
  @Input() errorMsgs: string[] = [];

  constructor(
    @Optional() @Inject(NG_VALIDATORS) validators: Array<any>,
    @Optional() @Inject(NG_ASYNC_VALIDATORS) asyncValidators: Array<any>,
  ) {
    super(validators, asyncValidators);
  }

  updateValue(newValue) {
    this.value = newValue;
    this.markAsDirty();
  }

}
