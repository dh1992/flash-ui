import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { FlexModule } from '../flex/flex.module';
import { I18nModule } from '../i18n/i18n.module';
import { IconModule } from '../icon/icon.module';

/**
 * Form
 */
import { FormDirective } from './form.directive';
import { FormItemComponent } from './form-item/form-item.component';
import { FormItemAddonDirective } from './form-item/form-item-addon.directive';
import { FormItemTextDirective } from './form-item/form-item-text.directive';
import { FormErrorComponent } from './form-error/form-error.component';
import { FormErrorPipe } from './form-error/form-error.pipe';

/**
 * Form components
 */
import { CheckboxComponent } from './checkbox/checkbox.component';
import { CheckboxGroupComponent } from './checkbox-group/checkbox-group.component';
import { CheckboxItemComponent } from './checkbox-item/checkbox-item.component';
import { CompelValidatorDirective } from './checkbox/compel.directive';
import { InputComponent } from './input/input.component';
import { InputNumberComponent } from './input-number/input-number.component';
import { RadioBtnGroupDirective } from './radio-group/radio-btn-group.directive';
import { RadioLinkGroupDirective } from './radio-group/radio-link-group.directive';
import { RadioGroupComponent } from './radio-group/radio-group.component';
import { RadioItemComponent } from './radio-item/radio-item.component';
import { TextareaComponent } from './textarea/textarea.component';
import { SwitchComponent } from './switch/switch.component';

/**
 * Validators
 */
import { MaxValueValidatorDirective } from './validators/max-value.directive';
import { MinValueValidatorDirective } from './validators/min-value.directive';
import { EqualValidatorDirective } from './validators/equal.directive';

export * from './element-base';
export * from './value-accessor';
export * from './validate';
export * from './checkbox/checkbox.component';
export * from './checkbox-group/checkbox-group.component';
export * from './checkbox-item/checkbox-item.component';
export * from './checkbox/compel.directive';
export * from './form.directive';
export * from './form-error/form-error.component';
export * from './form-error/form-error.pipe';
export * from './form-item/form-item.component';
export * from './form-item/form-item-addon.directive';
export * from './input/input.component';
export * from './input-number/input-number.component';
export * from './radio-group/radio-btn-group.directive';
export * from './radio-item/radio-item.component';
export * from './radio-group/radio-group.component';
export * from './switch/switch.component';
export * from './validators/validator-base';
export * from './validators/equal.directive';
export * from './validators/min-value.directive';
export * from './validators/max-value.directive';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    FlexModule,
    I18nModule,
    IconModule,
  ],
  declarations: [
    FormDirective,
    FormItemComponent,
    FormItemAddonDirective,
    FormItemTextDirective,
    FormErrorComponent,
    FormErrorPipe,
    CheckboxComponent,
    CheckboxGroupComponent,
    CheckboxItemComponent,
    CompelValidatorDirective,
    InputComponent,
    InputNumberComponent,
    RadioBtnGroupDirective,
    RadioLinkGroupDirective,
    RadioGroupComponent,
    RadioItemComponent,
    TextareaComponent,
    SwitchComponent,
    MaxValueValidatorDirective,
    MinValueValidatorDirective,
    EqualValidatorDirective,
  ],
  exports: [
    FormDirective,
    FormItemComponent,
    FormItemAddonDirective,
    FormItemTextDirective,
    FormErrorComponent,
    FormErrorPipe,
    CheckboxComponent,
    CheckboxGroupComponent,
    CheckboxItemComponent,
    CompelValidatorDirective,
    InputComponent,
    InputNumberComponent,
    RadioBtnGroupDirective,
    RadioLinkGroupDirective,
    RadioGroupComponent,
    RadioItemComponent,
    TextareaComponent,
    SwitchComponent,
    MaxValueValidatorDirective,
    MinValueValidatorDirective,
    EqualValidatorDirective,
  ],
})
export class FormModule { }
