
import {
  Directive,
  OnInit,
  Input,
  OnChanges,
  SimpleChanges,
  ElementRef,
  HostBinding,
} from '@angular/core';

const ICON_COLORS = ['danger', 'primary', 'warning', 'success', 'info', 'default', 'secondary', 'mute', 'disabled', 'reverse'];

@Directive({
  selector: '[fuiIcon]',
})
export class IconDirective implements OnInit, OnChanges {
  @HostBinding('class.fui-icon') true;
  @HostBinding('style.width') width;
  @HostBinding('style.height') height;

  /** Icon name. */
  @Input() fuiIcon: string;

  /**
   *  Icon color, support 'danger', 'primary', 'warning', 'success', 'info', 'default', 'secondary',
   *  'mute', 'disabled', 'reverse' or arbitrary color.
   */
  @Input() color: string;

  /** Icon size, support 'sm', 'md', 'lg' or arbitrary size. Default size is 'md', 16px. */
  @Input() size: string;
  node: SVGUseElement = document.createElementNS('http://www.w3.org/2000/svg', 'use');

  constructor(private el: ElementRef) {
    this.el.nativeElement.appendChild(this.node);
  }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges) {
    const {
      fuiIcon: iconChange,
      size: sizeChange,
      color: colorChange,
    } = changes;
    if (iconChange && iconChange.currentValue) {
      this.setIcon(`#${iconChange.currentValue}`);
    }
    if (sizeChange && sizeChange.currentValue) {
      this.setDims(sizeChange.currentValue);
    }
    if (colorChange && colorChange.currentValue) {
      this.setColor(colorChange.currentValue);
    }
  }

  setIcon(iconName) {
    this.node.setAttributeNS('http://www.w3.org/1999/xlink', 'xlink:href', iconName);
  }

  setDims(size) {
    let unit: string;
    switch (size) {
    case 'lg':
      unit = '24px';
      break;
    case 'md':
      unit = '16px';
      break;
    case 'sm':
      unit = '12px';
      break;
    default:
      unit = size;
    }
    this.width = unit;
    this.height = unit;
  }

  setColor(color) {
    const element: SVGSVGElement = this.el.nativeElement;
    if (ICON_COLORS.indexOf(color) > -1) {
      ICON_COLORS.forEach((c) => {
        element.classList.remove(`fui-icon-${c}`);
      });
      element.classList.add(`fui-icon-${color}`);
    } else {
      element.style['fill'] = color;
    }
  }

}
