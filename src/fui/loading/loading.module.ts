import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LoadingDirective } from './loading.directive';

export * from './loading.directive';

@NgModule({
  imports: [
    CommonModule,
  ],
  declarations: [
    LoadingDirective,
  ],
  exports: [
    LoadingDirective,
  ],
})
export class LoadingModule { }
