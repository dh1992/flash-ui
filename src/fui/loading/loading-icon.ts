export const makeLoadingIcon = (): SVGSVGElement => {
  const loadingIcon = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
  const loadingIconUse: SVGUseElement = document.createElementNS('http://www.w3.org/2000/svg', 'use');
  loadingIconUse.setAttributeNS('http://www.w3.org/1999/xlink', 'xlink:href', '#loading');
  loadingIcon.classList.add('fui-rotate');
  loadingIcon.classList.add('fui-loading');
  loadingIcon.appendChild(loadingIconUse);
  return loadingIcon;
};
