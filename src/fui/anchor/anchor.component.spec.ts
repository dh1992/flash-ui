import { Component, DebugElement, NO_ERRORS_SCHEMA, ViewChild } from '@angular/core';
import { By } from '@angular/platform-browser';
import { ComponentFixture, TestBed, fakeAsync, tick, waitForAsync } from '@angular/core/testing';

import { AnchorModule } from './anchor.module';

import { AnchorComponent } from './anchor.component';

const throttleTime = 2000;

describe('anchor', () => {
  let fixture: ComponentFixture<TestComponent>;
  let dl: DebugElement;
  let context: TestComponent;
  let page: PageObject;

  beforeEach(() => {
    const i = TestBed.configureTestingModule({
      imports: [AnchorModule],
      declarations: [TestComponent],
    });
    fixture = TestBed.createComponent(TestComponent);
    dl = fixture.debugElement;
    context = fixture.componentInstance;
    fixture.detectChanges();
    page = new PageObject();
    spyOn(context, 'onScroll');
  });
  afterEach(() => context.component.ngOnDestroy());

  describe('[default]', () => {
    it('should hava remove listen when the component is destroyed', () => {
      expect(context.component['scrollSub'].closed).toBeFalsy();
      context.component.ngOnDestroy();
      fixture.detectChanges();
      expect(context.component['scrollSub'].closed).toBeTruthy();
    });

    it('should actived when scrolling to the anchor', (done: () => void) => {
      context.container = document.querySelector('.container');
      expect(context.onScroll).not.toHaveBeenCalled();
      page.scrollTo();
      setTimeout(() => {
        expect(context.onScroll).toHaveBeenCalled();
        done();
      }, throttleTime);
    });

    it('should clean actived when leave all anchor', fakeAsync(() => {
      spyOn(context.component, 'clearActive' as any);
      page.scrollTo();
      tick(throttleTime);
      fixture.detectChanges();
      expect(context.component['clearActive']).not.toHaveBeenCalled();
    }));

    it(`should priorities most recently`, (done: () => void) => {
      context.container = document.querySelector('.container');
      expect(context.onScroll).not.toHaveBeenCalled();
      page.scrollTo('#parallel1');
      setTimeout(() => {
        expect(context.onScroll).toHaveBeenCalled();
        done();
      }, throttleTime);
    });
  });

  describe('property', () => {
    it('(clickEvent)', () => {
      spyOn(context, 'onClick');
      expect(context.onClick).not.toHaveBeenCalled();
      const linkList = dl.queryAll(By.css('.fui-anchor-link-title'));
      expect(linkList.length).toBeGreaterThan(0);
      (linkList[0].nativeElement as HTMLLinkElement).click();
      fixture.detectChanges();
      expect(context.onClick).toHaveBeenCalled();
    });
  });

  class PageObject {
    getEl(cls: string): HTMLElement {
      const el = dl.query(By.css(cls));
      expect(el).not.toBeNull();
      return el.nativeElement as HTMLElement;
    }
    scrollTo(href: string = '#basic'): this {
      const toNode = dl.query(By.css(href));
      (toNode.nativeElement as HTMLElement).scrollIntoView();
      fixture.detectChanges();
      return this;
    }
  }
});

@Component({
  template: `
    <fui-anchor
      [container]="container"
      (scrollEvent)="onScroll($event)"
      (clickEvent)="onClick($event)"
    >
      <fui-anchor-link href="#test" title="test"></fui-anchor-link>
      <fui-anchor-link href="#basic" title="Basic demo"></fui-anchor-link>
      <fui-anchor-link href="#API-AnchorLink">
        <ng-template #fuiTemplate>
          <span class="fuiTemplate-title">tpl</span>
        </ng-template>
      </fui-anchor-link>
      <fui-anchor-link href="#API" title="API">
        <fui-anchor-link href="#API-Anchor" title="fui-anchor"></fui-anchor-link>
        <fui-anchor-link href="#API-AnchorLink" [title]="title">
          <ng-template #title>
            <span class="fuiTitle-title">tpl-title</span>
          </ng-template>
        </fui-anchor-link>
      </fui-anchor-link>
      <fui-anchor-link href="#parallel1" title="parallel1"></fui-anchor-link>
      <fui-anchor-link href="#parallel2" title="parallel2"></fui-anchor-link>
    </fui-anchor>

    <div class="container" style="height: 1000px">
      <h2 id="test"></h2>
      <div style="height: 1000px"></div>
      <h2 id="basic"></h2>
      <div style="height: 1000px"></div>
      <h2 id="API"></h2>
      <div style="height: 1000px"></div>
      <h2 id="API-Anchor"></h2>
      <div style="height: 1000px"></div>
      <h2 id="API-AnchorLink"></h2>
      <table>
        <tr>
          <td><h2 id="parallel1">parallel1</h2></td>
          <td><h2 id="parallel2">parallel2</h2></td>
        </tr>
      </table>
    </div>
  `,
})
export class TestComponent {
  @ViewChild(AnchorComponent, { static: false }) component: AnchorComponent;
  container: any = null;
  onClick() {}
  onScroll() {}
  handleScrollTo() {}
}
