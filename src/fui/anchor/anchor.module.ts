import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PlatformModule } from '@angular/cdk/platform';

import { AnchorComponent } from './anchor.component';
import { AnchorLinkComponent } from './anchor-link.component';

export * from './anchor.component';

@NgModule({
  imports: [
    CommonModule,
    PlatformModule,
  ],
  declarations: [
    AnchorComponent,
    AnchorLinkComponent,
  ],
  exports: [
    AnchorComponent,
    AnchorLinkComponent,
  ],
})
export class AnchorModule { }
