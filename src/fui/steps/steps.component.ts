import {
  AfterContentInit,
  Component,
  ContentChildren,
  HostBinding,
  Input,
  QueryList,
} from '@angular/core';

import { StepItemComponent } from './step-item/step-item.component';

@Component({
  selector: 'fui-steps',
  templateUrl: './steps.component.html',
})
export class StepsComponent implements AfterContentInit {
  @HostBinding('class.fui-steps') true;
  @ContentChildren(StepItemComponent) steps: QueryList<StepItemComponent>;

  /** Current active step index. */
  @Input()
  set current(current: number) {
    this._current = current;
    this.updateChildrenSteps();
  }

  _current: number;

  constructor() {
  }

  ngAfterContentInit() {
    setTimeout(() => {
      this.updateChildrenSteps();
    });
  }

  updateChildrenSteps() {
    if (this.steps) {
      this.steps.toArray().forEach((step, index, attr) => {
        step.index = index;
        step.currentIndex = this._current;
        step.last = attr.length === index + 1;
      });
    }
  }
}
