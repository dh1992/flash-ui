import {
  Component,
  ElementRef,
  HostBinding,
  Input,
  Output,
} from '@angular/core';

@Component({
  selector: 'fui-step-item',
  templateUrl: './step-item.component.html',
})
export class StepItemComponent {
  @HostBinding('class.fui-step-item') true;
  @HostBinding('class.fui-step-active')
  get active() {
    return this.currentIndex === this.index;
  }

  /** Current active step index. (Internal use) */
  @Input() currentIndex: number;

  index = 0;
  last = false;

  constructor() {
  }

}
