import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IconModule } from '../icon/icon.module';

import { StepsComponent } from './steps.component';
import { StepItemComponent } from './step-item/step-item.component';

@NgModule({
  imports: [
    CommonModule,
    IconModule,
  ],
  declarations: [
    StepsComponent,
    StepItemComponent,
  ],
  exports: [
    StepsComponent,
    StepItemComponent,
  ],
})
export class StepsModule { }
