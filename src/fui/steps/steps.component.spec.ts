import { Component } from '@angular/core';
import { ComponentFixture, TestBed, fakeAsync, tick, waitForAsync } from '@angular/core/testing';

import { StepsComponent } from './steps.component';
import { StepItemComponent } from './step-item/step-item.component';

@Component({
  template:
    `<fui-steps>
      <fui-step-item>1</fui-step-item>
      <fui-step-item>2</fui-step-item>
      <fui-step-item>3</fui-step-item>
    </fui-steps>`,
})
class TestWrapperComponent {
}

describe('StepItemComponent', () => {
  let component: StepsComponent;
  let testComponent: TestWrapperComponent;
  let fixture: ComponentFixture<TestWrapperComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [
        StepItemComponent,
        StepsComponent,
        TestWrapperComponent,
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestWrapperComponent);
    testComponent = fixture.componentInstance;
    component = fixture.debugElement.children[0].componentInstance;
    fixture.detectChanges();
  });

  it('should set current step', fakeAsync(() => {
    component.current = 1;
    tick();
    expect(component.steps.toArray()[1].active).toEqual(true);
  }));
});
