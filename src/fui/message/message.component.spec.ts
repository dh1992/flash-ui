import { Component, NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, fakeAsync, tick, waitForAsync } from '@angular/core/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { MessageContainerComponent } from './message-container/message-container.component';
import { MessageComponent } from './message.component';

@Component({
  selector: 'fui-test-component',
  template: '<fui-message [message]="message" [index]="index"></fui-message>',
})
class MessageContainerStubComponent {
  message = {
    messageId: '1',
    options: {
      animate: true,
      duration: 10,
      pauseOnHover: true,
    },
  };
  index = 0;
  removeMessage() {}
}

describe('MessageComponent', () => {
  let testComponent: MessageContainerStubComponent;
  let component: MessageComponent;
  let fixture: ComponentFixture<MessageContainerStubComponent>;
  let container: MessageContainerComponent;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      schemas: [NO_ERRORS_SCHEMA],
      declarations: [
        MessageComponent,
        MessageContainerStubComponent,
      ],
      imports: [
        BrowserAnimationsModule,
      ],
      providers: [
        {
          provide: MessageContainerComponent,
          useClass: MessageContainerStubComponent,
        },
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MessageContainerStubComponent);
    testComponent = fixture.componentInstance;
    component = fixture.debugElement.children[0].componentInstance;
    fixture.detectChanges();

    container = TestBed.inject(MessageContainerComponent);
  });

  afterEach(() => {
    fixture.destroy();
  });

  it('should be created', fakeAsync(() => {
    spyOn(container, 'removeMessage');

    component.onEnter();
    tick(10);

    component.onLeave();
    tick(300);
    expect(container.removeMessage).toHaveBeenCalledWith('1');
  }));
});
