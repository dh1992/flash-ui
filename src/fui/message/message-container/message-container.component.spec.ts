import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { Router, NavigationStart } from '@angular/router';
import { Observable ,  Subject } from 'rxjs';

import { MessageDataFilled } from '../message.model';
import { MessageContainerComponent } from './message-container.component';

class RouterStub {
  eventSub = new Subject();
  events = new Subject();
}

describe('MessageContainerComponent', () => {
  let component: MessageContainerComponent;
  let fixture: ComponentFixture<MessageContainerComponent>;
  let router: Router;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      schemas: [NO_ERRORS_SCHEMA],
      declarations: [ MessageContainerComponent ],
      providers: [
        {
          provide: Router,
          useClass: RouterStub,
        },
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    router = TestBed.inject(Router);

    fixture = TestBed.createComponent(MessageContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create message', () => {
    const message = new MessageDataFilled();
    component.createMessage(message);
    expect(component.messages.length).toEqual(1);
    expect(component.messages[0].options).toEqual({
      duration: 4000,
      animate: true,
      pauseOnHover: true,
    });
  });

  it('should not display too many message', () => {
    const message = new MessageDataFilled();
    component.messages = Array(7).fill(message);
    expect(component.messages.length).toEqual(7);

    component.createMessage(message);
    expect(component.messages.length).toEqual(7);
  });

  it('should remove message', () => {
    const message = new MessageDataFilled();
    message.messageId = 'message-0';

    component.createMessage(message);
    expect(component.messages.length).toEqual(1);
    component.removeMessage('message-0');
    expect(component.messages.length).toEqual(0);
  });

  it('should remove all messages', () => {
    const message = new MessageDataFilled();
    message.messageId = 'message-0';

    component.createMessage(message);
    expect(component.messages.length).toEqual(1);
    component.removeMessageAll();
    expect(component.messages.length).toEqual(0);
  });

  it('should remove all messages after navigation', () => {
    const message = new MessageDataFilled();
    message.messageId = 'message-0';

    component.createMessage(message);
    expect(component.messages.length).toEqual(1);
    (router as unknown as RouterStub).events.next(new NavigationStart(1, ''));
    expect(component.messages.length).toEqual(0);
  });
});
