import { TestBed, inject } from '@angular/core/testing';
import { Overlay } from '@angular/cdk/overlay';

import { fuiMessageService } from './message.service';

class Container {
  createMessage() {}

}

class MessageContainerComponentStub {
  createMessage() {}
}

const CONTAINER = new MessageContainerComponentStub();

class OverlayStub {
  create() {
    return {
      attach() {
        return {
          instance: CONTAINER,
        };
      },
    };
  }
}

describe('fuiMessageService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        fuiMessageService,
        {
          provide: Overlay,
          useClass: OverlayStub,
        },
      ],
    });
  });

  it('should show success message', inject([fuiMessageService], (service: fuiMessageService) => {
    const mySpy = spyOn(CONTAINER, 'createMessage');
    service.success('Success message');
    expect((mySpy.calls.mostRecent().args as any[])[0].type).toEqual('success');
    expect((mySpy.calls.mostRecent().args as any[])[0].content).toEqual('Success message');
  }));

  it('should show different type of messages', inject([fuiMessageService], (service: fuiMessageService) => {
    const mySpy = spyOn(CONTAINER, 'createMessage');

    service.error('Error message');
    expect((mySpy.calls.mostRecent().args as any[])[0].type).toEqual('error');
    expect((mySpy.calls.mostRecent().args as any[])[0].content).toEqual('Error message');

    service.info('Info message');
    expect((mySpy.calls.mostRecent().args as any[])[0].type).toEqual('info');
    expect((mySpy.calls.mostRecent().args as any[])[0].content).toEqual('Info message');

    service.warning('Warning message');
    expect((mySpy.calls.mostRecent().args as any[])[0].type).toEqual('warning');
    expect((mySpy.calls.mostRecent().args as any[])[0].content).toEqual('Warning message');

    service.loading('Loading message');
    expect((mySpy.calls.mostRecent().args as any[])[0].type).toEqual('loading');
    expect((mySpy.calls.mostRecent().args as any[])[0].content).toEqual('Loading message');

    service.create('success', 'Success message');
    expect((mySpy.calls.mostRecent().args as any[])[0].type).toEqual('success');
    expect((mySpy.calls.mostRecent().args as any[])[0].content).toEqual('Success message');

    service.html('html content');
    expect((mySpy.calls.mostRecent().args as any[])[0].html).toEqual('html content');
  }));
});
