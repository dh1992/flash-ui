import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { OverlayModule } from '@angular/cdk/overlay';
import { IconModule } from '../icon/icon.module';

import { fuiMessageService } from './message.service';
import { MessageComponent } from './message.component';
import { MessageContainerComponent } from './message-container/message-container.component';

export * from './message.model';
export * from './message.service';
export * from './message.component';
export * from './message-container/message-container.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    OverlayModule,
    IconModule,
  ],
  declarations: [
    MessageComponent,
    MessageContainerComponent,
  ],
  providers: [
    fuiMessageService,
  ],
  entryComponents: [
    MessageContainerComponent,
  ],
  exports: [
    MessageComponent,
    MessageContainerComponent,
  ],
})
export class MessageModule { }
