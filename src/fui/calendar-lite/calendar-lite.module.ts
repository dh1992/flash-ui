import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CalendarLiteComponent } from './calendar-lite.component';

export * from './calendar-lite.component';

@NgModule({
  imports: [
    CommonModule,
  ],
  declarations: [
    CalendarLiteComponent,
  ],
  exports: [
    CalendarLiteComponent,
  ],
})
export class CalendarLiteModule { }
