import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { Subject } from 'rxjs';

import { fuiTranslateService } from '../i18n/i18n.module';
import { CalendarDay, CalendarLiteComponent, CalendarMonth } from './calendar-lite.component';

describe('CalendarLiteComponent', () => {
  let component: CalendarLiteComponent;
  let fixture: ComponentFixture<CalendarLiteComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ CalendarLiteComponent ],
      providers: [
        {
          provide: fuiTranslateService,
          useValue: {
            lang: 'zh_CN',
            translateKey: (key) => key,
            onLangChange: new Subject(),
          },
        },
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CalendarLiteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should build calendar on set', () => {
    const buildSpy = spyOn(component, 'buildCalendar');

    component.showMonth = 2;
    expect(buildSpy).toHaveBeenCalled();

    component.showYear = 2018;
    expect(buildSpy.calls.count()).toEqual(2);

    const date = new Date('2018-4-10');
    component.value = date;
    expect(component.showMonth).toEqual(3);
    expect(component.showYear).toEqual(2018);
    expect(buildSpy.calls.count()).toEqual(4);

    component.value = date;
    expect(buildSpy.calls.count()).toEqual(4);
  });

  it('should click day', () => {
    spyOn(component.clickDayChange, 'emit');

    const event = new MouseEvent('click');
    const day = new CalendarDay();
    day.disabled = true;
    component.clickDay(event, day);
    expect(component.clickDayChange.emit).not.toHaveBeenCalled();
    day.disabled = false;
    component.clickDay(event, day);
    expect(component.clickDayChange.emit).toHaveBeenCalled();
  });

  it('should click month', () => {
    spyOn(component.clickMonthChange, 'emit');

    const event = new MouseEvent('click');
    const month = new CalendarMonth();
    month.disabled = true;
    component.clickMonth(event, month);
    expect(component.clickMonthChange.emit).not.toHaveBeenCalled();
    month.disabled = false;
    component.clickMonth(event, month);
    expect(component.clickMonthChange.emit).toHaveBeenCalled();
  });
});
