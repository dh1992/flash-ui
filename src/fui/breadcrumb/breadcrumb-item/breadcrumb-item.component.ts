import { Component, OnInit, HostBinding } from '@angular/core';

@Component({
  selector: 'fui-breadcrumb-item',
  templateUrl: './breadcrumb-item.component.html',
})
export class BreadcrumbItemComponent implements OnInit {
  @HostBinding('class.fui-breadcrumb-item') true;

  constructor() { }

  ngOnInit() {
  }

}
