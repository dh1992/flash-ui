import {
  Component,
  HostBinding,
} from '@angular/core';

@Component({
  selector: 'fui-breadcrumb',
  templateUrl: './breadcrumb.component.html',
})
export class BreadcrumbComponent {
  @HostBinding('class.fui-breadcrumb') true;

  constructor() { }
}
