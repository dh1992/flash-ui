import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { I18nModule } from '../i18n/i18n.module';
import { IconModule } from '../icon/icon.module';

import { BreadcrumbComponent } from './breadcrumb.component';
import { BreadcrumbItemComponent } from './breadcrumb-item/breadcrumb-item.component';

export * from './breadcrumb.component';
export * from './breadcrumb-item/breadcrumb-item.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    I18nModule,
    IconModule,
  ],
  declarations: [
    BreadcrumbComponent,
    BreadcrumbItemComponent,
  ],
  exports: [
    BreadcrumbComponent,
    BreadcrumbItemComponent,
  ],
})
export class BreadcrumbModule { }
