import { NgModule, ModuleWithProviders } from '@angular/core';
import { OverlayContainer } from '@angular/cdk/overlay';
import { fuiOverlayContainer } from './shared/fui-overlay-container';

import { AnchorModule } from './anchor/anchor.module';
import { AvatarModule } from './avatar/avatar.module';
import { BtnModule } from './btn/btn.module';
import { CalendarLiteModule } from './calendar-lite/calendar-lite.module';
import { CarouselModule } from './carousel/carousel.module';
import { CascadeModule } from './cascade/cascade.module';
import { DatepickerModule } from './datepicker/datepicker.module';
import { DatepickerLiteModule } from './datepicker-lite/datepicker-lite.module';
import { DefaultModule } from './default/default.module';
import { DropdownModule } from './dropdown/dropdown.module';
import { FileDialogModule } from './file-dialog/file-dialog.module';
import { FlexModule } from './flex/flex.module';
import { FormModule } from './form/form.module';
import { IconModule } from './icon/icon.module';
import { LoadingModule } from './loading/loading.module';
import { MenuModule } from './menu/menu.module';
import { MessageModule } from './message/message.module';
import { ModalModule } from './modal/modal.module';
import { PaginationModule } from './pagination/pagination.module';
import { PopoverModule } from './popover/popover.module';
import { ProgressModule } from './progress/progress.module';
import { PropertyModule } from './property/property.module';
import { SearchModule } from './search/search.module';
import { SelectModule } from './select/select.module';
import { SubmenuModule } from './submenu/submenu.module';
import { StepsModule } from './steps/steps.module';
import { TabModule } from './tab/tab.module';
import { TableModule } from './table/table.module';
import { TimepickerModule } from './timepicker/timepicker.module';
import { TooltipModule } from './tooltip/tooltip.module';
import { TreeModule } from './tree/tree.module';
import { BreadcrumbModule } from './breadcrumb/breadcrumb.module';
import { PromptModule } from './prompt/prompt.module';
import { UploadModule } from './upload/upload.module';

const FUI_MODULES = [
  AnchorModule,
  AvatarModule,
  BreadcrumbModule,
  BtnModule,
  CalendarLiteModule,
  CarouselModule,
  CascadeModule,
  DatepickerModule,
  DatepickerLiteModule,
  DefaultModule,
  DropdownModule,
  FileDialogModule,
  FlexModule,
  FormModule,
  IconModule,
  LoadingModule,
  MenuModule,
  MessageModule,
  ModalModule,
  PaginationModule,
  PopoverModule,
  ProgressModule,
  PropertyModule,
  SearchModule,
  SelectModule,
  SubmenuModule,
  StepsModule,
  TabModule,
  TableModule,
  TimepickerModule,
  TooltipModule,
  TreeModule,
  PromptModule,
  UploadModule,
];

@NgModule({
  imports: FUI_MODULES,
  exports: FUI_MODULES,
  providers: [
    {
      provide: OverlayContainer,
      useClass: fuiOverlayContainer,
    },
  ],
})
export class FuiModule {
  // For lazy load
  public static forRoot(): ModuleWithProviders<FuiModule> {
    return {
      ngModule: FuiModule,
    };
  }

  public static forChild(): ModuleWithProviders<FuiModule> {
    return {
      ngModule: FuiModule,
    };
  }
}
